#!/bin/bash
 
#############################################All functions define start here#############################################################
function imsanity_version() {
imsanity_version="3.1"
		echo "==============================================================="
		echo "IMSANITY TOOL VERSION $imsanity_version STARTED AT ====> "`date`
		echo "==============================================================="
		interval_time_start=$(date +\%s)		
}

function check_setup_type() {
	check_clustername_count=$(cat config/config.db | grep -i clusterName | grep -v mig | wc -l)
	check_setup_type=$(imconfget -m mss statelessMSS)
	if [[ "$check_clustername_count" == "1" && "$check_setup_type" == "true" ]]
	then
		setuptype="stateless"
		#echo $setuptype
	else
		setuptype="stateful"
		#echo $setuptype
	fi
	if [ "$setuptype" == "stateless" ]
		then
		#call function for stateless execution
		stateless
		elif [ "$setuptype" == "stateful" ]
		then
		#call function for stateful execution
		stateful
	fi		
}

function set_config_keys() {

#copy useful files 
cp -pr config/config.db config/config.db.sanity
cp -r Imsanity_Tool_3.1/100K .
cp -r Imsanity_Tool_3.1/10MB .
cp -r Imsanity_Tool_3.1/1MB .
cp -r Imsanity_Tool_3.1/32k-mime_doc .
cp -r Imsanity_Tool_3.1/64k-mime_pdf .
cp -r Imsanity_Tool_3.1/75k .
cp -r Imsanity_Tool_3.1/troy-consistency-tool-1.0.0-09-SNAPSHOT-jar-with-dependencies.jar .
cp -r Imsanity_Tool_3.1/ss .
cp -r Imsanity_Tool_3.1/apitest .
cp -r Imsanity_Tool_3.1/imcl_apitest .

echo "Config keys are setting"

imconfcontrol -install -key "/*/mta/listProcessingDisable=false" &>>trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mta/listProcessingDisable" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "false" ] 
							then
							    echo "/*/mta/listProcessingDisable is set "
							else
                                echo " ERROR :/*/mta/listProcessingDisable is not set "	
                            fi
imconfcontrol -install -key "/*/common/mailFolderQuotaEnabled=true" > output.txt

							updtd_key_value7=$(cat config/config.db | grep -i "/*/common/mailFolderQuotaEnabled" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "true" ]                                                                                                   
							then
							    echo "/*/common/mailFolderQuotaEnabled is set "
							else
                                echo " ERROR :/*/common/mailFolderQuotaEnabled is not set "	
                            fi
cat output.txt | egrep -i "re-started" | cut -d " " -f4  >hosts.txt
imdbcontrol sac $user11 openwave.com mailfolderquota "/ all,AgeSeconds,1" >> trash							
imdbcontrol sac $user2 openwave.com mailfolderquota "/ all,AgeDays,2"  &>> trash
imdbcontrol sac $user12 openwave.com mailquotamaxmsgs 4  &>> trash
imdbcontrol sac $user13 openwave.com mailfolderquota "/INBOX all,msgkb,2"  &>> trash
imconfcontrol -install -key "/*/common/traceOutputLevel=imap=9"  &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/common/traceOutputLevel" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "imap=9" ] 
							then
							    echo "/*/common/traceOutputLevel is set "
							else
                                echo " ERROR :/*/common/traceOutputLevel is not set "	
                            fi

########################### Check for bounced messages when folderQuota message kb hit ########
 
imconfcontrol -install -key "/*/mss/maintenanceDurationSecs=3600"    &>> trash	
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/maintenanceDurationSecs" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "3600" ] 
							then
							    echo "/*/mss/maintenanceDurationSecs is set "
							else
                                echo " ERROR :/*/mss/maintenanceDurationSecs is not set "	
                            fi
imconfcontrol -install -key "/*/mss/maintenanceStartAtSecs=3600"	 &>> trash	
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/maintenanceStartAtSecs" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "3600" ] 
							then
							    echo "/*/mss/maintenanceStartAtSecs is set "
							else
                                echo " ERROR :/*/mss/maintenanceStartAtSecs is not set "	
                            fi
imconfcontrol -install -key "/*/mss/maintenanceNumThreads=10" 	 	 &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/maintenanceNumThreads" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "10" ] 
							then
							    echo "/*/mss/maintenanceNumThreads is set "
							else
                                echo " ERROR :/*/mss/maintenanceNumThreads is not set "	
                            fi
imconfcontrol -install -key "/*/mss/expireBeforeBounce=true"	 &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/expireBeforeBounce" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "true" ] 
							then
							    echo "/*/mss/expireBeforeBounce is set "
							else
                                echo " ERROR :/*/mss/expireBeforeBounce is not set "	
                            fi
imconfcontrol -install -key "/*/mss/quotaMaxMacroDepth=10"		 &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/quotaMaxMacroDepth" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "10" ] 
							then
							    echo "/*/mss/quotaMaxMacroDepth is set "
							else
                                echo " ERROR :/*/mss/quotaMaxMacroDepth is not set "	
                            fi
imconfcontrol -install -key "/*/mss/mailfolderQuotaPrefix=RSS(1):FSS(1) SSS(1),"  &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/mailfolderQuotaPrefix" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							#updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							#if [ "$updtd_key_value7" == "RSS(1):FSS(1) SSS(1)," ] 
							#then
							#    echo "/*/mss/mailfolderQuotaPrefix is set "
							#else
                            #    echo " ERROR :/*/mss/mailfolderQuotaPrefix is not set "	
                            #fi
imconfcontrol -install -key "/*/mss/maintenanceEnabled=true"  &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/maintenanceEnabled" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "true" ] 
							then
							    echo "/*/mss/maintenanceEnabled is set "
							else
                                echo " ERROR :/*/mss/maintenanceEnabled is not set "	
                            fi
#################################################################################################
#imap check
imconfcontrol -install -key "/*/mss/autoReplyExpireDays=3" &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/autoReplyExpireDays" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "3" ] 
							then
							    echo "/*/mss/autoReplyExpireDays=3 is set "
							else
                                echo " ERROR :/*/mss/autoReplyExpireDays= is not set "	
                            fi
imconfcontrol -install -key  "/*/common/traceOutputLevel=keepalive=1" &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/common/traceOutputLevel" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "keepalive=1" ] 
							then
							    echo "/*/common/traceOutputLevel is set "
							else
                                echo " ERROR :/*/common/traceOutputLevel is not set "	
                            fi
imconfcontrol -install -key  "/*/mss/bogus1=12333223" &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/bogus1" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "12333223" ] 
							then
							    echo "/*/mss/bogus1 is set "
							else
                                echo " ERROR :/*/mss/bogus1 is not set "	
                            fi
imconfcontrol -install -key  "/*/mss/bogus2=wwwqweeqe" &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/bogus2" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "wwwqweeqe" ] 
							then
							    echo "/*/mss/bogus2 is set "
							else
                                echo " ERROR :/*/mss/bogus2 is not set "	
                            fi
imconfcontrol -install -key  "/*/mss/bogus3=312sdwqwewe" &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/bogus3" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "312sdwqwewe" ] 
							then
							    echo "/*/mss/bogus3 is set "
							else
                                echo " ERROR :/*/mss/bogus3 is not set "	
                            fi
imconfcontrol -install -key  "/*/mss/bogus4=3423eweeqwewa" &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/bogus4" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "3423eweeqwewa" ] 
							then
							    echo "/*/mss/bogus4 is set "
							else
                                echo " ERROR :/*/mss/bogus4 is not set "	
                            fi
#imap move
imconfcontrol -install -key "/*/imapserv/enableMOVE=true" &>>trash.txt
							updtd_key_value7=$(cat config/config.db | grep -i "/*/imapserv/enableMOVE" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "true" ] 
							then
							    echo "/*/imapserv/enableMOVE is set "
							else
                                echo " ERROR :/*/imapserv/enableMOVE is not set "	
                            fi
imconfcontrol -install -key "/*/imapserv/enableIdle=true" &>>trash.txt
							updtd_key_value7=$(cat config/config.db | grep -i "/*/imapserv/enableIdle" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "true" ] 
							then
							    echo "/*/imapserv/enableIdle is set "
							else
                                echo " ERROR :/*/imapserv/enableIdle is not set "	
                            fi
imconfcontrol -install -key "/*/imapserv/enableSORT=true" &>> trash.txt
							updtd_key_value7=$(cat config/config.db | grep -i "/*/imapserv/enableSORT" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "true" ] 
							then
							    echo "/*/imapserv/enableSORT is set "
							else
                                echo " ERROR :/*/imapserv/enableSORT is not set "	
                            fi
###################################### Check for bounced messages when maintenance through maintenance server#########


imconfcontrol -install -key  "/*/mss/mailboxAging=true"  &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/mailboxAging" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "true" ] 
							then
							    echo "/*/mss/mailboxAging= is set "
							else
                                echo " ERROR :/*/mss/mailboxAging= is not set "	
                            fi
imconfcontrol -install -key  "/*/mss/mailboxAgingDeleteEnabled=true"  &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/mailboxAgingDeleteEnabled" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "true" ] 
							then
							    echo "/*/mss/mailboxAgingDeleteEnabled is set "
							else
                                echo " ERROR :/*/mss/mailboxAgingDeleteEnabled is not set "	
                            fi
imconfcontrol -install -key  "/*/mss/mailboxAgingLogEnabled=true"  &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/mailboxAgingLogEnabled" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "true" ] 
							then
							    echo "/*/mss/mailboxAgingLogEnabled is set "
							else
                                echo " ERROR :/*/mss/mailboxAgingLogEnabled is not set "	
                            fi
imconfcontrol -install -key  "/*/mss/mailboxAgingLogDir=agedMailbox"  &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/mailboxAgingLogDir" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "agedMailbox" ] 
							then
							    echo "/*/mss/mailboxAgingLogDir= is set "
							else
                                echo " ERROR :/*/mss/mailboxAgingLogDir= is not set "	
                            fi
imconfcontrol -install -key  "/*/mss/mailboxAgingLogEnabled=true"  &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/mailboxAgingLogEnabled" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "true" ] 
							then
							    echo "/*/mss/mailboxAgingLogEnabled is set "
							else
                                echo " ERROR :/*/mss/mailboxAgingLogEnabled is not set "	
                            fi

imconfcontrol -install -key  "/*/mss/mailboxAgingMaxAgeSeconds=2"	 &>> trash 
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/mailboxAgingMaxAgeSeconds" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "2" ] 
							then
							    echo "/*/mss/mailboxAgingMaxAgeSeconds is set "
							else
                                echo " ERROR :/*/mss/mailboxAgingMaxAgeSeconds is not set "	
                            fi						
imdbcontrol sac $user14 openwave.com mailfolderquota "/ all,ageseconds,1"  &>> trash

imconfcontrol -install -key "/*/mss/maintenanceStartAtSecs=`./ss`"  &>> trash
#imconfcontrol -install -key "/*/common/mailFolderQuotaEnabled=true"    	&>> trash
								#imconfcontrol -install -key "/*/mss/maintenanceDurationSecs=3600"   &>> trash	
								#imconfcontrol -install -key "/*/mss/maintenanceStartAtSecs=3600"	&>> trash	
								#imconfcontrol -install -key "/*/mss/maintenanceNumThreads=10" 	 &>> trash	
								#imconfcontrol -install -key "/*/mss/expireBeforeBounce=true"		&>> trash
								#imconfcontrol -install -key "/*/mss/quotaMaxMacroDepth=10"		&>> trash
								#imconfcontrol -install -key "/*/mss/mailfolderQuotaPrefix=RSS(1):FSS(1) SSS(1)," &>> trash
								#imconfcontrol -install -key "/*/mss/maintenanceEnabled=true" &>> trash
								#imconfcontrol -install -key  "/*/mss/mailboxAging=true" &>> trash
         						#imconfcontrol -install -key  "/*/mss/mailboxAgingDeleteEnabled=true" &>> trash
								#imconfcontrol -install -key  "/*/mss/mailboxAgingLogEnabled=true" &>> trash
								#imconfcontrol -install -key  "/*/mss/mailboxAgingLogDir=agedMailbox" &>> trash
								#imconfcontrol -install -key  "/*/mss/mailboxAgingLogEnabled=true" &>> trash
								imconfcontrol -install -key  "/*/mss/mailboxAgingMaxAgeDays=0" &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/mss/mailboxAgingMaxAgeDays" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "0" ] 
							then
							    echo "/*/mss/mailboxAgingMaxAgeDays is set "
							else
                                echo " ERROR :/*/mss/mailboxAgingMaxAgeDays is not set "	
                            fi						
								#imconfcontrol -install -key  "/*/mss/mailboxAgingMaxAgeSeconds=2" &>> trash
								imconfcontrol -install -key  "/*/common/enableCassandraHardDelete=true" &>> trash
							updtd_key_value7=$(cat config/config.db | grep -i "/*/common/enableCassandraHardDelete" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value7=`echo $updtd_key_value7 | tr -d " "`
							if [ "$updtd_key_value7" == "true" ] 
							then
							    echo "/*/common/enableCassandraHardDelete is set "
							else
                                echo " ERROR :/*/common/enableCassandraHardDelete is not set "	
                            fi						
imdbcontrol sac $user10 openwave.com mailfolderquota "/ all,ageseconds,2" &>> trash																																																			
###########################################################################################################################
#imdbcontrol sac $user14 openwave.com mailfolderquota "/ all,AgeSeconds,1"
imdbcontrol sca default mailquotamaxmsgkb 0  &>> trash
imdbcontrol sca default mailquotamaxmsgs 0  &>> trash
imdbcontrol sca default mailquotatotkb 0  &>> trash
						# setting keys for large mail box
                         #echo " Setting the keys ..."
							imconfcontrol -install -key /*/common/parentalBlockSendersEnabled="false" >> trash.txt
							imconfcontrol -install -key /*/popserv/clientTimeout="240" >> trash.txt
							imconfcontrol -install -key /*/imapserv/clientTimeout="1800">> trash.txt
							imconfcontrol -install -key /*/common/rmeConnectTimeout="10">> trash.txt
							imconfcontrol -install -key /*/common/netTimeout="240" >> trash.txt
							imconfcontrol -install -key /*/mss/cassMsgFileMaxSizeInKB="100000" >> trash.txt

							updtd_key_value1=$(imconfget parentalBlockSendersEnabled )	
							updtd_key_value1=`echo $updtd_key_value1 | tr -d " "`
							updtd_key_value2=$(cat config/config.db | grep -i "/*/popserv/clientTimeout" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value2=`echo $updtd_key_value2 | tr -d " "`
							updtd_key_value3=$(cat config/config.db | grep -i "/*/imapserv/clientTimeout" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value3=`echo $updtd_key_value3 | tr -d " "`
							updtd_key_value4=$(imconfget rmeConnectTimeout )	
							updtd_key_value4=`echo $updtd_key_value4 | tr -d " "`
							updtd_key_value5=$(cat config/config.db | grep -i "/*/common/netTimeout" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value5=`echo $updtd_key_value5 | tr -d " "`
							updtd_key_value6=$(cat config/config.db | grep -i "/*/mss/cassMsgFileMaxSizeInKB" | cut -d "[" -f2 | cut -d "]" -f1 | head -1)	
							updtd_key_value6=`echo $updtd_key_value6 | tr -d " "`

							if [ "$updtd_key_value1" == "false" ] 
							then
							    echo " /*/common/parentalBlockSendersEnabled is set "
							else
                                echo " ERROR : /*/common/parentalBlockSendersEnabled is not set "	
                            fi

                            if [ "$updtd_key_value2" == "240" ] 
							then
							    echo " /*/popserv/clientTimeout is set "
							else
                                echo " ERROR : /*/popserv/clientTimeout is not set "                 
                            fi		



                            if [ "$updtd_key_value3" == "1800" ] 
							then
							    echo " /*/imapserv/clientTimeout is set "
							else
                                echo " ERROR : /*/imapserv/clientTimeout is not set "								
                            fi		

                            if [ "$updtd_key_value4" == "10" ] 
							then
							    echo " /*/common/rmeConnectTimeout is set "
							else
                                echo " ERROR : /*/common/rmeConnectTimeout is not set "									
                            fi		

                            if [ "$updtd_key_value5" == "240" ] 
							then
							    echo " /*/common/netTimeout is set "
							else
                                echo " ERROR : /*/common/netTimeout is not set "                            
                            fi	

                            if [ "$updtd_key_value6" == "100000" ]
							then
							    echo " /*/mss/cassMsgFileMaxSizeInKB is set "
							else
                                echo " ERROR : /*/mss/cassMsgFileMaxSizeInKB is not set "	
                            fi
			
						 imconfcontrol -install -key /*/common/mailFolderQuotaEnabled="true" >>trash.txt
						 updtd_key_value=$(imconfget mailFolderQuotaEnabled )	
						 updtd_key_value=`echo $updtd_key_value | tr -d " "` 



						if [ "$updtd_key_value" == "true" ]
						then
						    echo
							echo " mailFolderQuotaEnabled is set as "$updtd_key_value
						else
						    echo
							echo " mailFolderQuotaEnabled is not set "
						fi
						  echo "Config keys are set"
                          echo " Restarting the servers "

 lib/imservctrl killStart  &>> trash
							    imservping >started.txt
							    started=$(cat started.txt | grep -i unreachable | wc -l)
                                strated=` echo $started | tr -d " " `
							
							    if [ $started -gt 0 ]
							    then
							        echo " ERROR : Servers are not started properly . "
								else
       							    echo " Servers are started . "							
							    fi

}

function start_time_tc() {	
			
				t=$1
				arg1=$1
				interval_time_start_tc=$(date +\%s) 
				echo " " >> Alltestcasestime.txt
				echo " " >> Alltestcasestime.txt
				echo "Test case $arg1 start at $interval_time_start_tc"  >> Alltestcasestime.txt

}

function end_time_tc() {
				t=$1
				arg1=$1
				interval_time_end_tc=$(date +\%s)
				echo "Test case $arg1 end at $interval_time_end_tc" >> Alltestcasestime.txt
				interval_time_total_tc=$(expr $interval_time_end_tc - $interval_time_start_tc)
				total_min_tc=60		
				interval_time_total_min_tc=$(echo "scale=2; $interval_time_total_tc / $total_min_tc" | bc)
				echo "===================================================================" >> Alltestcasestime.txt
				echo "Time taken by test case $arg1 is $interval_time_total_min_tc Min" >> Alltestcasestime.txt
				echo "===================================================================" >> Alltestcasestime.txt

}

function restart_all_mss() {
									imconfcontrol -install -key "/*/common/mailFolderQuotaEnabled=false" > output.txt
									cat output.txt | egrep -i "server reported" | cut -d " " -f4 > hosts.txt
									for host in `cat hosts.txt`
									do
										host=`echo $host | tr -d " "`
										imctrl $host killStart mss.1
										imservping -h $host mss > ping_resp.txt
										resp_count=$(cat ping_resp.txt | grep -i "responded" | wc -l)
										resp_count=`echo $resp_count | tr -d " "`
										if [ "$resp_count" == "1" ]
										then
											echo
											echo "MSS on "$host" is successfully restarted"
										else
											echo
											echo "ERROR: MSS on "$host" could not be restarted. Please restart manually."
											echo
										fi
									done
}

function log_delete() {
		echo "log files are deleting..."
		~/lib/imservctrl kill  &>> trash
		msg_chk=$(ps -ef | grep `whoami` | grep mss | wc -l)
		if [ "$msg_chk" == "1" ]
		then
			echo "servers are stopped"
			cd log 
			log=$(ls) 
			if [ "$log" == "" ]
			then
				echo
				echo "No log files are their to delete"
				echo "Processing Further..."
			cd ..
			else

			rm -rf *.log 
			rm -rf *.trace 
			rm -rf *.stat 

			log1=$(ls *.log| wc -l)
			 
				if [ "$log1" != "0" ]
				then
					echo "Log files are deleted"
					echo "server are starting...."
					
					~/lib/imservctrl killStart  &>> trash
					msg_chk=$(ps -ef | grep `whoami` | grep mss | wc -l)
					if [ "$msg_chk" != "1" ]
					then
					echo "servers are started"
					else
					echo "servers are not started"
					exit
					fi
					cd ..
				
				else
					echo "Log files are NOT deleted"
					echo "server are starting...."
					
					~/lib/imservctrl killStart  &>> trash
					msg_chk=$(ps -ef | grep `whoami` | grep mss | wc -l)
					if [ "$msg_chk" != "1" ]
					then
					echo "servers are started"
					else
					echo "servers are started"
					exit
					fi
					cd ..

				fi

			fi
		else
		echo "servers are not stopped"
		exit
		fi
}
 
function cleanup() {
								
				start_time_tc					
								echo "###########################"
								echo "     CLEANUP STARTED     "
								echo "###########################"

								echo "====================================="
								echo "Deleting mailboxes, please wait..."
								echo "====================================="
								imboxdelete $Cluster $MBXID_Sanity1  &>> trash
								imboxdelete $Cluster $MBXID_Sanity2 &>> trash
								imboxdelete $Cluster $MBXID_Sanity3 &>> trash
								imboxdelete $Cluster $MBXID_Sanity4 &>> trash
								imboxdelete $Cluster $MBXID_Sanity5 &>> trash
								imboxdelete $Cluster $MBXID_Sanity6 &>> trash
								imboxdelete $Cluster $MBXID_Sanity7 &>> trash
								imboxdelete $Cluster $MBXID_Sanity8  &>> trash
								imboxdelete $Cluster $MBXID_Sanity9 &>> trash
								imboxdelete $Cluster $MBXID_Sanity10 &>> trash
								imboxdelete $Cluster $MBXID_Sanity11 &>> trash
								imboxdelete $Cluster $MBXID_Sanity12 &>> trash
								imboxdelete $Cluster $MBXID_Sanity13 &>> trash
								imboxdelete $Cluster $MBXID_Sanity14 &>> trash
								imboxdelete $Cluster $MBXID_Sanity20 &> trash
								imboxdelete $FirstMSS $MBXID_Sanity21 &>> trash
								imboxdelete $SecondMSS $MBXID_Sanity22 &>> trash
								imboxdelete $Cluster $MBXID_Sanity23 &>> trash
								imboxdelete $Cluster $MBXID_Sanity24 &>> trash
								imboxdelete $Cluster $MBXID_Sanity25 &>> trash
								imboxdelete $Cluster $MBXID_Sanity27 &>> trash
								imboxdelete $Cluster $MBXID_Sanity18 &>> trash

								imboxstats $MBXID_Sanity1 > boxdelete_boxstats_check.txt
								imboxstats $MBXID_Sanity2 >> boxdelete_boxstats_check.txt
								#imboxstats $MBXID_Sanity3 >> boxdelete_boxstats_check.txt
								#imboxstats $MBXID_Sanity4 >> boxdelete_boxstats_check.txt
								imboxstats $MBXID_Sanity14 >> boxdelete_boxstats_check.txt
								imboxstats $MBXID_Sanity27 >> boxdelete_boxstats_check.txt
								
								msnotfound_count=$(cat boxdelete_boxstats_check.txt | grep -i "MsNotFound" | wc -l)
								msnotfound_count=`echo $msnotfound_count | tr -d " "`
								if [ "$msnotfound_count" == "4" ]
								then
									echo
									echo "All Mailboxes are deleted. imboxdelete utility is working fine."
									Result="0"
									summary "Imboxdelete utility" $Result
									echo
										echo "====================================="
										echo "Re-Creating mailboxes, please wait..."
										echo "====================================="
										start_time_tc
										imboxcreate $Cluster $MBXID_Sanity1 > verify_mbx_creat.txt
										imboxcreate $Cluster $MBXID_Sanity2 >> verify_mbx_creat.txt
										#imboxcreate $Cluster $MBXID_Sanity3 >> verify_mbx_creat.txt
										#imboxcreate $Cluster $MBXID_Sanity4 >> verify_mbx_creat.txt
										#imboxcreate $Cluster $MBXID_Sanity5 >> verify_mbx_creat.txt
										#imboxcreate $Cluster $MBXID_Sanity6 >> verify_mbx_creat.txt

										mbx_crtd1=$(cat verify_mbx_creat.txt | grep -i created | wc -l)
										mbx_crtd1=`echo $mbx_crtd1 | tr -d " "`
										if [ "$mbx_crtd1" == "2" ]
										then
											echo
											echo "Verified that Mailboxes were created successfully again."
											Result="0"
											echo
											echo "============================================"
											echo "Deleting recreated mailboxes, please wait..."
											echo "============================================"
											imboxdelete $Cluster $MBXID_Sanity1 >&3
											imboxdelete $Cluster $MBXID_Sanity2 >&3
											#imboxdelete $Cluster $MBXID_Sanity3 >&3
											#imboxdelete $Cluster $MBXID_Sanity4 >&3
											#imboxdelete $Cluster $MBXID_Sanity5 >&3
											#imboxdelete $Cluster $MBXID_Sanity6 >&3

											imboxstats $MBXID_Sanity1 > boxdelete_boxstats_check.txt
											imboxstats $MBXID_Sanity2 >> boxdelete_boxstats_check.txt
											#imboxstats $MBXID_Sanity3 >> boxdelete_boxstats_check.txt
											#imboxstats $MBXID_Sanity4 >> boxdelete_boxstats_check.txt
											#imboxstats $MBXID_Sanity5 >> boxdelete_boxstats_check.txt
											#imboxstats $MBXID_Sanity6 >> boxdelete_boxstats_check.txt

											msnotfound_count1=$(cat boxdelete_boxstats_check.txt | grep -i "MsNotFound" | wc -l)
											msnotfound_count1=`echo $msnotfound_count1 | tr -d " "`
											if [ "$msnotfound_count1" == "2" ]
											then
												echo
												echo "All Mailboxes are deleted."
												echo "===================================="
												echo "imboxdelete utility is working fine."
												echo "===================================="
												Result="0"
												echo
											else
												echo
												echo "ERROR: Some or all mailboxes are not deleted using imboxdelete utility in the second attempt."
												echo
												Result="1"
											fi
										else
											echo
											echo "Mailboxes were not created successfully."
											Result="1"
											echo
										fi
								else
									echo
									echo "ERROR: Some or all mailboxes are not deleted using imboxdelete utility in the first attempt."
									echo
									Result="1"
								fi
summary "re-creation of account and mailbox" $Result
start_time_tc
								echo "================================"
								echo "Deleting users, please wait..."
								echo "================================"
								imdbcontrol da $user1 openwave.com >&3
								imdbcontrol da $user2 openwave.com >&3
								imdbcontrol da $user3 openwave.com >&3
								imdbcontrol da $user4 openwave.com >&3
								imdbcontrol da $user5 openwave.com >&3
								imdbcontrol da $user6 openwave.com >&3
								imdbcontrol da $user7 openwave.com >&3
								imdbcontrol da $user8 openwave.com >&3
								imdbcontrol da $user9 openwave.com >&3
								imdbcontrol da $user10 openwave.com >&3
								imdbcontrol da $user11 openwave.com >&3
								imdbcontrol da $user12 openwave.com >&3
								imdbcontrol da $user13 openwave.com >&3
								imdbcontrol da $user14 openwave.com >&3
								imdbcontrol da $user20 openwave.com >&3
								imdbcontrol da $user21 openwave.com >&3
								imdbcontrol da $user22 openwave.com >&3
								imdbcontrol da $user23 openwave.com >&3
								imdbcontrol da $user24 openwave.com >&3
								imdbcontrol da $user25 openwave.com >&3
								imdbcontrol da $user26 openwave.com >&3
								imdbcontrol da Sanity11111113 openwave.com >&3
								imdbcontrol da Sanity11111114 openwave.com >&3
								imdbcontrol da Sani111111118 openwave.com >&3
								imdbcontrol da Sani111111115 openwave.com >&3
								imdbcontrol da Sanity11111127 openwave.com >&3
								#imdbcontrol da Sanity11111111 openwave.com >&3
								imdbcontrol da ownermailinglist openwave.com >&3
								userscnt1=$(imdbcontrol la | grep -i @ | grep -i Sanity111111 | wc -l)
								userscnt1=`echo $userscnt1 | tr -d " "`
								if [ "$userscnt1" == "0" ]
								then
									echo
									echo "Successfully deleted all Users"
									Result="0"
									echo
								else
									echo
									echo "Users are not deleted properly. Please check Manually"
									Result="1"
									echo
								fi

								#cat <&3
								#imdbcontrol dc newcosclass
								echo "###########################"
								echo "     CLEANUP COMPLETED     "
								echo "###########################"
								echo	
								summary "Account deletion" $Result
								# calling function for summary report of imasanity
								summary_imsanity
		
}

function prints() {

		string_to_print=$1
		time=$(date '+%H:%M:%S')
		echo
		echo "$time : $string_to_print"
		echo

}

function summary_imsanity() {
		echo "##############################################"
		echo "        SUMMARY OF THE IMSANITY TOOL          "
		echo " Refer sumamry.log file for Test cases result "
		echo "##############################################"
		echo
		echo "CORES FOUND"
		echo "==========="
		cat cores_found.txt
		cat mta_cores_found.txt
		cat pop_cores_found.txt
		cat imap_cores_found.txt
		find $INTERMAIL/tmp -name "*core.*"
		find $INTERMAIL/../ -name "*core.*"
		echo
		echo "ERRORS REPORTED"
		echo "==============="
		echo "MTA ERRORS >>>"
		cat $INTERMAIL/log/mta.log | egrep -i "erro;|urgt;|fatl;"
		echo "<<<"
		echo 
		echo "POP ERRORS >>>"
		cat $INTERMAIL/log/popserv.log | egrep -i "erro;|urgt;|fatl;"
		echo "<<<"
		echo 
		echo "IMAP ERRORS >>>"
		cat $INTERMAIL/log/imapserv.log | egrep -i "erro;|urgt;|fatl;"
		echo "<<<"
		echo 
		echo "MSS ERRORS >>>"
		cat $INTERMAIL/log/mss.log | egrep -i "erro;|urgt;|fatl;" | egrep -v "ClusterUnableToPingMSS|ProcWriteToStderr"
		echo "<<<"
		echo
		echo "NOTE:"
		echo "====="
		echo "1. Please check cores and errors on other Mx hosts in this setup."
		echo "2. This utility works only if atleast 2 MSS are there in the MSS Cluster."
		echo "3. Please refer sumamry.log file for Test cases result"
		echo "4. Please ignore the message \"-ERR no such message\" from the POP results. This occurs in an edge case and is expected."
		echo "##############################################"

		rm -rf boxcopy.txt boxstats_Sanity2.txt boxdelete_boxstats_check.txt folderlist_Sanity1.txt immsgfind_Sanity1.txt output.txt verify_mbx_creat.txt boxmaint_Sanity1.txt boxstats_Sanity3.txt hosts.txt mboxes.txt ping_resp.txt boxmaint_stats_Sanity1.txt chckfq.txt imap_cores_found.txt pop_cores_found.txt boxmaint_stats_Sanity1_2.txt cores_found.txt imap_errors.txt mta_cores_found.txt pop_errors.txt boxstats_Sanity1.txt descms_Sanity1.txt immsgdump_Sanity1.txt mta_errors.txt trace.txt boxstats_Sanity5.txt boxstats_Sanity3.txt boxstats_Sanity6.txt hostname.txt large_msg login.txt removeKey.txt 
		rm -rf 1 1.txt 664567.txt 664567_1.txt Alltestcasestime.txt IMCHECKFQ_Sanity2.txt boxget_sanity.txt boxstats.txt boxstats1.txt boxstats122.txt boxstats133.txt boxstats144.txt boxstats_Sanity155.txt boxstats_Sanity16.txt boxtest_sanity1.txt chk_mailbox.txt cmpms_output.txt cos_unset.txt copy.txt cos.txt delete_result.txt domain.txt folder.txt folder1.txt idle.txt imap.txt imboxatt.txt imboxstats.txt imfilter.txt imfilter1.txt imimap.txt immssinvlist.txt immtacheck.txt imsanity.txt invctrl_sanity.txt key_changed.txt keys_Set.txt keys_set.txt mail.txt mail_fwd.txt mail_sent.txt mailbox.txt 
		rm -rf temp temp1 mailinglist.txt maint.txt map.txt mbx_create.txt message.txt message1.txt modify_result.txt modify_result1.txt move.txt msgtrace.txt pop.txt queu.txt queu1.txt recepient.txt result.txt root.txt search_result.txt server_restart.txt servers_restart.txt shared.txt shared1.txt sort.txt started.txt temp.txt temporary.txt temporary1.txt trace1.txt trace2.txt trash.txt upr_string.txt user.txt user.txt.fail user.txt.pass value_set.txt
		
		rm -rf 100K 10MB 1MB 32k-mime_doc 64k-mime_pdf 75k troy-consistency-tool-1.0.0-09-SNAPSHOT-jar-with-dependencies.jar ss imcl_apitest apitest msg.txt
		
		rm -rf alias.txt add.ldif add_result.txt automsg add.ldif add_result.txt automsg del.ldif mail_del modify.ldif new.com.20130306141442.errors new.com.20130306141442.input trash "trash,txt" trasht 
		rm -rf recepient.* , check.txt
		mv -f config/config.db.sanity config/config.db
		echo
		echo "==============================================================="
		echo "IMSANITY TOOL COMPLETED AT ====> "`date`
		echo "==============================================================="
		interval_time_end=$(date +\%s)
		interval_time_total=$(expr $interval_time_end - $interval_time_start)
		total_min=60		
		interval_time_total_min=$(echo "scale=2; $interval_time_total / $total_min" | bc)
		echo "================================================================" >> summary.log
		echo "IMSANITY TOOL TOOK "$interval_time_total_min" MINUTE TO COMPLETE" >> summary.log
		echo "================================================================" >> summary.log
		echo "================================================================" 
		echo "IMSANITY TOOL TOOK "$interval_time_total_min" MINUTE TO COMPLETE" 
		echo "================================================================" 
		#calling function for summary logs
		summary summary_imsanity_tc 0
		exit
}

function creation_mailbox_user_stateless() {
start_time_tc
		# Creating users and their mailboxes
		echo
		echo 
		
		echo "Creating users..."
		echo
		user_prefix=Sanity111111
		user1=$user_prefix"1"
		user2=$user_prefix"2"
		user3=$user_prefix"3"
		user4=$user_prefix"4"
		user5=$user_prefix"5"
		user6=$user_prefix"6"
		user7=$user_prefix"7"
		user8=$user_prefix"8"
		user9=$user_prefix"9"
		user10=$user_prefix"10"
		user11=$user_prefix"11"
		user12=$user_prefix"12"
		user13=$user_prefix"13"
		user14=$user_prefix"14"
        user20=$user_prefix"20"
		user21=$user_prefix"21"
        user22=$user_prefix"22"
        user23=$user_prefix"23"
		user24=$user_prefix"24"
		user25=$user_prefix"25"
		user27=$user_prefix"27"


		MBXID_Sanity1="1"$(echo $RANDOM)
		MBXID_Sanity1=`echo $MBXID_Sanity1 | tr -d " "`
		imdbcontrol ca $user1 $Cluster $MBXID_Sanity1 $user1 $user1 clear  &>> trash

		MBXID_Sanity2="1"$(echo $RANDOM)
		MBXID_Sanity2=`echo $MBXID_Sanity2 | tr -d " "`
		imdbcontrol ca $user2 $Cluster $MBXID_Sanity2 $user2 $user2 clear &>> trash

		MBXID_Sanity3="1"$(echo $RANDOM)
		MBXID_Sanity3=`echo $MBXID_Sanity3 | tr -d " "`
		imdbcontrol ca $user3 $Cluster $MBXID_Sanity3 $user3 $user3 clear &>> trash

		MBXID_Sanity4="1"$(echo $RANDOM)
		MBXID_Sanity4=`echo $MBXID_Sanity4 | tr -d " "`
		imdbcontrol ca $user4 $Cluster $MBXID_Sanity4 $user4 $user4 clear &>> trash

		MBXID_Sanity5="1"$(echo $RANDOM)
		MBXID_Sanity5=`echo $MBXID_Sanity5 | tr -d " "`
		imdbcontrol ca $user5 $Cluster $MBXID_Sanity5 $user5 $user5 clear &>> trash

		MBXID_Sanity6="1"$(echo $RANDOM)
		MBXID_Sanity6=`echo $MBXID_Sanity6 | tr -d " "`
		imdbcontrol ca $user6 $Cluster $MBXID_Sanity6 $user6 $user6 clear &>> trash

		MBXID_Sanity7="1"$(echo $RANDOM)
		MBXID_Sanity7=`echo $MBXID_Sanity7 | tr -d " "`
		imdbcontrol ca $user7 $Cluster $MBXID_Sanity7 $user7 $user7 clear &>> trash

		MBXID_Sanity8="1"$(echo $RANDOM)
		MBXID_Sanity8=`echo $MBXID_Sanity8 | tr -d " "`
		imdbcontrol ca $user8 $Cluster $MBXID_Sanity8 $user8 $user8 clear &>> trash
		
		#for account mode M test case
		MBXID_Sanity9="1"$(echo $RANDOM)
		MBXID_Sanity9=`echo $MBXID_Sanity9 | tr -d " "`
		imdbcontrol ca $user9 $Cluster $MBXID_Sanity9 $user9 $user9 clear openwave.com M S default &>> trash
		
		MBXID_Sanity10="1"$(echo $RANDOM)
		MBXID_Sanity10=`echo $MBXID_Sanity10 | tr -d " "`
		imdbcontrol ca $user10 $Cluster $MBXID_Sanity10 $user10 $user10 clear &>> trash
		
		MBXID_Sanity11="1"$(echo $RANDOM)
		MBXID_Sanity11=`echo $MBXID_Sanity11 | tr -d " "`
		imdbcontrol ca $user11 $Cluster $MBXID_Sanity11 $user11 $user11 clear &>> trash
		
		MBXID_Sanity12="1"$(echo $RANDOM)
		MBXID_Sanity12=`echo $MBXID_Sanity12 | tr -d " "`
		imdbcontrol ca $user12 $Cluster $MBXID_Sanity12 $user12 $user12 clear &>> trash
		
		MBXID_Sanity13="1"$(echo $RANDOM)
		MBXID_Sanity13=`echo $MBXID_Sanity13 | tr -d " "`
		imdbcontrol ca $user13 $Cluster $MBXID_Sanity13 $user13 $user13 clear &>> trash
		
		MBXID_Sanity14="1"$(echo $RANDOM)
		MBXID_Sanity14=`echo $MBXID_Sanity14 | tr -d " "`
		imdbcontrol ca $user14 $Cluster $MBXID_Sanity14 $user14 $user14 clear &>> trash
		
		MBXID_Sanity20="1"$(date +%S%k%M%S)
		MBXID_Sanity20=`echo $MBXID_Sanity20 | tr -d " "`
		imdbcontrol ca $user20 $Cluster $MBXID_Sanity20 $user20 $user20 clear 
		if [ "$setuptype" == "stateless" ]
		then
		MBXID_Sanity21="1"$(date +%S%k%M%S)
		MBXID_Sanity21=`echo $MBXID_Sanity21 | tr -d " "`
		imdbcontrol ca $user21 $FirstMSS $MBXID_Sanity21 $user21 $user21 clear 

		MBXID_Sanity22="1"$(date +%S%k%M%S)
		MBXID_Sanity22=`echo $MBXID_Sanity22 | tr -d " "`
		imdbcontrol ca $user22 $SecondMSS $MBXID_Sanity22 $user22 $user22 clear 
		fi
		MBXID_Sanity23="1"$(date +%S%k%M%S)
		MBXID_Sanity23=`echo $MBXID_Sanity23 | tr -d " "`
		imdbcontrol ca $user23 $Cluster $MBXID_Sanity23 $user23 $user23 clear

		MBXID_Sanity24="1"$(date +%S%k%M%S)
		MBXID_Sanity24=`echo $MBXID_Sanity24 | tr -d " "`
		imdbcontrol ca $user24 $Cluster $MBXID_Sanity24 $user24 $user24 clear 		

		MBXID_Sanity25=456
		imdbcontrol ca $user25 $Cluster $MBXID_Sanity25 $user25 $user25 clear 	

		MBXID_Sanity27="1"$(date +%S%k%M%S)
		MBXID_Sanity27=`echo $MBXID_Sanity27 | tr -d " "`
		imdbcontrol ca $user27 $Cluster $MBXID_Sanity27 $user27 $user27 clear 		
		
		userscnt=$(imdbcontrol la | grep -i @ | grep -i Sanity111111 | wc -l)
		userscnt=`echo $userscnt | tr -d " "`
		
		if [ "$setuptype" == "stateless" ]
		then
		usercnt_check=21
		echo
		echo " Total users are created $userscnt. Expected was 21"
		elif [ "$setuptype" == "stateful" ]
		then
		usercnt_check=19
		echo
		echo " Total users are created $userscnt. Expected was 19"
		fi	
		
		
		if [ "$userscnt" > "14" ]
		then
			echo
			echo "Successfully Created Users"
			Result="0"
			summary "User created succesfully" $Result
		else
			echo
			echo "Users are not created properly. Please check Manually"
			Result="1"
			summary "User are not created succesfully" $Result
			cleanup
			exit
		fi

		echo
		echo "Creating mailboxes..."
		start_time_tc
		imboxcreate $Cluster $MBXID_Sanity1 > verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity2 >> verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity3 >> verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity4 >> verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity5 >> verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity6 >> verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity7 >> verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity8 >> verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity9 >> verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity10 >> verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity11 >> verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity12 >> verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity13 >> verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity14 >> verify_mbx_creat.txt

		imboxcreate $Cluster $MBXID_Sanity20 >> verify_mbx_creat.txt
		if [ "$setuptype" == "stateless" ]
		then
		imboxcreate $FirstMSS $MBXID_Sanity21 >> verify_mbx_creat.txt
		imboxcreate $SecondMSS $MBXID_Sanity22 >> verify_mbx_creat.txt
		
		fi
		imboxcreate $Cluster $MBXID_Sanity23 >> verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity24 >> verify_mbx_creat.txt
		imboxcreate $Cluster $MBXID_Sanity25 >> verify_mbx_creat.txt
		


		#Checking for cores..#
		echo
		echo "Please wait while we are checking for any cores..."
		cores=$(find $INTERMAIL/tmp -name "*core.*" | wc -l)
		cores=`echo $cores | tr -d " "`
		find $INTERMAIL/tmp -name "*core.*" > cores_found.txt
		if [ "$cores" == "0" ]
		then
			echo
			echo "No cores were found."
			echo "Processing Further..."
			if [ "$setuptype" == "stateless" ]
		    then
		       mbx_check=20
			   
		    elif [ "$setuptype" == "stateful" ]
		       then
		           mbx_check=18
				
		    fi	
			
			## Checking for number of mailboxes created ##
			mbx_crtd=$(cat verify_mbx_creat.txt | grep -i created | wc -l)
			mbx_crtd=`echo $mbx_crtd | tr -d " "`
			
			if [ "$setuptype" == "stateless" ]
		    then
		       
			   echo " Total mail box are created $mbx_crtd. Expected was 20"
		    elif [ "$setuptype" == "stateful" ]
		       then
		           
				echo " Total mail box are created $mbx_crtd. Expected was 18"
		    fi	
			if [ "$mbx_crtd" > "14" ]
			then
				echo 
				echo "Mailboxes were created successfully."
				echo
				echo "<<<<<<<<==========>>>>>>>>"
				echo
				echo "Processing Further..."
				Result="0"
				summary "Mail box creation" $Result
				echo
			### If less than 14 mailboes are created
			else
				echo
				echo "Less than 14 mailboxes were created. Can't Process Further. Total mail boxes created $mbx_crtd"
				echo "Please check if any issues are there with imboxcreate utility"
				Result="1"
				summary "Mail box creation" $Result
				cleanup
				exit
				echo
			fi
		fi
	if [ "$setuptype" == "stateless" ]
		then
		    Mxos=$(cat $INTERMAIL/config/config.db | grep -i mxosHttpHostname | cut -d "[" -f2 | cut -d "]" -f1)
			Mxos=`echo $Mxos | tr -d " "`
			if [ "$Mxos" != "" ]
			then
			    Mxos_attribute_set
			fi
		fi
}

function stateful() {

		echo "---------------------------------------------------"
		echo "| QUICK SANITY TOOL FOR STATEFUL EMAILMx EDITION  |"
		echo "|                  VERSION = $imsanity_version                  |"
		echo "|       		 Created by Mx QE Team          |"
		echo "---------------------------------------------------"
		echo
		echo "==============================================================="
		echo "IMSANITY TOOL STARTED AT ====> "`date`
		echo "==============================================================="
		echo
		echo "Before starting Imsanity your servers should be up."
		echo "Please check all points are mounted"
		echo
		echo "Gathering information required to run this Sanity tool."
		echo "Please wait....."
		echo

		MSSHost=$(cat config/config.db | grep -i mss_run  | grep -i on | cut -d "/" -f2)
		MTAHost=$(cat config/config.db | grep -i mta_run  | grep -i on | cut -d "/" -f2 | tail -1)
		SMTPPort=$(cat config/config.db | grep -i smtpport | grep -v ssl | grep -v outbound | cut -d "[" -f2 | cut -d "]" -f1| tail -1)
		POPHost=$(cat config/config.db | grep -i popserv_run  | grep -i on | cut -d "/" -f2 | tail -1)
		POPPort=$(cat config/config.db | grep -i pop3port | grep -v ssl | cut -d "[" -f2 | cut -d "]" -f1 | head -1)
		IMAPHost=$(cat config/config.db | grep -i imapserv_run  | grep -i on | cut -d "/" -f2)
		IMAPPort=$(cat config/config.db | grep -i imap4port | grep -v ssl | grep -v improxy | cut -d "[" -f2 | cut -d "]" -f1 | head -1)
		BackendType=$(imconfget -h $MSSHost -m mss databaseType)
		

		echo "============================================================"
		echo "MSS Server      ==> "$MSSHost
		echo "MTA Server      ==> "$MTAHost" Port ==> "$SMTPPort
		echo "POP Server      ==> "$POPHost" Port ==> "$POPPort
		echo "IMAP Server     ==> "$IMAPHost" Port ==> "$IMAPPort
		echo "DB Used         ==> "$BackendType
		echo "============================================================"
		
		#call of function stateful_stateless for setup independent features

		stateful_stateless

		immsgmassmail_utility
		
		mailbox_creation_immsgmassmail
    
}
 
function stateless() {
		
		echo "---------------------------------------------------"
		echo "| QUICK SANITY TOOL FOR STATELESS EMAILMx EDITION |"
		echo "|                  VERSION = $imsanity_version                  |"
		echo "|       	Created by Mx QE Team          |"
		echo "---------------------------------------------------"
		echo 
		echo "==============================================================="
		echo "IMSANITY TOOL STARTED AT ====> "`date`
		echo "==============================================================="
		echo
		echo "Before starting Imsanity your servers should be up."
		echo "Please check all points are mounted"
		echo
		echo "Gathering information required to run this Sanity tool."
		echo "Please wait....."
		echo
		site_clust=$(cat config/config.db | grep -i statelessMSS | cut -d "/" -f2)
		Site=$(cat config/config.db | grep -i statelessMSS | cut -d "/" -f2 | cut -d "-" -f1)
		Cluster=$(cat config/config.db | grep -i statelessMSS | cut -d "/" -f2 | cut -d "-" -f2)
		FirstMSS=$(imconfget groupDefinition | grep "$site_clust" | cut -d ":" -f2 | cut -d " " -f1)
		SecondMSS=$(imconfget groupDefinition | grep "$site_clust" | cut -d ":" -f2 | cut -d " " -f2)
		MTAHost=$(cat config/config.db | grep -i mta_run | grep -i on |  cut -d "/" -f2| tail -1)
		SMTPPort=$(cat config/config.db | grep -i smtpport | grep -v ssl | grep -v outbound | cut -d "[" -f2 | cut -d "]" -f1|head -1 | tail -1)
		POPHost=$(cat config/config.db | grep -i popserv_run | grep -i on |  cut -d "/" -f2 | tail -1)
		POPPort=$(cat config/config.db | grep -i pop3port | grep -v ssl | cut -d "[" -f2 | cut -d "]" -f1 | head -1)
		#IMAPHost=$(cat config/config.db | grep -i imap4port | grep -v ssl | grep -v improxy | cut -d "[" -f2 | cut -d "]" -f1 | tail -1)
		IMAPHost=$(cat config/config.db | grep -i imapserv_run | grep -i on | cut -d "/" -f2)
		IMAPPort=$(cat config/config.db | grep -i imap4port | grep -v ssl | grep -v improxy | cut -d "[" -f2 | cut -d "]" -f1 | head -1)
		BackendType=$(imconfget hostInfo | cut -d "=" -f2 | cut -d ":" -f1)
		BackendCluster=$(imconfget hostInfo | cut -d "=" -f2 | cut -d ":" -f2)
		BackendPort=$(imconfget hostInfo | cut -d "=" -f2 | cut -d ":" -f3)
		RMEPort=$(cat config/config.db | grep -i extrmeport| cut -d "[" -f2 | cut -d "]" -f1)
		extHost=$(cat config/config.db | grep -i extserv_run | grep -i on | cut -d "/" -f2)

		echo "============================================================"
		echo "Site            ==> "$Site
		echo "MSS Cluster     ==> "$Cluster
		echo "DB Used         ==> "$BackendType
		echo "DB Cluster/Host ==> "$BackendCluster
		echo "DB Port         ==> "$BackendPort
		echo "MSS-1           ==> "$FirstMSS
		echo "MSS-2           ==> "$SecondMSS
		echo "MTA Server      ==> "$MTAHost" Port ==> "$SMTPPort
		echo "POP Server      ==> "$POPHost" Port ==> "$POPPort
		echo "IMAP Server     ==> "$IMAPHost" Port ==> "$IMAPPort
		echo "============================================================"
		echo
		echo > summary.log
		TotalTestCases=0
		Pass=0
		Fail=0

		#call of function stateful_stateless for setup independent features
		stateful_stateless
		#imboxcopy utility verification from cluster to mss
		#imboxcopy_cluster_mss

		#imboxcopy utility verification from mss to cluster

		#imboxcopy_mss_cluster

		#imboxcopy utility verification from mss to mss

		#imboxcopy_mss_mss

		

		# mailbox creation through smtp using surrogate

		mailbox_creation_smtp2

		# mailbox creation through pop using surrogate

		mailbox_creation_pop2

		# mailbox creation through imap using surrogate

		mailbox_creation_imap2

		# mailbox creation through immtamassmail using surrogate

		#mailbox_creation_immtamassmail2

		# mailbox creation through immsgtrace using surrogate

		#mailbox_creation_immsgtrace2

		

		
		

		

		##MSS client can recognise when MSS is down

		ITS_1318609
		
		#hard deltion of message from cassendra

		#deletion_cassendra 

		
		#utility imboxcopy test

		imboxcopy_utility 
	    
		
		Mxos=$(cat $INTERMAIL/config/config.db | grep -i mxosHttpHostname | cut -d "[" -f2 | cut -d "]" -f1)
		Mxos=`echo $Mxos | tr -d " "`
		if [ "$Mxos" != "" ]
		then
			 
		
	    #Testing of FEP unsymmetric hash range scenario
		
		#commented as now Nginx do load balancing
		#fep_unsymmetric
		
		#VERIFICATION OF FEP HASH RANGE MODIFICATION SCENARIO
		
		#commented as now Nginx do load balancing
		##fep_hash_range_utility
		
		# Testing when the owner MSS is down , SMTP/POP/IMAP connected through same surrogate

		owner_mss_down_imap_pop_mta
		
		#PERFORMING SMTP OPERATIONS WITH SURROGATE MSS

		surrogate_testing 
		fi
		
}

function stateful_stateless() {
						
		#call function to create users and mail box
		if [ "$setuptype" == "stateless" ]
		then
		
		creation_mailbox_user_stateless	
		elif [ "$setuptype" == "stateful" ]
		then
		#creation_mailbox_user_stateful
		Cluster=$MSSHost
		creation_mailbox_user_stateless	
		fi	
		
		set_config_keys
		#check version of each server and MX
		MXVERSION
		
		#call function for smtp session
		smtp_operation 
		#check for uid and all
		imap_check
	    #call function for pop operations
		pop_operations 
		#call function for imap operations
		imap_operations	
	
		#utility imboxstats test
		imboxstats_utility 		
		#utility imfolderlist test
		imfolderlist_utility 
		#utility immssdescms test
		immssdescms_utility 
		
		#utility immsgdump test
		immsgdump_utility 	
		#utility immsgfind test
		immsgfind_utility 
		
		#utility immsgdelete test
		immsgdelete_utility 
		#utility imimapboxcopy test
		imimapboxcopy_utility 
		# Checking delivery of large messages and their retreival

		large_msg_delivery

		#utility imboxtest test
		imboxtest_utility

		#utility imboxget test

		imboxget_utility

		#utility iminvctrl test

		iminvctrl_utility

		#utility imboxdirdelete utility test

		imboxdirdelete_utility

		#utility immsscmpms utility test

		immsscmpms_utility

		#Testing of ldapadd 

		ldap_add_test

		#Testing of ldapsearch 

		ldap_search_utilty

		#Testing of ldapmodify  

		ldap_modify_utility

		#Testing of ldapdelete  

		ldap_delete_utility

		#Testing that no flags are set for messages when checked through imap in second consecutive session  

		no_flag_msg

		### Testing new immtamassmail utility for local users 

		immtamassmall_utility

        ## Testing utilities of apitest

		api_test_2

		##"rm" shows msgId attribute value as "<NULL>" for messages sent through SMTP telnet session.

		rm_api_test

		##Fetch(envelope) in IMAP

		ITS_1329310

		##Rename command in IMAP

		ITS_1326578

		## immsgdelete utility for single message

		ITS_1319590

		##  ITS - Issue 1324873 

		## imap fetch rfc822 need not read header summary column/file on cassandra

		ITS_1324873

		# mailbox creation through smtp

		mailbox_creation_smtp

		# mailbox creation through pop

		mailbox_creation_pop

		# mailbox creation through imap

		mailbox_creation_imap

		# mailbox creation through immtamassmail

    	mailbox_creation_immtamassmail

		# mailbox creation through immtamassmail

		mailbox_creation_immsgtrace

		

		sleep 15

		#Check forIMAP move

		imap_move 

		#Verify UID shown by copy command in IMAP session

		uid_imap 

		#Check for imap fetch full

		imap_fetch_full 

		#Check for IMAP IDLE

		imap_idle 

		#Check for imap get quota root

		imap_getquota_root 

		#check imap sort

		imap_sort 

		

		#Check for bounced messages when message number quota is hit.

		#bounced_msg_numberquota 

		#bounced_folderquota_msg_kb 

		
		##bounced_msg_maint 



								

		map_tc

		change_smtp_add

		setaccount_status

		password_chnage_ac

		create_alias

		delete_alias

		multiple_alias

		create_account_fwd

		list_account_fwd

		delete_account_fwd

		default_domain

		create_cos

		delete_cos

		show_cos	

		imap_invalid_tag

		stored_command_before_login

		

		

		

		#verify new cos creation

		verify_newcos 

		#uiltity imcheckfq test

		imcheckfq_utility 

		#utility iminboxlist test

		iminboxlist_utility  

		#core verify when mss and imqueserver are stop

		#mss_imqueuserver_stop  

		#expunge of message ,cache inconsistence

		expunge_cache_inconsistence 

		#utility immssinvlist test

		immssinvlist_utility 

		#test for account creation with mode M

		account_mode_M 

		#utiltiy immssdescma and shared message verification

		immssdescma_utility_sharedmsg 

		#utility immsg trace test

		immsgtrace_utility 

		#check for keepalive=1 Invalid key value] in mss log for IMAP

		invalid_keyvalue_msslog_imap 

		#check for keepalive=1 Invalid key value] in mss log for POP

		invalid_keyvalue_msslog_pop 

		#check mail delivery by freezing owner mss

		##mail_freezing_owner_mss 

		#utility immtacheck test

		immtacheck_utility 

		#Negative test case for incorrect message delete

		delete_invalid_msg 

		#Negative test for incorrect message ID in error message for long message id

		delete_invalid_long_msgid 

		#Negative test for incorrect message ID in error message for small message id

		delete_invalid_samll_msgid 

		#utility imboxattctrl test

		imboxattctrl_utility 

		#Verify accoutn creation with new domain and cos

		acc_creation_new_cos_domain 

		#utility imfilterctrl test

		imfilterctrl_utility 
      
		#Check for mailing list 
        
		mailing_list 

		#imcl api test 

		imcl_api_test 

		#api test 

		api_test 

	

		#utility imboxmaint test

		imboxmaint_utility
		
}

function mail_send() {
				
				user_mail=$1
				mail_size=$2
				mail_count=$3
				
				#Information related to SMTP session			
				SUBJECT="Sanity Test"
				user_mail_name=$(whoami)
				MAILFROM=`imdbcontrol la | grep -i @ | grep -i $user_mail_name | cut -d ":" -f2 | cut -d "@" -f1`
				MAILFROM=`echo $MAILFROM | tr -d " "`
				
				if [ "$mail_size" == "small" ]
				then
				DATA="Test message for Sanity Tool"
				elif [ "$mail_size" == "large" ]
				then
				ls -lrtR $INTERMAIL/../ > large_msg
				DATA=`cat large_msg`
				fi	 
				
				#SMTP Operations startes from here
				echo "=========================="
				echo "PERFORMING SMTP OPERATIONS"
				echo "=========================="
				echo 
				## Single recipient delivery
				echo "-------Single Recipient Delivery Starts--------"
				echo
				c="1"
				echo "Connecting to $MTAHost on Port $SMTPPort";
				echo "Please wait ... "
				echo			
				exec 3<>/dev/tcp/$MTAHost/$SMTPPort
				
				while [ $c -le $mail_count ]  
				do
					echo -en "MAIL FROM:$MAILFROM\r\n" >&3
					echo -en "RCPT TO:$user_mail\r\n" >&3
					echo -en "DATA\r\n" >&3
					echo -en "Subject: $SUBJECT\r\n\r\n" >&3
					echo -en "$DATA\r\n" >&3
					echo -en ".\r\n" >&3
					(( c++ ))
				done
				echo -en "QUIT\r\n" >&3
				cat <&3 > mail.txt
				echo
				echo "-------Single Recipient Delivery Ends--------"
				
}

function summary() {
					func_name=$1
					value=$2
					ITS_no=$3
					interval_time_end_tc=$(date +\%s)
					interval_time_total_tc=$(expr $interval_time_end_tc - $interval_time_start_tc)
					total_min_tc=60		
					interval_time_total_min_tc=$(echo "scale=2; $interval_time_total_tc / $total_min_tc" | bc)

					if [ "$func_name" == "summary_imsanity_tc" ]
					then 
					echo " Total number of Test Cases : "$TotalTestCases >> summary.log
					echo " PASS : "$Pass >> summary.log
					echo " FAIL : "$Fail >> summary.log
					else
						if [ "$value" == "0" ]
						then
						echo " $func_name ..................................................: [PASS] (Time Taken : $interval_time_total_tc Sec.)" >> summary.log
						Pass=$(($Pass+1))
						TotalTestCases=$(($TotalTestCases+1))
						else
						echo " "$func_name" ................................................: [FAIL] (Time Taken : $interval_time_total_tc Sec.) $ITS_no" >> summary.log
						Fail=$(($Fail+$value))
						TotalTestCases=$(($TotalTestCases+$value))
						fi
					fi
} 

function Mxos_attribute_set() {
    
	imdbcontrol sac $user1 openwave.com mailallowedaliasdomains openwave.com
    imdbcontrol sac $user2 openwave.com mailallowedaliasdomains openwave.com
    imdbcontrol sac $user3 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user4 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user5 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user6 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user7 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user8 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user9 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user10 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user11 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user12 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user13 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user14 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user20 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user21 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user22 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user23 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user24 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user25 openwave.com mailallowedaliasdomains openwave.com
	imdbcontrol sac $user27 openwave.com mailallowedaliasdomains openwave.com
	DIRHost=$(cat config/config.db | grep -i dirserv_run  | grep -i on | cut -d "/" -f2)						 
    DIRPort=$(cat config/config.db | grep -i ldapPort | grep -v cache  |cut -d "[" -f2 | cut -d "]" -f1)	
   	
	echo "gaf $user1 openwave.com " > abc.ldif
	 
	for((i=1;i<=14;i++))
	do
	echo "gaf Sanity111111"$i" openwave.com " > abc.ldif
	imldapsh -D cn=root -W secret -P $DIRPort  -H $DIRHost -F abc.ldif > temporary.txt 
    value=$(cat temporary.txt | grep -i mailallowedaliasdomain | wc -l )
	
	if [ "$value" == "1" ]
	then
	    echo " Attribute mailallowedaliasdomain is set for Sanity111111"$i 
	else
	    echo " Attribute mailallowedaliasdomain is not set for Sanity111111"$i 
	fi
    done
	
	for((i=20;i<=27;i++))
	do
	if [ "$i" != "26" ]
	then
	echo "gaf Sanity111111"$i" openwave.com " > abc.ldif
	imldapsh -D cn=root -W secret -P $DIRPort  -H $DIRHost -F abc.ldif > temporary.txt 
    value=$(cat temporary.txt | grep -i mailallowedaliasdomain | wc -l )
	
	if [ "$value" == "1" ]
	then
	    echo " Attribute mailallowedaliasdomain is set for Sanity111111"$i 
	else
	    echo " Attribute mailallowedaliasdomain is not set for Sanity111111"$i 
	fi
	
	fi
    done
	rm -rf abc.ldif temporary.txt
}

##  To delete all the messages of inbox of a particular user . As in future immsgdelete utility will be depricated

function deleteMessages() {
    username=$1
	password=$2
	mailboxId=$3
	
    echo > list1.txt
	exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

	echo -en "a login $username@openwave.com $password\r\n" >&3
	echo -en "a list \"\" *\r\n" >&3
	echo -en "a logout\r\n" >&3
	echo "+++++++++++++++++++++++++"
	cat <&3 >>list1.txt
	echo "+++++++++++++++++++++++++"
	echo
	
	noOfFolders=$(cat list1.txt | grep -i list | wc -l )	
	#echo " no of folders == "$noOfFolders
	
	
	for((i=1;i<=(($noOfFolders-1));i++))
    do
       fold=$(cat list1.txt | grep -i list | cut -d " " -f5 | cut -d "\"" -f2 | head -$i)
    done

	echo $fold > new.txt
    for((i=1;i<=(($noOfFolders-1));i++))
        do
           folder[$i]=$(cat new.txt | cut -d " " -f$i)
           #echo ${folder[$i]}
    done

	
	exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

	echo -en "a login $username $password\r\n" >&3
	for((i=1;i<=(($noOfFolders-1));i++))
    do
	   echo -en "a select ${folder[$i]}\r\n" >&3
	   echo -en "a store 1:* +flags (\Deleted)\r\n" >&3
	   echo -en "a expunge\r\n" >&3
	   echo -en "a close\r\n" >&3
	done
	echo -en "a logout\r\n" >&3
	echo "+++++++++++++++++++++++++"
	cat <&3 >> list1.txt 
	echo "+++++++++++++++++++++++++"
	echo
	
	imboxstats $mailboxId > boxstats_Sanity6.txt
	#cat boxstats_Sanity6.txt
	msgs_user=$(grep "Total Messages Stored"  boxstats_Sanity6.txt | cut -d " " -f6)
	msgs_user=`echo $msgs_user | tr -d " "`
	echo "============================================"
	if [ "$msgs_user" == "0" ]
	then 
		echo
		echo " Mailbox emptied successfully."
	else
		echo
		echo "ERROR:  Mails are not deleted"
		echo "ERROR: Please check this Manually."
	fi
	rm -rf list1.txt new.txt
}

#############################################All functions define end here#############################################################
 
#############################################All TEST CASES START HERE#################################################################
function MXVERSION() {
start_time_tc
								echo "###################"
								echo "|   Version of MX |"		
								echo "###################"
							    								
								msg_msg_ver2=$(imdbcontrol -version | grep -i version | cut -d " " -f2)
								msg_msg_ver3=$(imdbcontrol -version | grep -i version | cut -d " " -f3)
								
								msg_msg_ver=$msg_msg_ver2$msg_msg_ver3
										
								msg_msg_ver1=$(cat config/config.db | grep -i IntermailVersion | tail -1 | cut -d "[" -f2 | cut -d "]" -f1 | tr -d " " | grep "$msg_msg_ver" | wc -l)
															
								 if [ $msg_msg_ver1 == "1" ]
								then
									echo "Version of the MX is : $msg_msg_ver "
									Result="0"
									echo 
								else
									
									echo "ERROR: We are not able to find MX version"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								summary "Veriosn of MX" $Result
								start_time_tc
								mss=$(lib/mss -version | tr -d " "| grep "$msg_msg_ver" | wc -l)
								 if [ $mss == "1" ]
								then

									echo "Version of the mss is : $msg_msg_ver  "
									Result="0"
									echo 
								else
									
									echo "ERROR: We are not able to find mss version"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								summary "Version of mss" $Result
								start_time_tc
								mta=$(lib/mss -version | tr -d " "| grep "$msg_msg_ver" | wc -l)
								 if [ $mta == "1" ]
								then

									echo "Verions of the mta : $msg_msg_ver "
									Result="0"
									echo 
								else
								
									echo "ERROR: We are not able to find mta version"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								summary "Version of mta" $Result
								start_time_tc
								pop=$(lib/popserv -version | tr -d " "| grep "$msg_msg_ver" | wc -l)
								 if [ $pop == "1" ]
								then

									echo "Verions of the pop : $msg_msg_ver"
									Result="0"
									echo 
								else
									
									echo "ERROR: We are not able to find pop version"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								summary "Version of pop" $Result
								start_time_tc
								imap=$(lib/imapserv -version | tr -d " "| grep "$msg_msg_ver" | wc -l)
								 if [ $imap == "1" ]
								then

									echo "Verions of the imap : $msg_msg_ver "
									Result="0"
									echo 
								else
									
									echo "ERROR: We are not able to find imap version"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								summary "Version of imap" $Result
								start_time_tc
								ext=$(lib/imextserv -version | tr -d " "| grep "$msg_msg_ver" | wc -l)
								 if [ $ext == "1" ]
								then

									echo "Verions of the ext is : $msg_msg_ver "
									Result="0"
									echo 
								else
									
									echo "ERROR: We are not able to find extension server version"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								summary "Version of extension" $Result
								start_time_tc
								queu=$(lib/imqueueserv -version | tr -d " "| grep "$msg_msg_ver" | wc -l)
								 if [ $queu == "1" ]
								then

									echo "Verions of the queu server is : $msg_msg_ver "
									Result="0"
									echo 
								else
									
									echo "ERROR: We are not able to find queu server version"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								summary "Version of queuserver" $Result
								start_time_tc
								dir1=$(cat config/config.db | grep -i OPWVDirVersion  | tail -1 | cut -d "[" -f2 | cut -d "]" -f1)
								dir=$(lib/imdirserv -version | grep "$dir1" | wc -l)
								 if [ $dir == "1" ]
								then

									echo "Verions of the dirserver is :$msg_msg_ver"
									Result="0"
									echo 
								else
									
									echo "ERROR: We are not able to find dirserver version"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								summary "Version of dirserver" $Result
								start_time_tc
								gre=$(lib/immgrserv -version | tr -d " "| grep "$msg_msg_ver" | wc -l)
								 if [ $gre == "1" ]
								then

									echo "Verions of the greserver is : $msg_msg_ver "
									Result="0"
									echo 
								else
									
									echo "ERROR: We are not able to find MX version"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								summary "Version of greserver" $Result
}

function smtp_operation() {
				#taking start time of tc
				start_time_tc smtp_operation_tc 
				
				#Information related to SMTP session
				DATA="Test message for Sanity Tool"
				SUBJECT="Sanity Test"
				user_mail_name=$(whoami)
				MAILFROM=`imdbcontrol la | grep -i @ | grep -i $user_mail_name | cut -d ":" -f2 | cut -d "@" -f1`
				MAILFROM=`echo $MAILFROM | tr -d " "`
				#MAILFROM="imail1"
				ls -lrtR $INTERMAIL/../ > large_msg
				LARGE_DATA=`cat large_msg`

				#SMTP Operations startes from here
				echo "=========================="
				echo "PERFORMING SMTP OPERATIONS"
				echo "=========================="
				echo 


				## Single recipient delivery
				echo "-------Single Recipient Delivery Starts--------"
				echo
				echo "Connecting to $MTAHost on Port $SMTPPort";
				echo "Please wait ... "
				echo

				exec 3<>/dev/tcp/$MTAHost/$SMTPPort

				echo -en "MAIL FROM:$MAILFROM\r\n" >&3
				echo -en "RCPT TO:$user1\r\n" >&3
				echo -en "DATA\r\n" >&3
				echo -en "Subject: $SUBJECT\r\n\r\n" >&3
				echo -en "$DATA\r\n" >&3
				echo -en ".\r\n" >&3
				echo -en "MAIL FROM:$MAILFROM\r\n" >&3
				echo -en "RCPT TO:$user1\r\n" >&3
				echo -en "DATA\r\n" >&3
				echo -en "Subject: $SUBJECT\r\n\r\n" >&3
				echo -en "$DATA\r\n" >&3
				echo -en ".\r\n" >&3
				echo -en "QUIT\r\n" >&3
				cat <&3
				echo
				echo "-------Single Recipient Delivery Ends--------"
				sleep 10
				imboxstats $MBXID_Sanity1 > boxstats_Sanity1.txt
				cat boxstats_Sanity1.txt
				

				## Multi-recipient delivery
				echo
				echo "-------Multi-Recipient Delivery Starts--------"
				echo
				echo "Connecting to $MTAHost on Port $SMTPPort";
				echo "Please wait ... "
				echo

				exec 3<>/dev/tcp/$MTAHost/$SMTPPort
				 
				echo -en "MAIL FROM:$MAILFROM\r\n" >&3
				echo -en "RCPT TO:$user1\r\n" >&3
				echo -en "RCPT TO:$user2\r\n" >&3
				echo -en "DATA\r\n" >&3
				echo -en "Subject: $SUBJECT\r\n\r\n" >&3
				echo -en "$DATA\r\n" >&3
				echo -en ".\r\n" >&3
				echo -en "MAIL FROM:$MAILFROM\r\n" >&3
				echo -en "RCPT TO:$user1\r\n" >&3
				echo -en "RCPT TO:$user2\r\n" >&3
				echo -en "DATA\r\n" >&3
				echo -en "Subject: $SUBJECT\r\n\r\n" >&3
				echo -en "$DATA\r\n" >&3
				echo -en ".\r\n" >&3
				echo -en "QUIT\r\n" >&3
				cat <&3	
				imboxstats $MBXID_Sanity1 > boxstats_Sanity1.txt
				cat boxstats_Sanity1.txt                          
						
				## Large Message Delivery Starts
				echo
				echo "-------Large Message Delivery Sessions Starts--------"
				echo 
				echo "Connecting to $MTAHost on Port $SMTPPort";
				echo "Please wait ... "
				echo
				exec 3<>/dev/tcp/$MTAHost/$SMTPPort

				echo -en "MAIL FROM:$MAILFROM\r\n" >&3
				echo -en "RCPT TO:$user6\r\n" >&3
				echo -en "DATA\r\n" >&3
				echo -en "Subject: $SUBJECT\r\n\r\n" >&3
				echo -en "$LARGE_DATA\r\n" >&3
				echo -en ".\r\n" >&3
				echo -en "QUIT\r\n" >&3	
				cat <&3	 
 					 				
				echo			
				imboxstats $MBXID_Sanity6 > boxstats_Sanity6.txt
				msgs_user6=$(grep "Total Messages Stored"  boxstats_Sanity6.txt | cut -d " " -f6)
				msgs_user6=`echo $msgs_user6 | tr -d " "`
				echo "============================================"
				if [ "$msgs_user6" == "1" ]
				then 
					echo
					echo $msgs_user6" Mails were delivered successfully."
					Result="0"
				else
					echo
					echo "ERROR: "$msgs_user6" Mails were delivered only."
					echo "ERROR: Mails delivery failed. Please check this Manually."
					Result="1"
				fi

				echo
				echo "-------Large Message Delivery Sessions Ends--------"
				echo
				summary "Large message" $Result
				## Large Message Delivery Ends

				## Check if mails were delivered successfully or not
				imboxstats $MBXID_Sanity1 > boxstats_Sanity1.txt
				cat boxstats_Sanity1.txt
				msgs=$(grep "Total Messages Stored"  boxstats_Sanity1.txt | cut -d " " -f6)
				msgs=`echo $msgs | tr -d " "`
				if [ "$msgs" > "2" ]
				then
					echo
					echo $msgs" Mails were delivered successfully."
					Result="0"
					echo "============================================"			
					
					## Check for mta errors in logs
					cat $INTERMAIL/log/mta.log| egrep -i "erro;|urgt;|fatl;" > mta_errors.txt
					echo 
					echo "Errors logged in MTA Logs:"
					echo "=========================="
					cat mta_errors.txt
					echo
					echo "=========================="
					echo


					### Check for mss used for delivery
					mss_used_mta=$(cat $INTERMAIL/log/mta.log| egrep -i delivered | cut -d ":" -f4 | cut -d "=" -f2 | sort -u)
					echo
					echo "MSS used for smtp operations was on host: "$mss_used_mta
					#Checking for cores..#
					echo
					echo "Please wait while we are checking for any cores..."
					cores=$(find $INTERMAIL/tmp -name "*core.*" | wc -l)
					cores=`echo $cores | tr -d " "`
					find $INTERMAIL/tmp -name "*core.*" > mta_cores_found.txt
					if [ "$cores" == "0" ]
					then
						echo
						echo "No cores were found."
						echo
						echo "-----X-X  SMTP OPERATIONS COMPLETED  X-X-----"
						echo 
					else
						echo "Following cores were found:"
						echo "==========================="
						cat mta_cores_found.txt
						echo "==========================="
						echo "Hence, can't do processing Further."
						Result="1"
						summary "Mail delivery" $Result
						cleanup
					
						echo
					fi					
					### If mails are not delivered successfully then.
				else 
					echo
					echo "Mails were not delivered to the mailboxes."
					echo "Please check mail delivery manually to confirm if SMTP operations are working fine or not."
					echo
					Result="1"

					summary "Mail delivery" $Result
										
					Count_failure=1 
					
					for Count_failure in 1
					do
					echo "One round of cleanup again from Delete.sh"
					
					sh Imsanity_Tool_3.1/delete.sh&
					
					echo "Clean up finished from Delete.sh"
					
					echo "Re-startign servers"
					~/lib/imservctrl killStart
					
					echo "Re-trying for Sanity"
					Count_failure=2
					#call function imsanity_version to show the version of tool
					imsanity_version
		
					#call function log deletion to delete log
					#log_delete

					#call fucntion checksetuptype to cehck whether setup is stateless or stateful
					check_setup_type
		
					#call clean up fucntion for setup clean up
					cleanup 
					done
					cleanup
				fi
				
				#taking end time of tc
				#end_time_tc smtp_operation_tc
				summary "Mail delivery" $Result
}

function pop_operations() {
						#taking start time of tc
						start_time_tc pop_operations_tc 

						## POP OPERATIONS START FROM HERE
						echo
						echo "==========================="
						echo " PERFORMING POP OPERATIONS "
						echo "==========================="
						echo
						echo "Connecting to $POPHost on Port $POPPort";
						echo "Please wait ... "
						echo

						exec 3<>/dev/tcp/$POPHost/$POPPort
						echo -en "user $user1\r\n" >&3
						echo -en "pass $user1\r\n" >&3
						echo -en "list\r\n" >&3
						echo -en "retr 1\r\n" >&3
						echo -en "retr 2\r\n" >&3
						echo -en "retr 3\r\n" >&3
						echo -en "stat\r\n" >&3
						echo -en "uidl\r\n" >&3
						echo -en "top 1 1\r\n">&3  
						echo -en "dele 1\r\n" >&3
						echo -en "stat\r\n" >&3
						echo -en "retr 1\r\n" >&3
						echo -en "dele 2\r\n" >&3
						echo -en "stat\r\n" >&3
						echo -en "rset\r\n">&3
						echo -en "stat\r\n" >&3
						echo -en "dele 1\r\n" >&3
						echo -en "list\r\n" >&3
						echo -en "retr 1\r\n" >&3
						echo -en "quit\r\n" >&3
						echo "+++++++++++++++++++++++++"
						cat <&3
						echo "+++++++++++++++++++++++++"
						echo
						echo


						### Check for errors in pop logs
						cat $INTERMAIL/log/popserv.log| egrep -i "erro;|urgt;|fatl;" > pop_errors.txt
						echo "Errors logged in POP Logs:"
						echo "=========================="
						cat pop_errors.txt
						echo
						echo "=========================="


						### Check for mss used for pop operations
						mss_used_pop=$(cat $INTERMAIL/log/popserv.log | cut -d ":" -f3 | grep -i mss | cut -d "=" -f2 | sort -u)
						echo
						echo "MSS used for pop operations was on host: "$mss_used_pop
						#Checking for cores..#
						echo
						echo "Please wait while we are checking for any cores..."
						cores=$(find $INTERMAIL/tmp -name "*core.*" | wc -l)
						cores=`echo $cores | tr -d " "`
						find $INTERMAIL/tmp -name "*core.*" > pop_cores_found.txt
						if [ "$cores" == "0" ]
						then
							echo
							echo "No cores were found."
							echo
							Result="0"
							summary "POP Test case " $Result
							echo "-----X-X  POP OPERATIONS COMPLETED  X-X-----"
						### If cores seen during POP operations then,
						else
							echo
							echo "Following cores were found:"
							echo "==========================="
							cat pop_cores_found.txt
							echo "==========================="
							echo "Hence, can't do processing Further."
							Result="1"
							summary "POP Test case " $Result
							cleanup							
							echo
						fi
						#taking end time of tc
						#end_time_tc pop_operations_tc
						
						for ((i=1;i<11;i++))
							do
							summary "POP Test case $i" $Result
						done
}

function imap_operations() {
							start_time_tc imap_operations_tc
							## IMAP OPERATIONS START FROM HERE
							echo
							echo "=========================="
							echo "PERFORMING IMAP OPERATIONS"
							echo "=========================="
							echo
							echo "Connecting to $IMAPHost on Port $IMAPPort";
							echo "Please wait ... "
							echo

							exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

							echo -en "a login $user1@openwave.com $user1\r\n" >&3
							echo -en "a select INBOX\r\n" >&3
							echo -en "a fetch 1:* rfc822\r\n" >&3
							echo -en "a fetch 1:3 rfc822\r\n" >&3
							echo -en "a fetch 1 rfc822\r\n" >&3
							echo -en "a fetch 1,2,3 rfc822\r\n" >&3
							echo -en "a fetch 1:2,3:3 rfc822\r\n" >&3
							echo -en "a fetch 1:2,3 rfc822\r\n" >&3
							echo -en "a fetch 1:* body[text]\r\n" >&3
							echo -en "a fetch 1:3 body[text]\r\n" >&3
							echo -en "a fetch 1 body[text]\r\n" >&3
							echo -en "a fetch 1,2,3 body[text]\r\n" >&3
							echo -en "a fetch 1:2,3:3 body[text]\r\n" >&3
							echo -en "a fetch 1:2,3 body[text]\r\n" >&3
							echo -en "a fetch 1:* body[header]\r\n" >&3
							echo -en "a fetch 1:3 body[header]\r\n" >&3
							echo -en "a fetch 1 body[header]\r\n" >&3
							echo -en "a fetch 1,2,3 body[header]\r\n" >&3
							echo -en "a fetch 1:2,3:3 body[header]\r\n" >&3
							echo -en "a fetch 1:2,3 body[header]\r\n" >&3							
							echo -en "a fetch 1:* (uid)\r\n" >&3
							echo -en "a fetch 1:3 (uid)\r\n" >&3
							echo -en "a fetch 1 (uid)\r\n" >&3
							echo -en "a fetch 1,2,3 (uid)\r\n" >&3
							echo -en "a fetch 1:2,3:3 (uid)\r\n" >&3
							echo -en "a fetch 1:2,3 (uid)\r\n" >&3							
							echo -en "a fetch 1:* envelope\r\n" >&3
							echo -en "a fetch 1:3 envelope\r\n" >&3
							echo -en "a fetch 1 envelope\r\n" >&3
							echo -en "a fetch 1,2,3 envelope\r\n" >&3
							echo -en "a fetch 1:2,3:3 envelope\r\n" >&3
							echo -en "a fetch 1:2,3 envelope\r\n" >&3						
							echo -en "a fetch 1:* flags\r\n" >&3
							echo -en "a fetch 1:3 flags\r\n" >&3
							echo -en "a fetch 1 flags\r\n" >&3
							echo -en "a fetch 1,2,3 flags\r\n" >&3
							echo -en "a fetch 1:2,3:3 flags\r\n" >&3
							echo -en "a fetch 1:2,3 flags\r\n" >&3						
							echo -en "a list \"\" *\r\n" >&3
							echo -en "a create TEMP3\r\n" >&3
							echo -en "a copy 1:* TEMP3\r\n" >&3
							echo -en "a select TEMP3\r\n" >&3
							echo -en "a fetch 1:* rfc822\r\n" >&3
							echo -en "a fetch 1:* body[text]\r\n" >&3
							echo -en "a fetch 1:* body[header]\r\n" >&3
							echo -en "a fetch 1:* (uid)\r\n" >&3
							echo -en "a fetch 1:* envelope\r\n" >&3  
							echo -en "a fetch 1:* flags\r\n" >&3
							echo -en "a store 1:* +flags (\Deleted)\r\n" >&3
							echo -en "a expunge\r\n" >&3
							echo -en "a close\r\n" >&3
							echo -en "a examine INBOX\r\n" >&3
							echo -en "a select INBOX\r\n" >&3
							echo -en "a delete TEMP3\r\n" >&3
							echo -en "a create TEMP3\r\n" >&3
							echo -en "a copy 1:* TEMP3\r\n" >&3
							echo -en "a select TEMP3\r\n" >&3
							echo -en "a fetch 1:* rfc822\r\n" >&3
							echo -en "a fetch 1:* body[text]\r\n" >&3
							echo -en "a fetch 1:* body[header]\r\n" >&3
							echo -en "a fetch 1:* (uid)\r\n" >&3
							echo -en "a fetch 1:* envelope\r\n" >&3
							echo -en "a fetch 1:* flags\r\n" >&3
							echo -en "a select INBOX\r\n" >&3
							echo -en "a rename TEMP3 TEST3\r\n" >&3
							echo -en "a select TEST3\r\n" >&3
							echo -en "a fetch 1:* rfc822\r\n" >&3
							echo -en "a fetch 1:* body[text]\r\n" >&3
							echo -en "a fetch 1:* body[header]\r\n" >&3
							echo -en "a fetch 1:* (uid)\r\n" >&3
							echo -en "a fetch 1:* envelope\r\n" >&3
							echo -en "a fetch 1:* flags\r\n" >&3
							echo -en "a store 1:* +flags (\Deleted)\r\n" >&3
							echo -en "a expunge\r\n" >&3
							echo -en "a select INBOX\r\n" >&3
							echo -en "a append INBOX {2}\r\n" >&3
							echo -en "hi\n" >&3
							echo -en "a fetch 1:* rfc822\r\n" >&3
							echo -en "a search text \"Sanity\"\r\n" >&3
							echo -en "a search all\r\n" >&3
							echo -en "a NOOP\r\n" >&3
							echo -en "a check mailbox\r\n" >&3
							echo -en "a check\r\n" >&3
							echo -en "a create NEST1/NEST2/NEST3\r\n" >&3
							echo -en "a delete NEST1/NEST2/NEST3\r\n" >&3
							echo -en "a create NEST1/NEST2/NEST3\r\n" >&3
							echo -en "a logout\r\n" >&3
							echo "+++++++++++++++++++++++++"
							cat <&3
							echo "+++++++++++++++++++++++++"
							echo


							### Check for errors in the imap logs
							cat $INTERMAIL/log/imapserv.log| egrep -i "erro;|urgt;|fatl;" > imap_errors.txt
							echo "Errors logged in IMAP Logs:"
							echo "=========================="
							cat imap_errors.txt
							echo
							echo "=========================="
							### Check for mss used for imap operations
							mss_used_imap=$(cat $INTERMAIL/log/imapserv.log | cut -d ":" -f3 | grep -i mss | cut -d "=" -f2 | sort -u)
							echo
							echo "MSS used for imap operations was on host: "$mss_used_imap
														#Checking for cores..#
							echo
							echo "Please wait while we are checking for any cores..."
							cores=$(find $INTERMAIL/tmp -name "*core.*" | wc -l)
							cores=`echo $cores | tr -d " "`
							find $INTERMAIL/tmp -name "*core.*" > imap_cores_found.txt
							if [ "$cores" == "0" ]
							then
								echo
								echo "No cores were found."
								echo
								echo "-----X-X  IMAP OPERATIONS COMPLETED  X-X-----"
								Result="0"
							### If cores seen during imap operations then,
							else
								echo
								echo "Following cores were found:"
								echo "==========================="
								cat imap_cores_found.txt
								echo "==========================="
								echo "Hence, can't do processing Further."
								Result="1"
								summary "IMAP test cases" $Result
								cleanup
								echo
							fi
							#end_time_tc imap_operations_tc
							for ((i=1;i<52;i++))
							do
								summary "IMAP test cases $i" $Result
							done
							
}
 
function map_tc(){
start_time_tc
				echo "==========================="
				echo "PERFORMING MAP user command"
				echo "==========================="

				imdbcontrol map $user1 openwave.com mapuser
				## POP OPERATIONS START FROM HERE
				echo
				echo "==========================="
				echo " PERFORMING POP OPERATIONS "
				echo "==========================="
				echo
				echo "Connecting to $POPHost on Port $POPPort";
				echo "Please wait ... "
				echo
				exec 3<>/dev/tcp/$POPHost/$POPPort
				echo -en "user mapuser\r\n" >&3
				echo -en "pass $user1\r\n" >&3
				echo -en "quit\r\n" >&3
				#echo "+++++++++++++++++++++++++"
				cat <&3 > map.txt
				#echo "+++++++++++++++++++++++++"
				echo
						
				msgs_fldr=$(cat map.txt | grep -i "is welcome here" | wc -l)
								
				if [ "$msgs_fldr" == "1" ]
					then
					echo
					Result="0"
					echo "user name chnaged through map."
					echo
					else
					echo
					Result="1"
					echo "ERROR: user name not chnaged through map."
					echo
				fi
				echo
				imdbcontrol map $user1 openwave.com $user1
				
				exec 3<>/dev/tcp/$POPHost/$POPPort
				echo -en "user $user1\r\n" >&3
				echo -en "pass $user1\r\n" >&3
				echo -en "quit\r\n" >&3
				#echo "+++++++++++++++++++++++++"
				cat <&3 > map.txt
				#echo "+++++++++++++++++++++++++"
				echo
				msgs_fldr=$(cat map.txt | grep -i "is welcome here" | wc -l)
								
				if [ "$msgs_fldr" == "1" ]
					then
					echo
					Result="0"
					echo "user name reset is done through map."
					else
					Result="1"
					echo "ERROR: user name reset is not done through map."
					echo
				fi

summary "user name chnaged through map" $Result

}

function change_smtp_add() {
				start_time_tc
				echo "======================"
				echo "Change of SMTP address"
				echo "======================"
				start_time_tc
				msg_no=$(imboxstats $user1@openwave.com| grep "Total Messages Stored" | cut -d ":" -f2 | tr -d " ")		
				msg_no=$(($msg_no+2))
				#echo "$msg_no"
				imdbcontrol mas $user1 openwave.com testuser openwave.com
				#mail send to user2
				exec 3<>/dev/tcp/$MTAHost/$SMTPPort
				echo -en "MAIL FROM:$MAILFROM\r\n" >&3
				echo -en "RCPT TO:testuser\r\n" >&3
				echo -en "DATA\r\n" >&3
				echo -en "Subject: $SUBJECT\r\n\r\n" >&3
				echo -en "$DATA\r\n" >&3
				echo -en ".\r\n" >&3
				echo -en "MAIL FROM:$MAILFROM\r\n" >&3
				echo -en "RCPT TO:testuser\r\n" >&3
				echo -en "DATA\r\n" >&3
				echo -en "Subject: $SUBJECT\r\n\r\n" >&3
				echo -en "$DATA\r\n" >&3
				echo -en ".\r\n" >&3
				echo -en "QUIT\r\n" >&3
				cat <&3 > mail.txt
				echo

				msg_no1=$(imboxstats $user1@openwave.com| grep "Total Messages Stored" | cut -d ":" -f2 | tr -d " ")

				#echo "$msg_no1"

									if [ "$msg_no" == "$msg_no1" ]
									then
										
										echo "Change of SMTP address is working fine"
										Result="0"
									else
										
										echo "ERROR:Change of SMTP address is not working fine. Please check manually."
										
										Result="1"
									fi
				imdbcontrol mas testuser openwave.com $user1 openwave.com							
				summary "Change of SMTP address" $Result
				
}

function setaccount_status() {
								start_time_tc
								echo "======================"
								echo "Chnage account status "
								echo "======================"
								imdbcontrol SetAccountStatus $user1 openwave.com M
								
								msg_sh=$(imdbcontrol gaf $user1 openwave.com | grep "Status" | cut -d ":" -f2| grep "M" | wc -l)
										
								if [ "$msg_sh" == "1" ]
								then
									
									echo "Mode M account for a user is working fine"
									Result="0"

								else

									echo "ERROR:Mode M account for a user is not working fine"
									
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								#end_time_tc account_mode_M_tc
								summary "Mode M account for a user" $Result
								imdbcontrol SetAccountStatus $user1 openwave.com A
								
								msg_sh=$(imdbcontrol gaf $user1 openwave.com | grep "Status" | cut -d ":" -f2| grep "A" | wc -l)
										
								if [ "$msg_sh" == "1" ]
								then
									echo "Mode is reset to A"

								else
									
									echo "ERROR:Mode is not reset to A"

									
								fi
}

function password_chnage_ac() {
start_time_tc
								echo "============================"
								echo "Password Change of a Account"
								echo "============================"
								imdbcontrol setPassword $user1 openwave.com secret clear
								echo "==========================="
								echo " PERFORMING POP OPERATIONS "
								echo "==========================="
								echo
								echo "Connecting to $POPHost on Port $POPPort";
								echo "Please wait ... "
								echo
								
								exec 3<>/dev/tcp/$POPHost/$POPPort
								echo -en "user $user1\r\n" >&3
								echo -en "pass secret\r\n" >&3
								echo -en "list\r\n" >&3
								echo -en "retr 1\r\n" >&3
								echo -en "quit\r\n" >&3
								cat <& 3 &> shared.txt
								msg_sh=$(cat shared.txt | grep "is welcome here" | wc -l)
										
								if [ "$msg_sh" == "1" ]
								then
		
									echo "Password change succesfully"
									Result="0"

								else
									
									echo "ERROR:Password is not change succesfully"
									
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								#end_time_tc account_mode_M_tc
								summary "Password change succesfully" $Result

								imdbcontrol setPassword $user1 openwave.com $user1 clear
								echo "============================"
								echo "Password Change of a Account"
								echo "============================"
								imdbcontrol setPassword $user1 openwave.com secret clear
								echo "==========================="
								echo " PERFORMING POP OPERATIONS "
								echo "==========================="
								echo
								echo "Connecting to $POPHost on Port $POPPort";
								echo "Please wait ... "
								echo
								
								exec 3<>/dev/tcp/$POPHost/$POPPort
								echo -en "user $user1\r\n" >&3
								echo -en "pass $user1\r\n" >&3
								echo -en "list\r\n" >&3
								echo -en "retr 1\r\n" >&3
								echo -en "quit\r\n" >&3
								cat <& 3 &> shared.txt
								msg_sh=$(cat shared.txt | grep "is welcome here" | wc -l)
										
								if [ "$msg_sh" == "1" ]
								then

									echo "Password reset succesfully"

								else
					
									echo "ERROR:Password is not reset succesfully"
									
									echo "ERROR: Please check Manually."								
								fi
}

function create_alias () {
start_time_tc
									echo "======================="
									echo "Create Alias for a User"
									echo "======================="
									imdbcontrol CreateAlias $user1 1 openwave.com
									msg_no=$(imboxstats $user1@openwave.com| grep "Total Messages Stored" | cut -d ":" -f2 | tr -d " ")
									msg_no=$(($msg_no+2))
									#send mail to user name 1
									exec 3<>/dev/tcp/$MTAHost/$SMTPPort
									echo -en "MAIL FROM:$MAILFROM\r\n" >&3
									echo -en "RCPT TO:1\r\n" >&3
									echo -en "DATA\r\n" >&3
									echo -en "Subject: $SUBJECT\r\n\r\n" >&3
									echo -en "$DATA\r\n" >&3
									echo -en ".\r\n" >&3
									echo -en "MAIL FROM:$MAILFROM\r\n" >&3
									echo -en "RCPT TO:1\r\n" >&3
									echo -en "DATA\r\n" >&3
									echo -en "Subject: $SUBJECT\r\n\r\n" >&3
									echo -en "$DATA\r\n" >&3
									echo -en ".\r\n" >&3
									echo -en "QUIT\r\n" >&3
									cat <&3 > mail.txt
									echo
														
									msg_no1=$(imboxstats $user1@openwave.com| grep "Total Messages Stored" | cut -d ":" -f2 | tr -d " ")

									if [ "$msg_no" == "$msg_no1" ]
									then
										
										echo "Alias creation is working fine"
										Result="0"
									else
										
										echo "ERROR:Alias creation is not working fine. Please restart manually."
										
										Result="1"
									fi
													
									summary "Alias creation" $Result						

}

function delete_alias () {
start_time_tc
									echo "======================="
									echo "Delete Alias for a User"
									echo "======================="
									imdbcontrol DeleteAlias 1 openwave.com > alias.txt
									#msg_no=$(imboxstats $user1@openwave.com| grep "Total Messages Stored" | cut -d ":" -f2 | tr -d " ")
									
									#send mail to user name 1
									#mail_send 1 small 2
									#msg_no1=$(imboxstats $user1@openwave.com| grep "Total Messages Stored" | cut -d ":" -f2 | tr -d " ")
									msg_no=$(cat alias.txt | grep "ERROR" | wc -l)
									if [ "$msg_no" == "0" ]
									then
										
										echo "Alias deletion is working fine"
										Result="0"
									else
										
										echo "ERROR:Alias deletion is not working fine. Please restart manually."
										
										Result="1"
									fi
												
									summary "Alias deletion" $Result	
}

function multiple_alias() {
start_time_tc
				echo "========================="
				echo "Multiple Alias for a User"
				echo "========================="
				msg_no=$(imboxstats $user1@openwave.com| grep "Total Messages Stored" | cut -d ":" -f2 | tr -d " ")
				msg_no=$(($msg_no+4))
				imdbcontrol CreateAlias $user1 1 openwave.com >> trash,txt
				imdbcontrol CreateAlias $user1 2 openwave.com >> trash,txt
				imdbcontrol CreateAlias $user1 3 openwave.com >> trash,txt
				imdbcontrol CreateAlias $user1 4 openwave.com >> trash,txt
				imdbcontrol ListAliases $user1 openwave.com >> trash,txt
				#mailsend to alias
				mail_send 1 small 1
				mail_send 2 small 1
				mail_send 3 small 1
				mail_send 4 small 1

				msg_no1=$(imboxstats $user1@openwave.com| grep "Total Messages Stored" | cut -d ":" -f2 | tr -d " ")

									if [ "$msg_no" == "$msg_no1" ]
									then
										
										echo "Multiple Alias creation is working fine"
										Result="0"
									else
										
										echo "ERROR:Multiple Alias creation is not working fine. Please restart manually."
										
										Result="1"
									fi
													
									summary "Multiple Alias creation" $Result	

				imdbcontrol DeleteAlias 1 openwave.com >> trash,txt
				imdbcontrol DeleteAlias 2 openwave.com >> trash,txt
				imdbcontrol DeleteAlias 3 openwave.com >> trash,txt
				imdbcontrol DeleteAlias 4 openwave.com >> trash,txt
				imdbcontrol ListAliases $user1 openwave.com >> trash,txt
}

function create_account_fwd () {
start_time_tc

				echo "======================"
				echo "Create Account forward"
				echo "======================"
msg_no=$(imboxstats $user2@openwave.com| grep "Total Messages Stored" | cut -d ":" -f2 | tr -d " ")
				msg_no=$(($msg_no+2))
imdbcontrol CreateRemoteForward $user1 openwave.com $user2@openwave.com 
#mail send to user1
#mail_send $user1 small 2 


#msg_no1=$(imboxstats $user2@openwave.com| grep "Total Messages Stored" | cut -d ":" -f2 | tr -d " ")
msg_fwd=$(imdbcontrol ListAccountForwards $user1 openwave.com | grep "$user2"| wc -l)
									if [ "$msg_fwd" == "1" ]
									then
										
										echo "Create account forward is working fine"
										Result="0"
									else
										
										echo "ERROR:Create account forward is not working fine. Please restart manually."
										
										Result="1"
									fi
													
									summary "Create acoount forward" $Result	


}

function list_account_fwd() {
start_time_tc
				echo "======================"
				echo "List Account forward"
				echo "======================"
				#imdbcontrol ListAccountForwards $user1 openwave.com
				msg_fwd=$(imdbcontrol ListAccountForwards $user1 openwave.com | grep "$user2"| wc -l)
				if [ "$msg_fwd" == "1" ]
									then
										
										echo "List accoutnt forward is working fine"
										Result="0"
									else
										
										echo "ERROR:List accoutnt forward is not working fine. Please restart manually."
										
										Result="1"
				fi
summary "List account forward" $Result
}

function delete_account_fwd () {
start_time_tc
				echo "======================"
				echo "Delete Account forward"
				echo "======================"
				imdbcontrol DeleteRemoteForward $user1 openwave.com $user2@openwave.com
				msg_fwd=$(imdbcontrol ListAccountForwards $user1 openwave.com | grep "$user2"| wc -l)
				if [ "$msg_fwd" == "0" ]
									then
										
										echo "Delete accoutnt forward is working fine"
										Result="0"
									else
										
										echo "ERROR:Delete accoutnt forward is not working fine. Please check manually."
										
										Result="1"
				fi
summary "Delete account forward" $Result
}

function default_domain () {
start_time_tc
				echo "=============="
				echo "Default domain"
				echo "=============="
				msg=$(imdbcontrol GetDefaultDomain |grep "openwave.com" | wc -l)
				if [ "$msg" == "1" ]
									then
										
										echo "Default domain is openwave.com"
										Result="0"
									else
										
										echo "ERROR:Default domain is not openwave.com. Please check manually."
										
										Result="1"
				fi
				summary "Default domain" $Result
}

function create_cos () {
start_time_tc
				echo "=========="
				echo "Create Cos"
				echo "=========="
imdbcontrol CreateCos bogus
msg=$(imdbcontrol ListCosNames | grep "bogus" | wc -l)
if [ "$msg" == "1" ]
									then
										
										echo "Able to create new COS"
										Result="0"
									else
										
										echo "ERROR:Not able to create new COS. Please check manually."
										
										Result="1"
				fi

summary "Create new cos" $Result

}

function delete_cos() {

start_time_tc
				echo "=========="
				echo "Delete Cos"
				echo "=========="
imdbcontrol DeleteCos bogus
msg=$(imdbcontrol ListCosNames | grep "bogus" | wc -l)
if [ "$msg" == "0" ]
									then
										
										echo "Able to delete new COS"
										Result="0"
									else
										
										echo "ERROR:Not able to delete new COS. Please check manually."
										
										Result="1"
				fi

summary "Delete cos" $Result
}

function show_cos () {
start_time_tc
				echo "================"
				echo "Show default Cos"
				echo "================"
                msg=$(imdbcontrol ShowCos default | wc -l)
 
                if [ $msg -gt 110 ]
					then
						echo "Able to see default COS"
						Result="0"
					else
						echo "ERROR:Not able to see default COS. Please check manually."
						Result="1"
				fi
				summary "See default cos" $Result
}

function imap_invalid_tag () {
start_time_tc 
				echo "==================="
				echo "Invalid logout IMAP"
				echo "==================="
							## IMAP OPERATIONS START FROM HERE
							echo
							echo "=========================="
							echo "PERFORMING IMAP OPERATIONS"
							echo "=========================="
							echo
							echo "Connecting to $IMAPHost on Port $IMAPPort";
							echo "Please wait ... "
							echo

							exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

							echo -en "a login $user1@openwave.com $user1\r\n" >&3
							echo -en "a select INBOX\r\n" >&3
							echo -en "fdg#@$%%  logout\r\n" >&3
							echo -en "a logout\r\n" >&3
							echo "+++++++++++++++++++++++++"
							cat <&3 > mail.txt
							echo "+++++++++++++++++++++++++"
							echo
msg=$(cat mail.txt | grep "BAD Illegal character in tag" | wc -l)
if [ "$msg" == "1" ]
									then
										
										echo "Giving proper error message while wrong logout command"
										Result="0"
									else
										
										echo "ERROR:Not giving proper error message while wrong logout command. Please check manually."
										
										Result="1"
				fi
				summary "Error message while incorrect logout IMAP" $Result


}

function stored_command_before_login () {
start_time_tc
#Verifying that STORE command is not available in Non-Authenticated state – negative

				echo "========================================================"
				echo "Store commmand not available in Non-Authenticated state "
				echo "========================================================"
## IMAP OPERATIONS START FROM HERE
							echo
							echo "=========================="
							echo "PERFORMING IMAP OPERATIONS"
							echo "=========================="
							echo
							echo "Connecting to $IMAPHost on Port $IMAPPort";
							echo "Please wait ... "
							echo

							exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

							echo -en "store 1:* FLAGS (\Seen)\r\n" >&3
							echo -en "a logout\r\n" >&3
							echo "+++++++++++++++++++++++++"
							cat <&3 > mail.txt
							echo "+++++++++++++++++++++++++"
							

#BAD Unrecognized command, please login
msg=$(cat mail.txt | grep "BAD Unrecognized command, please login" | wc -l)
if [ "$msg" == "1" ]
									then
										
										echo "Giving proper error"
										Result="0"
									else
										
										echo "ERROR:Not giving proper error message. Please check manually."
										
										Result="1"
				fi
				summary "Store commmand not available in Non-Authenticated state" $Result

}

function imboxstats_utility() {
								start_time_tc imboxstats_utility_tc
								echo "##########################"
								echo "|   TESTING IMBOXSTATS   |"
								echo "##########################"
								echo
								imboxstats $MBXID_Sanity1 > boxstats_Sanity1.txt
								msgs=$(grep "Total Messages Stored"  boxstats_Sanity1.txt | cut -d " " -f6)
								bytes=$(grep "Total Bytes Stored"  boxstats_Sanity1.txt | cut -d " " -f9)
								lines=$(cat boxstats_Sanity1.txt | wc -l)
								msgs=`echo $msgs | tr -d " "`
								bytes=`echo $bytes | tr -d " "`
								lines=`echo $lines | tr -d " "`
								if [ $msgs -gt 0 ]
								then
									if [ "$bytes" != "0" ]
									then
										if [ "$lines" == "6" ]
										then
											echo "================================================================"
											cat boxstats_Sanity1.txt
											echo "================================================================"
											echo
											Result="0"
											echo "imboxstats utility is working fine."
											echo
										else
											echo
											Result="1"
											echo "ERROR: imboxstats output is not correct."
											echo
										fi
									else
										echo
										Result="1"
										echo "ERROR: 'Total Bytes Stored' value is not correct."
										echo
									fi
								else
									echo
									Result="1"
									echo "ERROR: 'Total Messages Stored' value is not correct."
									echo
								fi
								echo "XX-----Testing of IMBOXSTATS utility is completed-----XX"
								echo
								summary "Imboxstats" $Result "ITS:NOT DEFINED"
								#end_time_tc imboxstats_utility_tc
}

function imfolderlist_utility() {
								start_time_tc imfolderlist_utility_tc
								echo "##########################"
								echo "|  TESTING IMFOLDERLIST  |"
								echo "##########################"
								echo
								imfolderlist $MBXID_Sanity1 > folderlist_Sanity1.txt
								msgs_fldr=$(cat folderlist_Sanity1.txt | grep -i "Message-Id" | wc -l)
								msgs_fldr=`echo $msgs_fldr | tr -d " "`
								
								if [ "$msgs_fldr" == "4" ]
								then
									echo "================================================================"
									cat folderlist_Sanity1.txt
									
									Result="0"
									echo "imfolderlist utility is working fine."
									
								else
									
									Result="1"
									echo "ERROR: imfolderlist utility is not showing proper results."
									
								fi
								#echo "XX-----Testing of IMFOLDERLIST utility is completed-----XX"
								
								summary "imfolderlist_utility" $Result "MX-230"
								#end_time_tc imfolderlist_utility_tc
}

function immssdescms_utility() {
								start_time_tc immssdescms_utility_tc
								echo "##########################"
								echo "|  TESTING  IMMSSDESCMS  |"
								echo "##########################"
								echo
								
								if [ "$setuptype" == "stateless" ]
								then
									shrd_yes_no="2" 
									shrd_no_no="2"
								else
        							shrd_yes_no="3" 
									shrd_no_no="1"
		    					fi
									
								year=$(date +%Y)
								month=$(date +%b)
								immssdescms -h $Cluster -i $MBXID_Sanity1 > descms_Sanity1.txt
								msgid=$(cat descms_Sanity1.txt | egrep -i "Message ID" | wc -l)
								msgid=`echo $msgid | tr -d " "`
								shrd_yes=$(cat descms_Sanity1.txt | egrep -i "Shared=YES" | wc -l)
								shrd_yes=`echo $shrd_yes | tr -d " "`
								shrd_no=$(cat descms_Sanity1.txt | egrep -i "Shared=NO" | wc -l)
								shrd_no=`echo $shrd_no | tr -d " "`
								msartime_yr=$(cat descms_Sanity1.txt | egrep -i "MSSArrivalTime" | grep -i "$year" | wc -l)
								msartime_yr=`echo $msartime_yr | tr -d " "`
								msartime_mon=$(cat descms_Sanity1.txt | egrep -i "MSSArrivalTime" | grep -i "$month" | wc -l)
								msartime_mon=`echo $msartime_mon | tr -d " "`
								mbxartime_yr=$(cat descms_Sanity1.txt | egrep -i "MailboxArrivalTime" | grep -i "$year" | wc -l)
								mbxartime_yr=`echo $mbxartime_yr | tr -d " "`
								mbxartime_mon=$(cat descms_Sanity1.txt | egrep -i "MailboxArrivalTime" | grep -i "$month" | wc -l)
								mbxartime_mon=`echo $mbxartime_mon | tr -d " "`
								#echo " shared yes == "$shrd_yes
								#echo " shared no == "$shrd_no
								if [ "$msgid" == "4" ]
								then
																
									if [[ "$shrd_yes" == "$shrd_yes_no" || "$shrd_no" == "$shrd_no_no" ]]
									then
										if [ "$msartime_yr" == "4" ]
											then
												if [ "$msartime_mon" == "4" ]
												then
													if [ "$mbxartime_yr" == "4" ]
													then
														if [ "$mbxartime_mon" == "4" ]
														then
															#echo "================================================================"
															#cat descms_Sanity1.txt
															#echo "================================================================"
															echo
															Result="0"
															echo "imssdescms utility is working fine."
															echo
														else
															echo
															Result="1"
															echo "ERROR: mailbox arrival time is not correct"
															echo
														fi
													else
														echo
														Result="1"
														echo "ERROR: mailbox arrival time is not correct"
														echo
													fi
												else
													echo
													Result="1"
													echo "ERROR: message arrival time is not correct"
													echo
												fi
											else
												echo
												Result="1"
												echo "ERROR: message arrival time is not correct"
												echo
											fi
										
									else
										echo
										Result="1"
										echo "ERROR: Number of unshared messages is not correct"
										echo
									fi
								else
									echo
									Result="1"
									echo "ERROR: Number of Messages are not correct"
									echo
								fi
								#echo "XX-----Testing of IMMSSDESCMS utility is completed-----XX"
								echo
								summary "immssdescms_utility" $Result "MX-230"
								#end_time_tc immssdescms_utility_tc
}

function immsgdump_utility() {
								start_time_tc immsgdump_utility_tc
								echo "##########################"
								echo "|   TESTING  IMMSGDUMP   |"
								echo "##########################"
								echo
								immsgdump $Cluster $MBXID_Sanity1 1 > immsgdump_Sanity1.txt
								ln_msgfind=$(cat immsgdump_Sanity1.txt | wc -l)
								ln_msgfind=`echo $ln_msgfind | tr -d " "`
								if [ $ln_msgfind > 1 ]
								then
									#echo "================================================================"
									#cat immsgdump_Sanity1.txt
									#echo "================================================================"
									echo
									Result="0"
									echo "immsgdump utility is working fine"
									echo
								else
									echo
									Result="1"
									echo "ERROR: immsgdump utility output is not correct. Please check manually."
									echo
								fi
								#echo "XX-----Testing of IMMSGDUMP utility is completed-----XX"
								echo
								summary "immsgdump utility" $Result
								#end_time_tc immsgdump_utility_tc
}

function immsgfind_utility() {
								start_time_tc immsgfind_utility_tc
								echo "##########################"
								echo "|   TESTING  IMMSGFIND   |"
								echo "##########################"
								echo
								msg_id=$(cat folderlist_Sanity1.txt | grep -i Message-Id | cut -d " " -f2 | grep -i $year | head -1)
								msg_id=`echo $msg_id | tr -d " "`
								immsgfind $Cluster $MBXID_Sanity1 "$msg_id" > immsgfind_Sanity1.txt
								chk_msg_id=$(cat immsgfind_Sanity1.txt | egrep -i "MESSAGE-ID" | wc -l)
								chk_msg_id=`echo $chk_msg_id | tr -d " "`
								if [ "$chk_msg_id" == "1" ]
								then
									echo "================================================================"
									cat immsgfind_Sanity1.txt
									echo "================================================================"
									echo
									Result="0"
									echo "immsgfind utility is working fine"
									echo
								else
									echo
									Result="1"
									echo "ERROR: immsgfind utility output is not correct. Please check Manually."
									echo
								fi
								echo "XX-----Testing of IMMSGFIND utility is completed-----XX"
								echo
								summary "immsgfind utility" $Result "MX-230"
								#end_time_tc immsgfind_utility_tc

}

function imboxmaint_utility() {
								start_time_tc imboxmaint_utility_tc
								echo "##########################"
								echo "|   TESTING  IMBOXMAINT  |"
								echo "##########################"
								echo
	
							#imconfcontrol -install -key "/*/common/mailFolderQuotaEnabled=true" > output.txt			

							#imdbcontrol sac $user1 openwave.com mailfolderquota "/ all,AgeSeconds,1"
							quotaset_check="0"

								imcheckfq -u $user11@openwave.com > chckfq.txt
								quotaset_check=$(cat chckfq.txt | egrep -i "AgeSeconds" | wc -l)
								quotaset_check=`echo $quotaset_check | tr -d " "`
								if [ "$quotaset_check" == "1" ]
								then
									imboxstats $MBXID_Sanity11 > boxmaint_stats_Sanity1.txt
									msgs_fq1=$(grep "Total Messages Stored" boxmaint_stats_Sanity1.txt | cut -d " " -f6)
									msgs_fq1=`echo $msgs_fq1 | tr -d " "`
									echo "Currently there are "$msgs_fq1" Messages in the mailbox"
									echo "Now Running Maintenance on this mailbox, please wait..."
									imboxmaint $MBXID_Sanity11 > boxmaint_Sanity1.txt
									imboxstats $MBXID_Sanity11 > boxmaint_stats_Sanity1_2.txt
									msgs_fq2=$(grep "Total Messages Stored" boxmaint_stats_Sanity1_2.txt | cut -d " " -f6)
									msgs_fq2=`echo $msgs_fq2 | tr -d " "`
									echo "After running maintenance, there are "$msgs_fq2" Messages in the mailbox"
									if [ "$msgs_fq2" == "0" ]
									then
										echo
										echo "imboxmaint utility is working fine."
										Result="0"
										echo 
									else
										echo
										echo "ERROR: imboxmaint utility is not working fine. Please check Manually."
										echo
										Result="1"
									fi
								else
									echo
									echo "ERROR: FolderQuota is not set properly."
									echo
								fi


								#end_time_tc imboxmaint_utility_tc
								summary "imboxmaint utility" $Result
}

function imboxcopy_utility() {
								start_time_tc imboxcopy_utility_tc
								echo "##########################"
								echo "|   TESTING   IMBOXCOPY  |"
								echo "##########################"
								
								imboxstats $MBXID_Sanity2 > boxstats_Sanity2.txt
								msg_bxcp1=$(grep "Total Messages Stored" boxstats_Sanity2.txt | cut -d " " -f6)
								msg_bxcp1=`echo $msg_bxcp1 | tr -d " "`
								echo "Currently "$msg_bxcp1" messages are stored in the mailbox"
								echo
								echo "Running imboxcopy utility, please wait.."
								imboxcopy ms://$Cluster/DB/$MBXID_Sanity2 ms://$Cluster/DB/$MBXID_Sanity5 > boxcopy.txt
								imboxstats $MBXID_Sanity5 > boxstats_Sanity5.txt
								msg_bxcp2=$(grep "Total Messages Stored" boxstats_Sanity5.txt | cut -d " " -f6)
								msg_bxcp2=`echo $msg_bxcp2 | tr -d " "`
								if [ $msg_bxcp2 == $msg_bxcp1 ]
								then
									
									echo "Result of imboxcopy command:"
									#cat boxcopy.txt
									echo "Currently "$msg_bxcp2" messages are stored in the mailbox"
									
									echo "imboxcopy utility is working fine"
									Result="0"
								else
									
									echo "ERROR: Currently "$msg_bxcp2" messages are stored in the mailbox"
									
									echo "ERROR: imboxcopy utility is not working fine. Please check Manually."
									Result="1"
								fi
								echo "XX-----Testing of IMBOXCOPY utility is completed-----XX"
								#end_time_tc imboxcopy_utility_tc
								summary "imboxcopy utility" $Result
}

function immsgdelete_utility() {
								start_time_tc immsgdelete_utility_tc
								echo "##########################"
								echo "|   TESTING IMMSGDELETE  |"
								echo "##########################"
								
								imboxstats $MBXID_Sanity2 > boxstats_Sanity2.txt
								msg_msgdel1=$(grep "Total Messages Stored"  boxstats_Sanity2.txt | cut -d " " -f6)
								msg_msgdel1=`echo $msg_msgdel1 | tr -d " "`
								echo "Currently "$msg_msgdel1" messages are stored in the mailbox"
								echo
								echo "Running immsgdelete utility, please wait.."
								immsgdelete $user2@openwave.com -all
								imboxstats $MBXID_Sanity2 > boxstats_Sanity2.txt
								msg_msgdel2=$(grep "Total Messages Stored"  boxstats_Sanity2.txt | cut -d " " -f6)
								msg_msgdel2=`echo $msg_msgdel2 | tr -d " "`
								if [ "$msg_msgdel2" == "0" ]
								then
									
									echo "Currently "$msg_msgdel2" messages are stored in the mailbox"
									
									echo "immsgdelete utility is working fine"
									Result="0"
								else
									
									echo "ERROR: Currently "$msg_msgdel2" messages are stored in the mailbox"
									echo
									echo "ERROR: immsgdelete utility is not working fine. Please check Manually."
									Result="1"
								fi
								#echo "XX-----Testing of IMMSGDELETE utility is completed-----XX"
								#end_time_tc immsgdelete_utility_tc
								summary "immsgdelete utility" $Result "MX-230"
}

function imimapboxcopy_utility() {
								start_time_tc imimapboxcopy_utility_tc
								echo "############################"
								echo "|   Verify imimapboxcopy . |"		
								echo "############################"
								#sending small mail to $user7 
								mail_send $user7 small 2
								
								#echo > imimap.txt
								echo $user7@openwave.com &> user.txt
								
								imimapboxcopy -append -batch user.txt -host $Cluster &> imimap.txt
								
								echo "imimapboxcopy -append -batch user.txt -host $Cluster"
								
								msg_msg=$(grep "com to host $Cluster Successful" imimap.txt | cut -d "." -f2 | wc -l )
								
								 if [ $msg_msg == "1" ]
								then
									
							     	
									echo "We are able to append mailbox through imimapboxcopy"
									
									echo "imimapboxcopy utility is working fine"
									Result="0"
								else
									
									echo "ERROR: Currently are not able to append mailbox through imimapboxcopy"
									echo
									echo "ERROR: imimapboxcopy utility is not working fine. Please check Manually."
									Result="1"
								fi
								echo "XX-----Testing of IMIMAPBOXCOPY utility is completed-----XX"
								#end_time_tc imimapboxcopy_utility_tc
								summary "imimapboxcopy utility" $Result "MX-230"

}

function verify_newcos() {
								start_time_tc verify_newcos_tc
								echo "#####################################"
								echo "|   Verify new cos class creation . |"		
								echo "#####################################"
							
								imdbcontrol cc newcosclass
								imdbcontrol lcn &> cos.txt
															
								msg_msg=$( grep "newcosclass" cos.txt | wc -l )
								 if [ $msg_msg == "1" ]
								then
									
							     	
									echo "We are able to create new cos class of service"
									
									Result="0"
									
								else
									
									echo "ERROR: We are not able to create new cos class of service"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								#end_time_tc verify_newcos_tc
								summary "Create new cos class" $Result
}

function imcheckfq_utility() {
								start_time_tc imcheckfq_utility_tc
								echo "##########################"
								echo "|   TESTING IMCHECKFQ  |"
								echo "##########################"
								echo
								#imconfcontrol -install -key "/*/common/mailFolderQuotaEnabled=true"
								
								#lib/imservctrl killStart mss
								#restart mss function
								#restart_all_mss
										
								imcheckfq -u $user2@openwave.com > IMCHECKFQ_Sanity2.txt
								
								msg_msgdel9=$(grep "/ all,AgeDays,2"  IMCHECKFQ_Sanity2.txt)
								msg_msgdel9=`echo $msg_msgdel9 | tr -d " "`
								
								echo "Currently "$msg_msgdel9" folderquota is set"
								
								
								
								if [ "$msg_msgdel9" == "" ]
								then
									
									echo "ERROR: Currently "$msg_msgdel9" folder quota is set for the mailbox"
									echo
									echo "ERROR: Imcheckfq utility is not working fine. Please check Manually."
									Result="1"
								else
									
									echo "Currently "$msg_msgdel9" folder quota is not set for the mailbox"
									echo
									echo "Imcheckfq utility is working fine"
									Result="0"
								fi
								#imconfcontrol -install -key "/*/common/mailFolderQuotaEnabled=false"
								echo "XX-----Testing of IMCHECKFQ  utility is completed-----XX"
								#end_time_tc imcheckfq_utility_tc
								summary "Imcheckfq utility" $Result
}

function iminboxlist_utility() {
								start_time_tc iminboxlist_utility_tc
								echo "##########################"
								echo "|   TESTING iminboxlist  |"
								echo "##########################"
								echo
								#sending small mail to user1
								
								exec 3<>/dev/tcp/$MTAHost/$SMTPPort
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user1\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user1\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "QUIT\r\n" >&3
								cat <&3 > mail.txt
								echo

																
								sleep 5
								 
								iminboxlist $Cluster $user1@openwave.com  -all >imsanity.txt
								
								
								msg_msg=$(grep "Message-Id"  imsanity.txt | cut -d "<" -f2 | wc -l)
								msg_msg=`echo $msg_msg | tr -d " "`
								
								echo "Currently "$msg_msg" messages are stored in the mailbox"
								
								
								if [ "$msg_msg" == "" ]
								then
									
									echo "ERROR: We are not able to find messages in Inbox"
									echo
									echo "ERROR: Iminboxlist utility is not working fine. Please check Manually."
									Result="1"
								else
									
									echo "Currently "$msg_msg" messages are thier in inbox"
									echo
									echo "Iminboxlist utility is working fine"
									Result="0"
								fi
								
								
								#echo "XX-----Testing of iminboxlist utility is completed-----XX"
								#end_time_tc iminboxlist_utility_tc
								summary "Iminboxlist utility" $Result
}

function mss_imqueuserver_stop() {
								start_time_tc mss_imqueuserver_stop_tc
								echo "################################"
								echo "| Verify core when mss was stop|"
								echo "################################"
								
								if [ "$setuptype" == "stateful" ]

		                         then

								 FirstMSS=$MSSHost

                                 fi								
								
								~/lib/imservctrl stop mss &>> trash
								#~/lib/imservctrl stop imqueueserv	 &>> trash
								
								imservping -h $FirstMSS m mss > ping_resp.txt
								#imservping -h $FirstMSS imqueueserv >> ping_resp.txt
								resp_count=$(cat ping_resp.txt | grep -i "responded" | wc -l)
								resp_count=`echo $resp_count | tr -d " "`
								
								if [ "$resp_count" == "0" ]
									then
									
									echo "MSS and imqueueserv on "$FirstMSS" is successfully stopped"
									else
									
									echo "ERROR: MSS on "$FirstMSS" could not be stopped. Please stop manually."
									Result="1"
									echo
								fi
								#mail send to user1
								#mail_send $user1 small 2
									exec 3<>/dev/tcp/$MTAHost/$SMTPPort
									echo -en "MAIL FROM:$MAILFROM\r\n" >&3
									echo -en "RCPT TO:$user1\r\n" >&3
									echo -en "DATA\r\n" >&3
									echo -en "Subject: $SUBJECT\r\n\r\n" >&3
									echo -en "$DATA\r\n" >&3
									echo -en ".\r\n" >&3
									echo -en "MAIL FROM:$MAILFROM\r\n" >&3
									echo -en "RCPT TO:$user1\r\n" >&3
									echo -en "DATA\r\n" >&3
									echo -en "Subject: $SUBJECT\r\n\r\n" >&3
									echo -en "$DATA\r\n" >&3
									echo -en ".\r\n" >&3
									echo -en "QUIT\r\n" >&3
									cat <&3 > mail.txt
									echo

							    					 
								echo "Please wait while we are checking for any cores..."
								cores=$(find $INTERMAIL/tmp -name "*core.*" | wc -l)
								cores=`echo $cores | tr -d " "`
								find $INTERMAIL/tmp -name "*core.*" > imap_cores_found.txt
								if [ "$cores" == "0" ]
								then
									
									echo "No cores were found."
									Result="0"
									echo
								else
								Result="1"
								fi 	
								lib/imservctrl start mss &>> trash
								lib/imservctrl start imqueueserv &>> trash	
								imservping -h $FirstMSS mss > ping_resp.txt
								imservping -h $FirstMSS imqueueserv >> ping_resp.txt
								resp_count=$(cat ping_resp.txt | grep -i "responded" | wc -l)
								resp_count=`echo $resp_count | tr -d " "`
								
								if [ "$resp_count" == "2" ]
									then
									
									echo "MSS and imqueueserv on "$FirstMSS" is successfully started"
									else
									echo
									echo "ERROR: MSS on "$FirstMSS" could not be started. Please stop manually."
									Result="1"
									
								fi
							#end_time_tc mss_imqueuserver_stop_tc
							summary "Verify core when mss and imqueueserver was stop" $Result
}

function expunge_cache_inconsistence() {
								start_time_tc expunge_cache_inconsistence_tc
								echo "##########################################################"
								echo "| Verify Expunge of message makes imap cache inconsistant|"
								echo "##########################################################"
								echo
								#mail send to user8
								mail_send $user8 small 2
							    								
								echo > imap.txt
								exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

								echo -en "a login $user8@openwave.com $user8\r\n" >&3
								echo -en "a select INBOX\r\n" >&3
								echo -en "a store 1 +flags (\Deleted)\r\n" >&3
								echo -en "a expunge\r\n" >&3
								echo -en "a logout\r\n" >&3
								#echo "+++++++++++++++++++++++++"
								cat <&3 >> imap.txt
								#cat imap.txt
								#echo "+++++++++++++++++++++++++"
								
								msg_count1=$(cat log/imapserv.trace | grep "expungeMsgCache" | wc -l)
								 
							    msg_count=$(cat log/imapserv.trace | grep "unknown message" | wc -l)
								#[[ "$check_clustername_count" == "1" && "$check_setup_type" == "true" ]]
								
								if [[ "$msg_count" == "1" && "$msg_count1" != "0" ]]
								then
								
								
								echo " Expunge of message makes imap cache inconsistant. Please check manually."
								Result="1"
								else
								
								echo " Expunge of message does not make imap cache inconsistant."
								Result="0"
								
								fi 
								#end_time_tc expunge_cache_inconsistence_tc
								summary "Expunge of message makes imap cache inconsistant" $Result
}

function immssinvlist_utility() {
								start_time_tc immssinvlist_utility_tc
								echo "##########################"
								echo "|   TESTING immssinvlist  |"
								echo "##########################"
								
								immssinvlist -u $user1 -d openwave.com > immssinvlist.txt
					            
								
								msg_msg=$(grep "$MBXID_Sanity1"  immssinvlist.txt | cut -d ":" -f2)

								msg_msg=`echo $msg_msg | tr -d " "`
								
								echo "Currently we found messages in mail box id  "$msg_msg" "
								
								
								if [ "$msg_msg" == "" ]
								then
									
									echo "ERROR: Currently we are not able to find mailbox id $MBXID_Sanity1"
									echo
									echo "ERROR: immssinvlist utility is not working fine. Please check Manually."
									Result="1"
								else
									
									echo "We can open "$msg_msg" mailbox"
									echo
									echo "immssinvlist utility is working fine"
									Result="0"
								fi
								#end_time_tc immssinvlist_utility_tc
								summary "immssinvlist utility" $Result
}

function account_mode_M() {
								start_time_tc account_mode_M_tc
								echo "############################"
								echo "| Check for account mode M |"
								echo "############################"
								
								
								#mail send to user9
								mail_send $user9 small 1
								#sleep 5
							 								
								echo "==========================="
								echo " PERFORMING POP OPERATIONS "
								echo "==========================="
								echo
								echo "Connecting to $POPHost on Port $POPPort";
								echo "Please wait ... "
								echo
								
								exec 3<>/dev/tcp/$POPHost/$POPPort
								echo -en "user $user9\r\n" >&3
								echo -en "pass $user9\r\n" >&3
								echo -en "list\r\n" >&3
								echo -en "retr 1\r\n" >&3
								echo -en "quit\r\n" >&3
								cat <& 3 &> shared.txt
						
								msg_sh=$(cat shared.txt | grep "account is undergoing maintenance" | wc -l)
										
								if [ "$msg_sh" == "1" ]
								then
							
									echo "Mode M account for a user is workign fine"
									Result="0"

									
								else
									
									echo "ERROR:Mode M account for a user is not working fine"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								#end_time_tc account_mode_M_tc
								summary "Mode M account for a user is workign fine" $Result
}

function immssdescma_utility_sharedmsg () {
								start_time_tc immssdescma_utility_sharedmsg_tc
								echo "####################################################"
								echo "| Check for immssdescma utility and shared message |"
								echo "####################################################"

							    ## Single recipient delivery
								echo "-------Single Recipient Delivery Starts--------"
								echo
								echo "Connecting to $MTAHost on Port $SMTPPort";
								echo "Please wait ... "
								echo
								
								exec 3<>/dev/tcp/$MTAHost/$SMTPPort
								
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user3\r\n" >&3
								echo -en "RCPT TO:$user4\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "QUIT\r\n" >&3								
								#cat <&3 
								sleep 5
							 				 				
							    immssdescms -h $Cluster -i $MBXID_Sanity3 >  shared.txt
								immssdescms -h $Cluster -i $MBXID_Sanity4 >  shared1.txt

                                msh_immss=$(cat shared.txt | grep "Mailbox Name" | wc -l)
								
								#if [ "$msh_immss" == "1" ]
								#then

								#	echo "immssdescms utiltiy is working fine"
								#	Result="0"

								#else
									
								#	echo "ERROR:immssdescms utiltiy is not working fine"
								#	echo
								#	echo "ERROR: Please check Manually."
								#	Result="1"
								#fi 		
								
								msg_sh=$(cat shared.txt | grep "Shared=YES" | wc -l)
								
										
								if [ "$msg_sh" == "1" ]
								then

									echo "Shared message is working fine"
									Result="0"

								else
									echo "ERROR:Shared message is not working fine"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi 				
								summary "Shared messgae found" $Result "MX-230"
								start_time_tc
								echo " PERFORMING POP OPERATIONS "
								echo "==========================="
								echo
								echo "Connecting to $POPHost on Port $POPPort";
								echo "Please wait ... "
								echo
								
								exec 3<>/dev/tcp/$POPHost/$POPPort
								echo -en "user $user3\r\n" >&3
								echo -en "pass $user3\r\n" >&3
								echo -en "list\r\n" >&3
								echo -en "retr 1\r\n" >&3
								echo -en "dele 1\r\n" >&3
								echo -en "quit\r\n" >&3
								cat <& 3 &> shared.txt
						
								msg_sh=$(cat shared.txt | grep "Received" | wc -l)
								
										
								if [ "$msg_sh" == "1" ]
								then

									echo "Shared message is retrived fine"
									Result="0"
								else
									
									echo "ERROR:Shared message is not retrived fine"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi 			 	
								summary "Shared message retriving" $Result "MX-230"
								start_time_tc
								imboxstats $MBXID_Sanity3 > shared.txt
								
								msg_sh=$(cat shared.txt | grep "Total Messages Stored" | cut -d ":" -f2 | tr -d " ")

								if [ "$msg_sh" == "0" ]
								then

									echo "Shared message is deleted fine"
									Result="0"
								else
									
									echo "ERROR:Shared message is not deleted fine"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								#end_time_tc immssdescma_utility_sharedmsg_tc
								summary "Shared message deletion" $Result "MX-230"
}

function immsgtrace_utility() {
								start_time_tc immsgtrace_utility_tc
								echo "#####################"
								echo "| Verify immsg trace|"
								echo "#####################"						
								immsgtrace -to $user1 -size 2 > msgtrace.txt				
								msg_count1=$(cat msgtrace.txt | grep "sending message" | wc -l)
													   
								if [ "$msg_count1" == "1" ]
								then
								echo " immsgtrace is working fine."
								Result="0"
								else
								
								echo " immsgtrace is not working fine. Please check manually"
								Result="1"
								
								fi
								#end_time_tc immsgtrace_utility_tc
								summary "immsgtrace" $Result
}

function invalid_keyvalue_msslog_imap() {
								start_time_tc invalid_keyvalue_msslog_imap_tc
								echo "################################################################"
								echo "| Check for [keepalive=1 Invalid key value] in mss log for IMAP|"
								echo "################################################################"

								#imconfcontrol -install -key "/*/mss/autoReplyExpireDays=3" &>> trash
								#imconfcontrol -install -key  "*/common/traceOutputLevel=keepalive=1" &>> trash
								#imconfcontrol -install -key  "/*/mss/bogus1=12333223" &>> trash
								#imconfcontrol -install -key  "/*/mss/bogus1=wwwqweeqe" &>> trash
								#imconfcontrol -install -key  "/*/mss/bogus3=312sdwqwewe" &>> trash
								#imconfcontrol -install -key  "/*/mss/bogus4=3423eweeqwewa" &>> trash
								
								#mail send to user1
							    #mail_send $user1 small 2
								#exec 3<>/dev/tcp/$MTAHost/$SMTPPort
								#echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								#echo -en "RCPT TO:$user1\r\n" >&3
								#echo -en "DATA\r\n" >&3
								#echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								#echo -en "$DATA\r\n" >&3
								#echo -en ".\r\n" >&3
								#echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								#echo -en "RCPT TO:$user1\r\n" >&3
								#echo -en "DATA\r\n" >&3
								#echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								#echo -en "$DATA\r\n" >&3
								#echo -en ".\r\n" >&3
								#echo -en "QUIT\r\n" >&3
								#cat <&3 > mail.txt
								#echo
								
								exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

								echo -en "a login $user1@openwave.com $user1\r\n" >&3
								echo -en "a logout\r\n" >&3
								#echo "+++++++++++++++++++++++++"
								cat <&3 >> imap.txt
								#cat imap.txt
								#echo "+++++++++++++++++++++++++"
								
																					
								imconfcontrol -install -key "/*/mss/bogus1=12333240" &>> trash
								imconfcontrol -install -key "/*/mss/autoReplyExpireDays=20" &>> trash
								
								exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

								echo -en "a login $user1@openwave.com $user1\r\n" >&3
								echo -en "a logout\r\n" >&3
								echo "+++++++++++++++++++++++++"
								cat <&3 >> imap.txt
								#cat imap.txt
								echo "+++++++++++++++++++++++++"
							 
							    msg_count=$(cat log/mss.log | grep "keepalive=1 Invalid key value" | wc -l)
															
								if [ "$msg_count" == "0" ]
								then
								
								
								echo "We are not able to see [keepalive=1 Invalid key value] in mss log for IMAP"
								Result="0"
								else
								
								echo "We are able to see [keepalive=1 Invalid key value] in mss log for IMAP. Please check manually."
								Result="1"
								
								fi 
								#end_time_tc invalid_keyvalue_msslog_imap_tc
								summary "To verify [keepalive=1 Invalid key value] in mss log for IMAP" $Result
								


}

function invalid_keyvalue_msslog_pop() {
								start_time_tc invalid_keyvalue_msslog_pop_tc
								echo "################################################################"
								echo "| Check for [keepalive=1 Invalid key value] in mss log for POP|"
								echo "################################################################"

								#imconfcontrol -install -key "/*/mss/autoReplyExpireDays=3" &>> trash
								#imconfcontrol -install -key  "*/common/traceOutputLevel=keepalive=1" &>> trash
								#imconfcontrol -install -key  "/*/mss/bogus1=12333223" &>> trash
								#imconfcontrol -install -key  "/*/mss/bogus1=wwwqweeqe" &>> trash
								#imconfcontrol -install -key  "/*/mss/bogus3=312sdwqwewe" &>> trash
								#imconfcontrol -install -key  "/*/mss/bogus4=3423eweeqwewa" &>> trash
								
							    #mail send to user1
								#mail_send $user1 small 2
								
								exec 3<>/dev/tcp/$POPHost/$POPPort
								echo -en "user $user1\r\n" >&3
								echo -en "pass $user1\r\n" >&3
								echo -en "quit\r\n" >&3
								echo "+++++++++++++++++++++++++"
								#cat <&3 
								echo "+++++++++++++++++++++++++"
								echo
								imconfcontrol -install -key "/*/mss/bogus1=12333230" &>> trash
								imconfcontrol -install -key "/*/mss/autoReplyExpireDays=10" &>> trash
									
								exec 3<>/dev/tcp/$POPHost/$POPPort
								echo -en "user $user1\r\n" >&3
								echo -en "pass $user1\r\n" >&3
								echo -en "quit\r\n" >&3
								echo "+++++++++++++++++++++++++"
								#cat <&3 
								echo "+++++++++++++++++++++++++"
								echo
									
							    msg_count=$(cat log/mss.log | grep "keepalive=1 Invalid key value" | wc -l)
								
							
								if [ "$msg_count" == "0" ]
								then
								
								
								echo "We are not able to see [keepalive=1 Invalid key value] in mss log for POP"
								Result="0"
								else
								
								echo "We are able to see [keepalive=1 Invalid key value] in mss log for POP. Please check manually."
								Result="1"
								
								fi
								#end_time_tc invalid_keyvalue_msslog_pop_tc
								summary "To verify [keepalive=1 Invalid key value] in mss log for POP" $Result

}

function mail_freezing_owner_mss() {
								start_time_tc mail_freezing_owner_mss_tc
								echo "###################################################################"
								echo "|   Check for mail delivery by freezing the owner MSS process     |"
								echo "###################################################################"
								
								
								
							    #mail send to user 10
								mail_send $user10 small 1
								#sleep 5
							    
								pid=$(ps -ef |grep `whoami`| grep mss | cut -c10-15 | head -n 1 |cut -d " " -f1)
								if [ "$pid" == "" ]
								then
								pid=$(ps -ef |grep `whoami`| grep mss | cut -c10-15 | head -n 1 |cut -d " " -f2)
								else
								pid=$(ps -ef |grep `whoami`| grep mss | cut -c10-15 | head -n 1 |cut -d " " -f1)
								fi 
								echo "Please wait while we freeze owner MSS... "
								kill -STOP $pid

								
								#lib/imservctrl kill mss
								
								#mail send to user 10
								mail_send $user10 small 1
								
								kill -CONT $pid
									
								#sleep 30
								sleep 5
								
								echo "==========================="
								echo " PERFORMING POP OPERATIONS "
								echo "==========================="
								echo
								echo "Connecting to $POPHost on Port $POPPort";
								echo "Please wait ... "
								echo
								
								exec 3<>/dev/tcp/$POPHost/$POPPort
								echo -en "user $user10\r\n" >&3
								echo -en "pass $user10\r\n" >&3
								echo -en "list\r\n" >&3
								echo -en "retr 1\r\n" >&3
								echo -en "quit\r\n" >&3
								#cat <& 3
						
								imboxstats $MBXID_Sanity10 > imboxstats.txt
								msg_mss=$(cat imboxstats.txt | grep "Total Messages Stored" | cut -d ":" -f2 | tr -d " ")
										
								if [ "$msg_mss" == "2" ]
								then

									echo "Verification for mail delivery by freezing the owner MSS process is working fine"
									echo
									Result="0"
									
									
								else
									
									echo "ERROR: Verification for mail delivery by freezing the owner MSS process is not working fine"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								#end_time_tc mail_freezing_owner_mss_tc
								summary "Verification for mail delivery by freezing the owner MSS process" $Result
}

function deletion_cassendra() {
								start_time_tc deletion_cassendra_tc
								echo "#########################################################"
								echo "|   Verify message and box hard deletion from Cassandra.|"
								echo "#########################################################"
								echo				
								## POP OPERATIONS START FROM HERE
								echo 
								echo "==========================="
								echo " PERFORMING POP OPERATIONS "
								echo "==========================="
								echo
								echo "Connecting to $POPHost on Port $POPPort";
								echo "Please wait ... "
								echo
								echo > folder.txt
								exec 3<>/dev/tcp/$POPHost/$POPPort
								echo -en "user $user10\r\n" >&3
								echo -en "pass $user10\r\n" >&3
								echo -en "list\r\n" >&3
								echo -en "retr 1\r\n" >&3
								echo -en "retr 2\r\n" >&3
								echo -en "list\r\n" >&3
								echo -en "quit\r\n" >&3
								echo "+++++++++++++++++++++++++"
								cat <&3 >> folder.txt
								#cat folder.txt
								echo "+++++++++++++++++++++++++"
								msg_folder=$(cat folder.txt | grep "OK 2 messages")
								
						
						echo
						imdbcontrol da $user10 openwave.com
						imboxdelete $FirstMSS $MBXID_Sanity10 >> trash
						imboxstats $MBXID_Sanity10 > imboxstats.txt
						msg_msg=$(grep "Failed to read the account information for" imboxstats.txt | cut -d "," -f1 | cut -d ":" -f2 | cut -d " " -f2)
						if [ "$msg_msg" == "Failed" ]
								then
									
									echo "Verified box hard deletion from Cassandra"
								
								else
								    
									echo "ERROR: Unable to verify box hard deletion from Cassandra"
									echo
									echo "ERROR: Please check Manually."
									
								fi
						
								imdbcontrol CreateAccount $user10 $FirstMSS $MBXID_Sanity10 $user10 $user10 clear
								imdbcontrol CreateAccount casmail $FirstMSS 664567 casmail casmail clear openwave.com A S default &>> trash
								#imconfcontrol -install -key "/*/common/mailFolderQuotaEnabled=true"    	&>> trash
								#imconfcontrol -install -key "/*/mss/maintenanceDurationSecs=3600"   &>> trash	
								#imconfcontrol -install -key "/*/mss/maintenanceStartAtSecs=3600"	&>> trash	
								#imconfcontrol -install -key "/*/mss/maintenanceNumThreads=10" 	 &>> trash	
								#imconfcontrol -install -key "/*/mss/expireBeforeBounce=true"		&>> trash
								#imconfcontrol -install -key "/*/mss/quotaMaxMacroDepth=10"		&>> trash
								#imconfcontrol -install -key "/*/mss/mailfolderQuotaPrefix=RSS(1):FSS(1) SSS(1)," &>> trash
								#imconfcontrol -install -key "/*/mss/maintenanceEnabled=true" &>> trash
								#imconfcontrol -install -key  "/*/mss/mailboxAging=true" &>> trash
         						#mconfcontrol -install -key  "/*/mss/mailboxAgingDeleteEnabled=true" &>> trash
								#imconfcontrol -install -key  "/*/mss/mailboxAgingLogEnabled=true" &>> trash
								#imconfcontrol -install -key  "/*/mss/mailboxAgingLogDir=agedMailbox" &>> trash
								#imconfcontrol -install -key  "/*/mss/mailboxAgingLogEnabled=true" &>> trash
								#imconfcontrol -install -key  "/*/mss/mailboxAgingMaxAgeDays=0" &>> trash
								#imconfcontrol -install -key  "/*/mss/mailboxAgingMaxAgeSeconds=2" &>> trash
								#imconfcontrol -install -key  "/*/common/enableCassandraHardDelete=true" &>> trash
								#imdbcontrol sac $user10 openwave.com mailfolderquota "/ all,ageseconds,2" &>> trash
								
							    #~/lib/imservctrl killStart mss &>> trash
								
								
								
								echo "###########################"
								imboxstats casmail@openwave.com > 1.txt
								cat 1.txt
								echo "###########################"
								sleep 2
								
								## Message Delivery Starts
								echo
								echo "------Message Delivery Sessions Starts--------"
								echo
								echo "Connecting to $MTAHost on Port $SMTPPort";
								echo "Please wait ... "
								echo
								echo > queu1.txt
								exec 3<>/dev/tcp/$MTAHost/$SMTPPort

								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:casmail\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:casmail\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "QUIT\r\n" >&3
								echo						
								#cat <&3 
								
								imboxstats casmail@openwave.com >> queu1.txt
				
								java -jar troy-consistency-tool-1.0.0-09-SNAPSHOT-jar-with-dependencies.jar $BackendCluster $BackendPort $BackendCluster $BackendPort 664567 > 664567_1.txt
				
								echo "#############################"
								cat queu1.txt
								echo "#############################"
						
								## POP OPERATIONS START FROM HERE
								echo
								echo "==========================="
								echo " PERFORMING POP OPERATIONS "
								echo "==========================="
								echo
								echo "Connecting to $POPHost on Port $POPPort";
								echo "Please wait ... "
								echo > folder1.txt

								exec 3<>/dev/tcp/$POPHost/$POPPort
								echo -en "user casmail\r\n" >&3
								echo -en "pass casmail\r\n" >&3
								echo -en "list\r\n" >&3
								echo -en "retr 1\r\n" >&3
								echo -en "retr 2\r\n" >&3
								echo -en "dele 1\r\n" >&3
								echo -en "list\r\n" >&3
								echo -en "quit\r\n" >&3
								echo "+++++++++++++++++++++++++"
								cat <&3 >> folder1.txt
								#cat folder1.txt
								msg_folder1=$(cat folder.txt | grep "OK 2 messages")
								echo "+++++++++++++++++++++++++"
											
								#echo > queu.txt
								sleep 10
								
								imboxstats casmail@openwave.com >> queu.txt
								
								echo "#############################"
								cat queu.txt
								echo "#############################"
								
								java -jar troy-consistency-tool-1.0.0-09-SNAPSHOT-jar-with-dependencies.jar $BackendCluster $BackendPort $BackendCluster $BackendPort 664567 > 664567.txt
								msg_msg1=$(strings 664567_1.txt | grep "blobtier:" | wc -l)
								msg_msg2=$(strings 664567.txt | grep "blobtier:" | wc -l)
						        msg_msg="$((msg_msg1-msg_msg2))" 				
								if [ "$msg_msg" == "1" ]
								then
									
									echo "Verified message hard deletion from Cassandra"
									echo
									echo 
								else
								    
									echo "ERROR: Currently we are not able to Verifiey message hard deletion from Cassandra"
									echo
									echo "ERROR: Please check Manually."
									
								fi
								
								
								echo
								imdbcontrol da casmail openwave.com
								imboxdelete $FirstMSS 664567
								
								#end_time_tc deletion_cassendra_tc
								summary "Message hard deletion from cassesndra" $Result
}

function immtacheck_utility() {
								start_time_tc immtacheck_utility_tc
								echo "##########################"
								echo "|   TESTING immtacheck  |"
								echo "##########################"
								echo
								#lib/imservctrl killStart mss								
								#mail send to user1
								mail_send $user1 small 2
								
								immtacheck > immtacheck.txt
					            
								msg_msg=$(grep "$Messages currently being processed"  immtacheck.txt | cut -d ":" -f2)
								msg_msg=`echo $msg_msg | tr -d " "`
								
								echo "Currently "$msg_msg" messages are stored in the mailbox"
								
								
								if [ "$msg_msg" == "" ]
								then
									
									
									echo "ERROR: immtacheck utility is not working fine. Please check Manually."
									Result="1"
								else
									
									echo "immtacheck utility is working fine"
									Result="0"
								fi
								#end_time_tc immtacheck_utility_tc
								summary "immtacheck utility" $Result
}

function bounced_msg_numberquota() {
start_time_tc bounced_msg_numberquota_tc
								echo "###################################################################"
								echo "|   Check for bounced messages when message number quota is hit.  |"
								echo "###################################################################"
								echo
								#immsgdelete $user12@openwave.com -all
								
								imdbcontrol gaf $user12 openwave.com | grep  mailquotamaxmsgs: | cut -d ":" -f2 | sed -e 's/^\s*//' -e 's/\s*$//' > mailbox.txt
								msg_msg=$(grep "4"  mailbox.txt)
								msg_msg=`echo $msg_msg | tr -d " "`
						
								if [ "$msg_msg" == "4" ]
								then
									echo
									echo "We can set "$msg_msg" mailquotamaxmsgs quota for the user $user1" 
									Result="0"
								else
								    
									echo "ERROR: Currently we are not able to set mailquotamaxmsgs for user $user1"
									echo
									echo "ERROR: mailquotamaxmsgs is not working fine. Please check Manually."
									 Result="1"
								fi
								summary "SAC mailquotamaxmsgs" $Result
								start_time_tc
								#sleep 50 	
								#send mail to user12
								mail_send $user12 small 5
								
								echo "#############################"
								cat mail.txt
								echo "#############################"
								msg_msgid=$(grep "Message received:" mail.txt| cut -d ":" -f2 | sed -e 's/^\s*//' -e 's/\s*$//' | tail -1 | cut -d "[" -f1)
							    msg_msg1=$(cat log/mta.log |grep -i "$msg_msgid" |grep -i "bounced:user=")
									
								echo "Bounced message log in mta.log"
								echo "################################"

								echo $msg_msg1

								echo "################################"
								
								msg_msg2=$(cat log/mta.log | grep -i "$msg_msgid" | grep "Warn;MsLimitNumMsgs")
								
								echo "Warning message log in mta.log"
								echo "################################"

								echo $msg_msg2

								echo "################################"
								
								if [ ["$msg_msg1" == " "] -o ["$msg_msg2" == " "] ] 
								then
									
									echo "ERROR: no BOUNCE and WARNING log in mta log file "
									echo
									echo "ERROR: mailquotamaxmsgs is not working fine. Please check Manually."
									Result="1"
								else
									
									echo "Found BOUNCE AND WARNING log in mta file"
									echo
									echo "mailquotamaxmsgs is working fine "
									Result="0"
								fi
								#end_time_tc bounced_msg_numberquota_tc
								summary "Mailquotamaxmsgs " $Result 
}

function bounced_folderquota_msg_kb() {
start_time_tc
								echo "#####################################################################"
								echo "|   Check for bounced messages when folderQuota message kb hit.     |"
								echo "#####################################################################"
								echo
								# $user3@openwave.com -all
								
								#imconfcontrol -install -key "/*/common/mailFolderQuotaEnabled=true"    	
								#imconfcontrol -install -key "/*/mss/maintenanceDurationSecs=3600"   	
								#imconfcontrol -install -key "/*/mss/maintenanceStartAtSecs=3600"		
								#imconfcontrol -install -key "/*/mss/maintenanceNumThreads=10" 	 	
								#imconfcontrol -install -key "/*/mss/expireBeforeBounce=true"		
								#imconfcontrol -install -key "/*/mss/quotaMaxMacroDepth=10"		
								#imconfcontrol -install -key "/*/mss/mailfolderQuotaPrefix=RSS(1):FSS(1) SSS(1),"
								#imconfcontrol -install -key "/*/mss/maintenanceEnabled=true"
								#imdbcontrol sac $user3 openwave.com mailfolderquota "/INBOX all,msgkb,2"
								
								imcheckfq -u $user13@openwave.com | grep "/INBOX all,msgkb,2" |cut -d ":" -f2 | sed -e 's/^\s*//' -e 's/\s*$//' > mailbox.txt					
						        msg_msg_mail=$(grep "/INBOX all,msgkb,2"  mailbox.txt)
											
								echo "We have set the  "$msg_msg_mail" mailfolderquota msgkb quota for $user1"
								echo
								
								if [ "$msg_msg_mail" == "/INBOX all,msgkb,2" ]
								then
									
									echo "We can set "$msg_msg_mail"  mailfolderquota msgkb quota for the user $user1"
									Result="0"
									echo 
								else
								    
									echo "ERROR: Currently we are not able to set mailfolderquota msgkb for user $user1"
									echo
									echo "ERROR:  mailfolderquota msgkb is not working fine. Please check Manually."
									Result="1"
								fi
								summary "SAC mailfolderquota msgkb" $Result
								start_time_tc
								#for host in `cat hosts.txt`
								#do
								#	host=`echo $host | tr -d " "`
								#	imctrl $host killStart mss.1
								#	imservping -h $host mss > ping_resp.txt
								#	resp_count=$(cat ping_resp.txt | grep -i "responded" | wc -l)
								#	resp_count=`echo $resp_count | tr -d " "`
								#	if [ "$resp_count" == "1" ]
								#	then
								#		echo
								#		echo "MSS on "$host" is successfully restarted"
								#	else
								#		echo
								#		echo "ERROR: MSS on "$host" could not be restarted. Please restart manually."
								#		echo
								#	fi
								#done
									
								mail_send $user13 large 1
								
								## Large Message Delivery Starts
								#echo
								#echo "-------Large Message Delivery Sessions Starts--------"
								#echo
								#echo "Connecting to $MTAHost on Port $SMTPPort";
								#echo "Please wait ... "
								#echo
								#echo > mail.txt
								#exec 3<>/dev/tcp/$MTAHost/$SMTPPort

								#echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								#echo -en "RCPT TO:$user1\r\n" >&3
								#echo -en "DATA\r\n" >&3
								#echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								#echo -en "$LARGE_DATA\r\n" >&3
								#echo -en ".\r\n" >&3
								#echo -en "QUIT\r\n" >&3
								#echo						
								#cat <&3 >> mail.txt
								#sleep 5
								echo "#############################"
								#cat mail.txt
								echo "#############################"
								msg_msgid=$(grep "Message received:" mail.txt| cut -d ":" -f2 | sed -e 's/^\s*//' -e 's/\s*$//' | tail -1 | cut -d "[" -f1)
								
								echo > msg.txt
								echo "-------Single Recipient Delivery Ends--------"
								
								echo
								echo
							    cat log/mta.log | grep -i "$msg_msgid" | grep -i "bounced:user=" >> msg.txt
										 

								echo "Bounced message log in mta.log"
								echo "################################"

								cat msg.txt

								echo "################################"
								
								msg_msg2=$(cat msg.txt |grep -i "$msg_msgid" |grep -i "bounced:user=" |cut -d ":" -f2 | sed -e 's/^\s*//' -e 's/\s*$//')
								
								
								
								if [ "$msg_msg2" == "" ] 
								then
									
									echo "ERROR: no BOUNCE and WARNING log in mta log file "
									echo
									echo "ERROR:  mailfolderquota msgkb is not working fine. Please check Manually."
									Result="1"
								else
									
									echo "Found BOUNCE AND WARNING log in mta file"
									echo
									echo " mailfolderquota msgkb is working fine "
									Result="0"
								fi
								summary "mailfolderquota msgkb" $Result


}

function bounced_msg_maint() {
								echo "############################################################################"
								echo "|   Check for bounced messages when maintenance through maintenance server.|"
								echo "############################################################################"
								echo
								start_time_tc		
								## Message Delivery Starts
								mail_send $user14 small 2
								
								imboxstats $user14@openwave.com > queu1.txt
								echo "#############################"
								#cat queu1.txt
								echo "#############################"
								
										## POP OPERATIONS START FROM HERE
								echo
								echo "==========================="
								echo " PERFORMING POP OPERATIONS "
								echo "==========================="
								echo
								echo "Connecting to $POPHost on Port $POPPort";
								echo "Please wait ... "
								echo 

								exec 3<>/dev/tcp/$POPHost/$POPPort
								echo -en "user $user14\r\n" >&3
								echo -en "pass $user14\r\n" >&3
								echo -en "list\r\n" >&3
								echo -en "retr 1\r\n" >&3
								echo -en "retr 2\r\n" >&3
								echo -en "quit\r\n" >&3
								echo "+++++++++++++++++++++++++"
								
								echo "+++++++++++++++++++++++++"
								echo
								echo
					
								imconfcontrol -install -key "/*/common/mailFolderQuotaEnabled=true"   &>> trash
								imconfcontrol -install -key "/*/mss/maintenanceDurationSecs=3600"   &>> trash	
								imconfcontrol -install -key "/*/mss/maintenanceStartAtSecs=3600"	&>> trash	
								imconfcontrol -install -key "/*/mss/maintenanceNumThreads=10" 	 &>> trash	
								imconfcontrol -install -key "/*/mss/expireBeforeBounce=true"	&>> trash	
								imconfcontrol -install -key "/*/mss/quotaMaxMacroDepth=10"		&>> trash
								imconfcontrol -install -key "/*/mss/mailfolderQuotaPrefix=RSS(1):FSS(1) SSS(1)," &>> trash
								imconfcontrol -install -key "/*/mss/maintenanceEnabled=true" &>> trash
								imconfcontrol -install -key  "/*/mss/mailboxAging=true" &>> trash
         						imconfcontrol -install -key  "/*/mss/mailboxAgingDeleteEnabled=true" &>> trash
								imconfcontrol -install -key  "/*/mss/mailboxAgingLogEnabled=true" &>> trash
								imconfcontrol -install -key  "/*/mss/mailboxAgingLogDir=agedMailbox" &>> trash
								imconfcontrol -install -key  "/*/mss/mailboxAgingLogEnabled=true" &>> trash
								imconfcontrol -install -key  "/*/mss/mailboxAgingMaxAgeDays=0" &>> trash
								imconfcontrol -install -key  "/*/mss/mailboxAgingMaxAgeSeconds=2" &>> trash
								
								imdbcontrol sac $user14 openwave.com mailfolderquota "/ all,ageseconds,2"
								imconfcontrol -install -key "/*/mss/maintenanceStartAtSecs=`./ss`" &>> trash
								
							    for host in `cat hosts.txt`
								do
									host=`echo $host | tr -d " "`
									imctrl $host killStart mss.1
									imservping -h $host mss > ping_resp.txt
									resp_count=$(cat ping_resp.txt | grep -i "responded" | wc -l)
									resp_count=`echo $resp_count | tr -d " "`
									if [ "$resp_count" == "1" ]
									then
										echo
										echo "MSS on "$host" is successfully restarted"
									else
										echo
										echo "ERROR: MSS on "$host" could not be restarted. Please restart manually."
										echo
									fi
								done 
								
								echo > queu.txt
								sleep 10
								
								imboxstats $user14@openwave.com > queu.txt
						
								echo "#############################"
								#cat queu.txt
								echo "#############################"
								
								msg_msg=$(grep "Failed to read the mailbox" queu.txt)
								
								
								echo > msg.txt
								echo "-------Single Recipient Delivery Ends--------"
								#sleep 3
								echo
								echo
							    cat log/mss.log | grep -i "Note;MsMailboxAgedOut" | grep -i "was deleted" >> msg.txt
										 

								echo "Message log in mss.log"
								echo "################################"

								cat msg.txt

								echo "################################"
								
								
						        msg_msg=$(grep "Note;MsMailboxAgedOut" msg.txt | tail -1 | cut -d ":" -f2)
								
								if [ "$msg_msg" == "0" ]
								then
									
									echo "We can see the maintenance log in mss.log with no error count"

								else
								    
									echo "ERROR: Currently we are not able to see maintenance log in mss.log"
									echo
									echo "ERROR: Please check Manually."
									
								fi
								
								
								echo
								msg_msg3=$(diff queu.txt queu1.txt)
								if [ "$msg_msg3" != "" ]
								then
									
									echo "Maintenance through maintenance server is woring fine"
									Result="0"
									echo 
								else
								    
									echo "ERROR: Currently we are not able to do maintenance through maintenance server"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								summary "Maintenance through maintenance server" $Result
}

function delete_invalid_msg() {
start_time_tc delete_invalid_msg_tc
								echo "###################################################"
								echo "| Negative test case for incorrect message delete. |"
								echo "###################################################"
								echo
								echo
								#user15=Sani111111115
								#MBXID_Sanity15="15"$(echo $RANDOM)
								#MBXID_Sanity15=`echo $MBXID_Sanity15 | tr -d " "`
								#imdbcontrol ca $user15 $Cluster $MBXID_Sanity15 $user15 $user15 clear 
							    ## Single recipient delivery
								echo "-------Single Recipient Delivery Starts--------"
								echo
								echo "Connecting to $MTAHost on Port $SMTPPort";
								echo "Please wait ... "
								echo
								
								exec 3<>/dev/tcp/$MTAHost/$SMTPPort
								
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user14\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user14\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "QUIT\r\n" >&3								
								#cat <&3 
								
								{
									echo rf $MBXID_Sanity14 INBOX;
									echo dm $MBXID_Sanity14 INBOX;
									echo +++;
									echo 2347239472949;
									echo 0890248203948;
									echo 1309231089302;
									echo 5498502344094;
									echo 5242424534223;
									echo +++;
									echo quit;
								} | imcl_apitest $Cluster | tee trace.txt
								#cat trace.txt
								
								
								 
							msg_error=$(cat trace.txt |grep "ProcAssertFail" | wc -l)
							
								if [ "$msg_error" == "0" ]
								then

								echo "Negative test for incorrect message delete is not sending error"
								Result="0"
								else
							
								echo "Negative test for incorrect message delete is sending error"
								Result="1"
								fi
								#end_time_tc delete_invalid_msg_tc
								summary "Negative test for incorrect message delete" $Result
}

function delete_invalid_long_msgid() {
start_time_tc delete_invalid_long_msgid_tc
								echo "##############################################################################"
								echo "|Negative test for incorrect message ID in error message for long message id |"
								echo "##############################################################################"
								{
									echo rf $MBXID_Sanity4 INBOX;
									echo dm $MBXID_Sanity4 INBOX;
									echo +++;
									echo 91919191919;
									echo 92030982390;
									echo 09823987232;
									echo 23208200000;
									echo +++;
									echo quit;
								} | imcl_apitest $Cluster &> trace.txt
								#cat trace.txt
								
								#msg_error=$(cat trace.txt |grep "1724878703" | wc -l)
								msg_error=$(cat trace.txt |grep "91919191919" | wc -l)
								
							
							    if [ "$msg_error" == "1" ]
								then
								
								echo "Negative test for incorrect long message delete is giving not incorrect message id in error message when giving large message id"
								Result="0"
								else
								
								echo "Negative test for incorrect long message delete is giving incorrect message id in error message when giving large message id"
								echo
								Result="1"
								fi
								#end_time_tc delete_invalid_long_msgid-tc
								summary "Negative test for incorrect long message delete" $Result "ITS:1325354"
}

function delete_invalid_samll_msgid() {
start_time_tc delete_invalid_samll_msgid_tc
								echo "############################################################################"
								echo "Negative test for incorrect message ID in error message for small message id"
								echo "############################################################################"
								{
									echo rf $MBXID_Sanity3 INBOX;
									echo dm $MBXID_Sanity3 INBOX;
									echo +++;
									echo 919191;
									echo 920309;
									echo 098239;
									echo 232082;
									echo +++;
									echo quit;
								} | imcl_apitest $Cluster &> trace1.txt
								#cat trace1.txt
								msg_error=$(cat trace1.txt |grep "919191" | wc -l)
							    
							    if [ "$msg_error" == "1" ] 
								then
								
								
								echo "Negative test for incorrect message delete is not giving incorrect message id in error message when giving small message id"
								Result="0"
								else
								Result="1"
								
								echo "Negative test for incorrect message delete is giving incorrect message id in error message when giving small message id"
								
								fi
								#end_time_tc delete_invalid_samll_msgid_tc
								summary "Negative test for message delete small" $Result "MX-748"
}

function imboxattctrl_utility() {
start_time_tc imboxattctrl_utility_tc
								echo "##########################"
								echo "|   TESTING imboxattctrl  |"
								echo "##########################"
								
								imboxattrctrl > automsg
							    imboxattrctrl set -1 $user1 openwave.com automsg > imboxatt.txt
								
								msg_msg=$(grep "1"  imboxatt.txt)
								msg_msg=`echo $msg_msg | tr -d " "`
								
								echo "We have set the  "$msg_msg" auto reply for this user $user1"
								
								
								if [ "$msg_msg" == "" ]
								then
									
									echo "ERROR: Currently we are not able to set auto reply for user $user1"
									echo
									echo "ERROR: imboxattrctrl utility is not working fine. Please check Manually."
									Result="1"
								else
									
									echo "We can set "$msg_msg" auto reply for the user $user1"
									echo
									echo "imboxattrctrl utility is working fine"
									Result="0"
								fi
								
							    #echo "XX-----Testing of imboxattrctrl utility is completed-----XX"
								#end_time_tc imboxattctrl_utility_tc
								summary "imboxattrctrl" $Result
}

function acc_creation_new_cos_domain() {
start_time_tc acc_creation_new_cos_domain_tc
								echo "######################################################"
								echo "|   Verify account creation with new domain and cos. |"		
								echo "######################################################"
							    
								imdbcontrol cc newcosclass &>> trash
								imdbcontrol lcn &> cos.txt
								imdbcontrol cd imsanity.com  &>> trash
								imdbcontrol ld > domain.txt
								
								
								
								msg_msg=$( grep "newcosclass" cos.txt | wc -l )
								 if [ $msg_msg == "1" ]
								then

									echo "We are able to create new cos class of service"
									
									Result="0"
									 
								else
									
									echo "ERROR: We are not able to create new cos class of service"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								summary "create new cos class of service" $Result
								start_time_tc acc_creation_new_cos_domain_tc
								msg_msg=$( grep "imsanity.com" domain.txt | wc -l )
								 if [ $msg_msg == "1" ]
								then

									echo "We are able to create new domain"
									
									Result="0"
									
								else
									
									echo "ERROR: We are not able to create new domain"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi

								summary "create new domain" $Result
								start_time_tc acc_creation_new_cos_domain_tc 
								imdbcontrol sca newcosclass maildeliveryoption P &>> trash
								imdbcontrol sca newcosclass mailforwarding 0 &>> trash
								
								imdbcontrol CreateAccount imsanityuser $Cluster 145672 imsanityuser imsanityuser clear imsanity.com A S newcosclass &>> trash
								
								msg_msg=$(imdbcontrol la | grep "imsanityuser@imsanity.com" | wc -l)
								if [ $msg_msg == "1" ]
								then

									echo "We are able to create new user with new domain and cos"
									Result="0"

								else

									echo "ERROR: We are not able to create new user with new domain and cos"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								summary "create new user with new domain and cos" $Result
								
								imdbcontrol da imsanityuser imsanity.com  &>> trash
								imdbcontrol dd imsanity.com  &>> trash
								imdbcontrol uca newcosclass maildeliveryoption  &>> trash
								imdbcontrol uca newcosclass mailforwarding &>> trash
								imdbcontrol dc newcosclass &>> trash
								
								
								#end_time_tc acc_creation_new_cos_domain_tc
}

function imfilterctrl_utility() {
start_time_tc imfilterctrl_utility_tc
								echo "#############################"
								echo "|   Check for imfilterctrl  |"
								echo "#############################"

								
								echo "if header \"from\" contains \"imail1@openwave.com\" {" > imfilter.txt
								echo "forward \"testuser6@openwave.com\";" >> imfilter.txt
								echo "}" >> imfilter.txt
								
								imfilterctrl set $user1 openwave.com imfilter.txt
								
								imfilterctrl get $user1 openwave.com > imfilter1.txt
								
								msg_msgimfilter=$(diff imfilter.txt imfilter1.txt)
								if [ "$msg_msgimfilter" == "" ]
								then
									echo "imfilterctrl set the forward rule"
									echo
									echo "imfilterctrl is working fine "
									Result="0"
									
								else
									
									echo "ERROR: imfilterctrl cant set the filter rule "
									echo
									echo "ERROR: imfilterctrl is not working fine. Please check Manually."
									Result="1"
								fi
								#end_time_tc imfilterctrl_utility_tc
								summary "imfilterctrl" $Result
}

function mailing_list() {
start_time_tc mailing_list_tc
								echo "##############################"
								echo "|   Check for mailing list   |"
								echo "##############################"

								#imconfcontrol -install -key "/*/mta/listProcessingDisable=false" &>> trash
								#lib/imservctrl killStart mta &>> trash
								
								#immsgdelete $user1@openwave.com -all &>> trash
								deleteMessages $user1 $user1 $MBXID_Sanity1
								imdbcontrol CreateAccount ownermailinglist $Cluster 667777 ownermailinglist ownermailinglist clear 
								
								immlistcontrol create dc=openwave,dc=com 'cn=default,cn=admin root' testlistsanity testlistsanity@openwave.com ownermailinglist@openwave.com testlistsanity-request@openwave.com $Cluster 378654
								immlistcontrol subscribe mail=testlistsanity@openwave.com,dc=openwave,dc=com $user1@openwave.com immediate
								

								## Single recipient delivery
								echo "-------Single Recipient Delivery Starts--------"
								echo
								echo "Connecting to $MTAHost on Port $SMTPPort";
								echo "Please wait ... "
								echo
								mailto="testlistsanity"
								exec 3<>/dev/tcp/$MTAHost/$SMTPPort
								
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$mailto\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$mailto\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "QUIT\r\n" >&3								
								#cat <&3
								sleep 6
								
						## POP OPERATIONS 
						echo
						echo "==========================="
						echo " PERFORMING POP OPERATIONS "
						echo "==========================="
						echo
						echo "Connecting to $POPHost on Port $POPPort";
						echo "Please wait ... "
						echo
						
						exec 3<>/dev/tcp/$POPHost/$POPPort
						echo -en "user $user1\r\n" >&3
						echo -en "pass $user1\r\n" >&3
						echo -en "list\r\n" >&3
						echo -en "retr 1\r\n" >&3
						echo -en "quit\r\n" >&3
						echo "+++++++++++++++++++++++++"
						#cat <&3 
						echo "+++++++++++++++++++++++++"
						echo
						
						immsgdump $Cluster $MBXID_Sanity1 1 > mailinglist.txt 
						
						msg_msg=$(grep "for <testlistsanity@openwave.com>;" mailinglist.txt)
								msg_msg=`echo $msg_msg | tr -d " "`
								
								
								if [ "$msg_msg" != "" ]
								then
									echo "immailing list is working fine"
									Result="0"

								else
									
									echo "ERROR: immailing list is not working fine"
									echo
									echo "ERROR: immailing list is not working fine. Please check Manually."
									Result="1"
								fi
								#end_time_tc mailing_list_tc
								summary "immailing list" $Result
}

function imap_move() {
start_time_tc imap_move_tc
								echo "##############################"
								echo "|   Check forIMAP move       |"
								echo "##############################"

								## IMAP OPERATIONS START FROM HERE
							#echo
							#echo "=========================="
							#echo "PERFORMING IMAP OPERATIONS"
							#echo "=========================="
							#echo
							#echo "Connecting to $IMAPHost on Port $IMAPPort";
							#echo "Please wait ... "
							#echo
							#immsgdelete $user1@openwave.com -all
							## Single recipient delivery
								#echo "-------Single Recipient Delivery Starts--------"
								#echo
								#echo "Connecting to $MTAHost on Port $SMTPPort";
								#echo "Please wait ... "
								#echo
								
								#exec 3<>/dev/tcp/$MTAHost/$SMTPPort
								
								#echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								#echo -en "RCPT TO:$user1\r\n" >&3
								#echo -en "DATA\r\n" >&3
								#echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								#echo -en "$DATA\r\n" >&3
								#echo -en ".\r\n" >&3
								#echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								#echo -en "RCPT TO:$user1\r\n" >&3
								#echo -en "DATA\r\n" >&3
								#echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								#echo -en "$DATA\r\n" >&3
								#echo -en ".\r\n" >&3
								#echo -en "QUIT\r\n" >&3	
								#echo -en "QUIT\r\n" >&3								
								#cat <&3
								#sleep 10
							echo > move.txt
							#imconfcontrol -install -key "/*/imapserv/enableMOVE=true" &>>trash.txt
							exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

							echo -en "a login $user1@openwave.com $user1\r\n" >&3
							echo -en "a select INBOX\r\n" >&3				
							echo -en "a move 1 inbox\r\n" >&3
							echo -en "a logout\r\n" >&3
							#echo "+++++++++++++++++++++++++"
							cat <&3 >> move.txt
							#cat imap.txt
							#echo "+++++++++++++++++++++++++"
							echo
							msg_msg=$(grep "BAD Missing required argument to MOVE: destinationMailbox." move.txt | wc -l)
								#msg_msg=`echo $msg_msg | tr -d " "`
								
								if [ "$msg_msg" == "1" ]
								then
									echo "IMAP MOVE is working fine"

									Result="0"
									
								else

									echo "ERROR: IMAP MOVE is not working fine"
									echo
									echo "ERROR: IMAP MOVE is not working fine. Please check Manually."
									Result="1"
								fi
								#end_time_tc imap_move_tc
								summary "IMAP MOVE" $Result 
}

function uid_imap() {
start_time_tc uid_imap_tc
								echo "###################################################"
								echo "| Verify UID shown by copy command in IMAP session|"
								echo "###################################################"

								user18=Sani111111118
								MBXID_Sanity18="18"$(echo $RANDOM)
								MBXID_Sanity18=`echo $MBXID_Sanity18 | tr -d " "`
								imdbcontrol ca $user18 $Cluster $MBXID_Sanity18 $user18 $user18 clear openwave.com A S default
								
							    ## Single recipient delivery
								echo "-------Single Recipient Delivery Starts--------"
								echo
								echo "Connecting to $MTAHost on Port $SMTPPort";
								echo "Please wait ... "
								echo
								 
								exec 3<>/dev/tcp/$MTAHost/$SMTPPort
								
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user18\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user18\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user18\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user18\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "QUIT\r\n" >&3								
								#cat <&3 
								
								
								
							echo > copy.txt
							exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

							echo -en "a login $user18@openwave.com $user18\r\n" >&3
							echo -en "a select INBOX\r\n" >&3
							echo -en "a copy 1 Trash\r\n" >&3
							echo -en "a select Trash\r\n" >&3
							echo -en "a copy 1 INBOX\r\n" >&3
							echo -en "a select INBOX\r\n" >&3
							echo -en "a fetch 1:*UID\r\n" >&3
							echo -en "a fetch 1:* UID\r\n" >&3
							echo -en "a logout\r\n" >&3
							#echo "+++++++++++++++++++++++++"
							cat <&3 >> copy.txt
							#cat imap.txt
							#echo "+++++++++++++++++++++++++"
								


								 
							    msg_count=$(cat copy.txt| grep "COPY completed" | wc -l)
								
								
								if [ "$msg_count" == "2" ]
								then

								echo "Copy is working as expected in IMAP"
								Result="0"
								else

								echo "Copy is not working as expected in IMAP. Please check manually."
								Result="1"
								
								fi
								#end_time_tc uid_imap_tc
								summary "IMAP copy" $Result
}

function imap_fetch_full() {
start_time_tc imap_fetch_full_tc
								echo "#############################"
								echo "| Check for imap fetch full |"
								echo "#############################"

								user13=Sani111111113
								MBXID_Sanity13="13"$(echo $RANDOM)
								MBXID_Sanity13=`echo $MBXID_Sanity13 | tr -d " "`
								imdbcontrol ca $user13 $Cluster $MBXID_Sanity13 $user13 $user13 clear openwave.com A S default
							    ## Single recipient delivery
								echo "-------Single Recipient Delivery Starts--------"
								echo
								echo "Connecting to $MTAHost on Port $SMTPPort";
								echo "Please wait ... "
								echo
								
								exec 3<>/dev/tcp/$MTAHost/$SMTPPort
								
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user13\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user13\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "QUIT\r\n" >&3								
								#cat <&3 
								sleep 5
							 				 				
							#echo "=========================="
							#echo "PERFORMING IMAP OPERATIONS"
							#echo "=========================="
							#echo
							#echo "Connecting to $IMAPHost on Port $IMAPPort";
							#echo "Please wait ... "
							#echo
							echo > imap.txt
							
							exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

							echo -en "a login $user13@openwave.com $user13\r\n" >&3
							echo -en "a list \"\" *\r\n" >&3
							echo -en "a select INBOX\r\n" >&3				
							echo -en "a fetch 1:* full\r\n" >&3
							echo -en "a logout\r\n" >&3
							#echo "+++++++++++++++++++++++++"
							cat <&3 &>> imap.txt
							#cat imap.txt

                                msh_immss=$(cat imap.txt | grep "a OK FETCH completed" | wc -l)
								
								if [ "$msh_immss" == "1" ]
								then

									echo "IMAP Ftech full is working fine"
									Result="0"

								else
									
									echo "ERROR:IMAP Ftech full is not working fine"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi 
								#end_time_tc imap_fetch_full_tc
								summary "IMAP Ftech full" $Result
}

function imap_idle() {
start_time_tc imap_idle_tc
								echo "##############################"
								echo "|   Check for IMAP IDLE      |"
								echo "##############################"
								echo
								echo
								## IMAP OPERATIONS START FROM HERE
							#echo
							#echo "=========================="
							#echo "PERFORMING IMAP OPERATIONS"
							#echo "=========================="
							#echo
							#echo "Connecting to $IMAPHost on Port $IMAPPort";
							#echo "Please wait ... "
							#echo
							#immsgdelete $user1@openwave.com -all
							## Single recipient delivery
								#echo "-------Single Recipient Delivery Starts--------"
								#echo
								#echo "Connecting to $MTAHost on Port $SMTPPort";
								#cho "Please wait ... "
								#echo
								
								#exec 3<>/dev/tcp/$MTAHost/$SMTPPort
								
								#echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								#echo -en "RCPT TO:$user1\r\n" >&3
								#echo -en "DATA\r\n" >&3
								#echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								#echo -en "$DATA\r\n" >&3
								#echo -en ".\r\n" >&3
								#echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								#echo -en "RCPT TO:$user1\r\n" >&3
								#echo -en "DATA\r\n" >&3
								#echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								#echo -en "$DATA\r\n" >&3
								#echo -en ".\r\n" >&3
								#echo -en "QUIT\r\n" >&3	
								#echo -en "QUIT\r\n" >&3								
								#cat <&3
								#sleep 10
							#echo > imap.txt
							
							#imconfcontrol -install -key "/*/imapserv/enableIdle=true"
							exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

							echo -en "a login $user1@openwave.com $user1\r\n" >&3
							echo -en "a list \"\" *\r\n" >&3
							echo -en "a select INBOX\r\n" >&3				
							echo -en "a idle\r\n" >&3
							echo -en "DONE\r\n" >&3
							echo -en "a logout\r\n" >&3
							#echo "+++++++++++++++++++++++++"
							cat <&3 &>> idle.txt
							#cat idle.txt
							#echo "+++++++++++++++++++++++++"
							echo
								msg_msg=$(grep "+ idling" idle.txt |wc -l)
								msg_msg=`echo $msg_msg | tr -d " "`
								
								
								if [ "$msg_msg" != "" ]
								then
									echo "IMAP IDLE is working fine"
									
									Result="0"
									
								else
									
									echo "ERROR: IMAP IDLE is not working fine"
									echo
									echo "ERROR: IMAP IDLE is not working fine. Please check Manually."
									Result="1"
								fi
								#end_time_tc imap_idle_tc
								summary "IMAP IDLE" $Result
}

function imap_getquota_root() {
start_time_tc imap_getquota_root_tc
								echo "#################################"
								echo "| Check for imap get quota root |"
								echo "#################################"

								#user14=Sani111111114
								#MBXID_Sanity14="14"$(echo $RANDOM)
								#MBXID_Sanity14=`echo $MBXID_Sanity14 | tr -d " "`
								#imdbcontrol ca $user14 $Cluster $MBXID_Sanity14 $user14 $user14 clear openwave.com A S default
							    ## Single recipient delivery
								#echo "-------Single Recipient Delivery Starts--------"
								#echo
								#echo "Connecting to $MTAHost on Port $SMTPPort";
								#echo "Please wait ... "
								#echo
								
								#exec 3<>/dev/tcp/$MTAHost/$SMTPPort
								
								#echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								#echo -en "RCPT TO:$user1\r\n" >&3
								#echo -en "DATA\r\n" >&3
								#echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								#echo -en "$DATA\r\n" >&3
								#echo -en ".\r\n" >&3
								#echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								#echo -en "RCPT TO:$user1\r\n" >&3
								#echo -en "DATA\r\n" >&3
								#echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								#echo -en "$DATA\r\n" >&3
								#echo -en ".\r\n" >&3
								#echo -en "QUIT\r\n" >&3								
								#cat <&3 
								#sleep 5
							 				 				
							echo "=========================="
							echo "PERFORMING IMAP OPERATIONS"
							echo "=========================="
							echo
							echo "Connecting to $IMAPHost on Port $IMAPPort";
							echo "Please wait ... "
							echo
							echo > root.txt
							
							exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

							echo -en "a login $user1@openwave.com $user1\r\n" >&3
							echo -en "a list \"\" *\r\n" >&3
							echo -en "a select INBOX\r\n" >&3				
							echo -en "a getquotaroot INBOX\r\n" >&3
							echo -en "a logout\r\n" >&3
							#echo "+++++++++++++++++++++++++"
							cat <&3 >> root.txt
							#cat imap.txt 

                               # msh_immss=$(cat imap.txt | grep "QUOTA \"\" " | cut -d "(" -f2 | cut -d ")" -f1 | grep "MESSAGE" | wc -l)
								msh_immss=$(cat root.txt | grep "QUOTA \"\" " | cut -d "(" -f2 | cut -d ")" -f1 | grep "MESSAGE" | wc -l)
								if [ "$msh_immss" == "1" ]
								then

									echo "IMAP get quota root is working fine"
									Result="0"

								else
									
									echo "ERROR:IMAP get quota root is not working fine"
									echo
									echo "ERROR: Please check Manually."
									Result="1"
								fi
								#end_time_tc imap_getquota_root_tc
								summary "IMAP get quota root" $Result "ITS:1324836"
}

function imap_sort() {
start_time_tc imap_sort_tc
								echo "##############################"
								echo "|   Check for IMAP SORT      |"
								echo "##############################"

								## IMAP OPERATIONS START FROM HERE
							echo
							echo "=========================="
							echo "PERFORMING IMAP OPERATIONS"
							echo "=========================="
							echo
							echo "Connecting to $IMAPHost on Port $IMAPPort";
							echo "Please wait ... "
							echo
							#immsgdelete $user1@openwave.com -all &>> trash.txt
							deleteMessages $user1 $user1 $MBXID_Sanity1
							## Single recipient delivery
								echo "-------Single Recipient Delivery Starts--------"
								echo
								echo "Connecting to $MTAHost on Port $SMTPPort";
								echo "Please wait ... "
								echo
								
								exec 3<>/dev/tcp/$MTAHost/$SMTPPort
								
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user1\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user1\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "QUIT\r\n" >&3	
								echo -en "QUIT\r\n" >&3								
								#cat <&3
								sleep 6							
								echo > imap.txt
							
							#imconfcontrol -install -key "/*/imapserv/enableSORT=true" &>> trash.txt
							exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

							echo -en "a login $user1@openwave.com $user1\r\n" >&3
							echo -en "a list \"\" *\r\n" >&3
							echo -en "a select INBOX\r\n" >&3				
							echo -en "a uid sort (arrival) us-ascii all\r\n" >&3
							echo -en "a uid fetch 1:* rfc822.size\r\n" >&3
							echo -en "a logout\r\n" >&3
							#echo "+++++++++++++++++++++++++"
							cat <&3 >> sort.txt
							#cat sort.txt
							#echo "+++++++++++++++++++++++++"
							echo
								msg_msg=$(grep "a OK UID FETCH completed" sort.txt)
								msg_msg=`echo $msg_msg | tr -d " "`
								
								
								if [ "$msg_msg" != "" ]
								then
									echo "IMAP SORT is working fine"
									Result="0"

								else
									
									echo "ERROR: IMAP SORT is not working fine"
									echo
									echo "ERROR: IMAP SORT is not working fine. Please check Manually."
									Result="1"
								fi
								#end_time_tc imap_sort_tc
								summary "IMAP SORT" $Result
}

function imcl_api_test() {
start_time_tc imcl_api_test_tc
								###C-API TESTING START FROM HERE

								### COPYING IMCL_APITEST AND APITEST UTILITES FOR TESTING
								echo
								echo "##########################"
								echo "|  IMCL_APITEST TESTING  |"
								echo "##########################"
								echo

								
								exec 3<>/dev/tcp/$MTAHost/$SMTPPort
								
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user4\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user4\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "QUIT\r\n" >&3									
								cat <&3 > trace.txt

								### imcl_apitest operations
								{
									echo rf $MBXID_Sanity4 INBOX;
									echo cm $MBXID_Sanity4 INBOX;
									echo hello;
									echo +++;
									echo +++;
									echo rf $MBXID_Sanity4 INBOX;
									msgId=$(cat trace.txt | grep -i msgId | cut -d " " -f2 | cut -d "\"" -f2 | sort -u | grep -i "-" | head -1)
									msgId=`echo $msgId | tr -d " "`
									echo rm $MBXID_Sanity4 INBOX;
									echo $msgId
									echo +++;
									echo +++;
									echo +++;
									echo +++;
									echo +++;
									echo +++;
									echo cf $MBXID_Sanity4 ABC;
									echo cf $MBXID_Sanity4 ABC1;
									echo mm $MBXID_Sanity4 INBOX ABC;
									echo $msgId
									echo +++;
									echo +++;
									echo rm $MBXID_Sanity4 INBOX;
									echo $msgId
									echo +++;
									echo +++;
									echo +++;
									echo +++;
									echo +++;
									echo +++;
									echo mm $MBXID_Sanity4 ABC INBOX;
									echo $msgId
									echo +++;
									echo +++;
									echo rm $MBXID_Sanity4 INBOX;
									echo $msgId
									echo +++;
									echo +++;
									echo +++;
									echo +++;
									echo +++;
									echo +++;
									echo pm $MBXID_Sanity4 INBOX ABC;
									echo $msgId
									echo +++;
									echo +++;
									echo dm $MBXID_Sanity4 ABC;
									echo $msgId
									echo +++;
									echo +++;
									echo rm $MBXID_Sanity4 ABC;
									echo $msgId
									echo +++;
									echo +++;
									echo +++;
									echo +++;
									echo +++;
									echo +++;
									echo um $MBXID_Sanity4 INBOX Recent
									echo +++;
									echo +++;
									echo +++;
									echo +++;
									echo +++;
									echo sac $MBXID_Sanity4 INBOX;
									echo mailquotamaxmsgs:2;
									echo +++;
									echo nf $MBXID_Sanity4 ABC ABC1;
									echo rf $MBXID_Sanity4 ABC1;
									echo df $MBXID_Sanity4 ABC1;
									#echo rf $MBXID_Sanity4 ABC1;
									echo df $MBXID_Sanity4 ABC;
									#echo rf $MBXID_Sanity4 ABC;
									
									echo quit;
								} | imcl_apitest $Cluster | tee trace.txt
								#cat trace.txt
								echo  
								echo "ERRORS During imcl_apitest testing"
								echo "=================================="
								cat trace.txt | egrep -i "erro|fatl|fail|urgt|warn"
								err=$(cat trace.txt | egrep -i "ERROR|fail|urgt|warn" | wc -l)
								echo
								echo
								echo
								echo "IMCL_APITEST Testing is completed."
								#end_time_tc imcl_api_test_tc 
								if [ "$err" == "0" ]
								then
									echo "No error in IMCL api test"
									Result="0"

								else
									
									echo "ERROR: IMCL api test error found"
									echo
									echo "ERROR:Please check Manually."
									Result="1"
								fi
								
								for ((i=1;i<13;i++))
							do
							summary "IMCL Test case $i" $Result
						done

}

function api_test() {
start_time_tc api_test_tc
								###C-API TESTING START FROM HERE

								### COPYING APITEST  UTILITES FOR TESTING
								echo
								echo "##########################"
								echo "|  APITEST TESTING  |"
								echo "##########################"
								echo
								#user8=Sani111118
								#MBXID_Sanity8="8"$(echo $RANDOM)
								#MBXID_Sanity8=`echo $MBXID_Sanity8 | tr -d " "`
								#imdbcontrol ca $user8 $Cluster $MBXID_Sanity8 $user8 $user8 clear
								
												## Single recipient delivery
								echo "-------Single Recipient Delivery Starts--------"
								echo
								echo "Connecting to $MTAHost on Port $SMTPPort";
								echo "Please wait ... "
								echo

								exec 3<>/dev/tcp/$MTAHost/$SMTPPort

								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user8\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "MAIL FROM:$MAILFROM\r\n" >&3
								echo -en "RCPT TO:$user8\r\n" >&3
								echo -en "DATA\r\n" >&3
								echo -en "Subject: $SUBJECT\r\n\r\n" >&3
								echo -en "$DATA\r\n" >&3
								echo -en ".\r\n" >&3
								echo -en "QUIT\r\n" >&3
								#cat <&3 
								echo
								mail_send $user8 small 1
								#sm.pl -u $user8 -f $MAILFROM -H sanity msg
								echo "-------Single Recipient Delivery Ends--------"
								#echo "Please wait while we copy imcl_apitest and apitest utilities to testing location..."
								#echo
								#capi_path=$(strings $INTERMAIL/lib/mta | grep -i build_root | cut -d "=" -f2)
								#capi_path=`echo $capi_path | tr -d " "`
								#cp "$capi_path"/mercury/c-api/imcl_apitest .
								#cp "$capi_path"/mercury/c-api/apitest .
								#echo
								#echo "C-API utilities copied. Starting operations..."
								echo						
								imdbcontrol cd sanity.com
							    smtpadd="$user8@openwave.com"
								{
								 
								echo rf $MBXID_Sanity8 INBOX;
								echo
								echo
								echo ua $smtpadd;
								echo
								echo
								echo fd $smtpadd;
								echo
								echo
								echo dd sanity.com
								echo
								echo
								echo quit;
								} | apitest $Cluster &>trace1.txt
								
								#cat trace1.txt								
								
								msg_id=$(cat trace1.txt  | egrep -i "not_private Mail" | tail -1 | cut -d "\"" -f2)
								msg_ud=$(cat trace1.txt  | egrep -i "UIDValidity" | tail -1 | cut -d "\"" -f1 |cut -d " " -f2)
								echo 
								{
									echo rmm $MBXID_Sanity8 INBOX $msg_id;
									echo
								    echo
									echo rmb $MBXID_Sanity8 INBOX $msg_id;
									echo
								    echo
									echo rbu $MBXID_Sanity8;
									echo
								    echo
									echo rb $MBXID_Sanity8;
									echo
								    echo
									echo cw $user1@openwave.com $smtpadd;
									echo
								    echo 
									echo fe $user1@openwave.com;
									echo
								    echo
									echo dw $user1@openwave.com $smtpadd;
									echo
								    echo
									echo rmi $MBXID_Sanity8 INBOX $msg_uid;
									echo
								    echo
									echo rmh $MBXID_Sanity8 INBOX $msg_uid sanity;
									echo
								    echo
									echo rp $user1@openwave.com;
									echo
								    echo
								    echo quit;
								}| apitest $Cluster | tee trace.txt
								
								echo "ERRORS During imcl_apitest testing"
								echo "=================================="
								cat trace.txt | egrep -i "erro|fatl|fail|urgt|warn"
								cat trace1.txt | egrep -i "erro|fatl|fail|urgt|warn"
								echo 
								echo
								echo
								echo
								echo
								 
								msg_err=$(cat trace.txt | egrep -i "erro|fatl|fail|urgt|warn"| wc -l)
								msg_err1=$(cat trace1.txt | egrep -i "erro|fatl|fail|urgt|warn" | wc -l)
								echo
								echo
								echo
								echo "APITEST Testing is completed."
								if [ "$msg_err" == "0" ]
								then
									echo "No Error found in API test"
									Result="0"

								else
									
									echo "ERROR:Error found in API test"
									echo
									echo "ERROR:Please check Manually."
									Result="1"
								fi
								
								#end_time_tc api_test_tc
								for ((i=1;i<16;i++))
							do
							summary "API Test case $i" $Result
						done

}

function surrogate_testing() {
start_time_tc surrogate_testing_tc
								#SMTP Operations using surrogate mss startes from here
								echo "============================================="
								echo "PERFORMING SMTP OPERATIONS WITH SURROGATE MSS"
								echo "============================================="
								echo

								## Bringing down owner mss
								ownerMSS=$(cat $INTERMAIL/log/mta.log| egrep -i delivered | grep -i $user1 | cut -d ":" -f4 | cut -d "=" -f2 | sort -u )
								ownerMSS=`echo $ownerMSS | tr -d " "`
								echo "Owner MSS of the mailbox used is: "$ownerMSS
								echo
								echo "Please wait while we bring down owner MSS... "
								imctrl $ownerMSS kill mss.1
								imservping -h $ownerMSS mss > ping_resp.txt
								resp_count=$(cat ping_resp.txt | grep -i "unreachable" | wc -l)
								resp_count=`echo $resp_count | tr -d " "`
								if [ "$resp_count" == "2" ]
								then
								    echo
								    echo "MSS on "$ownerMSS" is successfully shutdown"
								    echo
								    sleep 20
									echo "Processing Surrogate Testing.... "
									echo
									## Single recipient delivery
									echo "-------Single Recipient Delivery Starts--------"
									echo
									echo "Connecting to $MTAHost on Port $SMTPPort";
									echo "Please wait ... "
									echo

									exec 3<>/dev/tcp/$MTAHost/$SMTPPort

									echo -en "MAIL FROM:$MAILFROM\r\n" >&3
									echo -en "RCPT TO:$user1\r\n" >&3
									echo -en "DATA\r\n" >&3
									echo -en "Subject: $SUBJECT\r\n\r\n" >&3
									echo -en "$DATA\r\n" >&3
									echo -en ".\r\n" >&3
									echo -en "MAIL FROM:$MAILFROM\r\n" >&3
									echo -en "RCPT TO:$user1\r\n" >&3
									echo -en "DATA\r\n" >&3
									echo -en "Subject: $SUBJECT\r\n\r\n" >&3
									echo -en "$DATA\r\n" >&3
									echo -en ".\r\n" >&3
									echo -en "QUIT\r\n" >&3
									cat <&3
									echo
									echo "-------Single Recipient Delivery Ends--------"
									echo

									exec 3<>/dev/tcp/$MTAHost/$SMTPPort

									echo -en "MAIL FROM:$MAILFROM\r\n" >&3
									echo -en "RCPT TO:$user1\r\n" >&3
									echo -en "DATA\r\n" >&3
									echo -en "Subject: $SUBJECT\r\n\r\n" >&3
									echo -en "$DATA\r\n" >&3
									echo -en ".\r\n" >&3
									echo -en "MAIL FROM:$MAILFROM\r\n" >&3
									echo -en "RCPT TO:$user1\r\n" >&3
									echo -en "DATA\r\n" >&3
									echo -en "Subject: $SUBJECT\r\n\r\n" >&3
									echo -en "$DATA\r\n" >&3
									echo -en ".\r\n" >&3
									echo -en "QUIT\r\n" >&3
									cat <&3
									echo
									echo "-------Single Recipient Delivery Ends--------"
									echo


									## Check if mails were delivered successfully or not
									imboxstats $MBXID_Sanity1 > boxstats_Sanity1.txt
									msgs=$(grep "Total Messages Stored"  boxstats_Sanity1.txt | cut -d " " -f6)
									msgs=`echo $msgs | tr -d " "`
									if [ $msgs -gt 0 ]
									#if [ "$msgs" == "2" ]
									then
									    echo $msgs" Mails were delivered successfully."
									    echo

										
									    ## Check for mta errors in logs
									    cat $INTERMAIL/log/mta.log| egrep -i "erro;|urgt;|fatl;" > mta_errors.txt
									    echo "Errors logged in MTA Logs:"
									    echo "=========================="
									    cat mta_errors.txt
									    echo
									    echo "=========================="


									    ### Check for mss used for delivery
									    surrogateMSS=$(cat $INTERMAIL/log/mta.log| egrep -i delivered | grep -i $user1 | cut -d ":" -f4 | cut -d "=" -f2 | tail -1 | sort -u)
									    surrogateMSS=`echo $surrogateMSS | tr -d " "`
										if [ "$surrogateMSS" == "$ownerMSS" ]
										then
											echo
											echo "ERROR: Mails were delivered using the owner mss only."
											echo "ERROR: Please check if redirection is working or not."
										else
											echo
											echo "Mails were delivered successfully using Surrogate MSS on host => "$surrogateMSS
									    fi
									
									
									## if 4 mails are not delivered properly
									else
										echo
										echo "ERROR: "$msgs" Mails are delivered. Please check manually."
										echo
									fi
									
									
								## If mss is not shutdown, then
								else
									echo
									echo "ERROR: MSS on "$ownerMSS" could not be shutdown. Please shut it down manually."
									echo "Test surrogate Functionality manually."
									echo
								fi
								echo
								

								### Starting Owner MSS 
								echo
								echo "Please wait while owner mss is starting up again..."
								imctrl $ownerMSS start mss.1
								imservping -h $ownerMSS mss > ping_resp.txt
								resp_count=$(cat ping_resp.txt | grep -i "respond" | wc -l)
								resp_count=`echo $resp_count | tr -d " "`
								if [ "$resp_count" == "1" ]
								then
									echo "MSS on "$ownerMSS" is successfully started"
									echo
								else
									echo
									echo "ERROR: OWNER MSS on host "$ownerMSS" could not be started, please start manually."
									echo
								fi
								echo
								echo "XXXXXX----SURROGATE TESTING COMPLETES----XXXXXX"
end_time_tc surrogate_testing_tc
}

function large_msg_delivery() {

                           

                           start_time_tc large_msg_delivery_tc

                           

						   ## Checking Large message delivery



									### Testing delivery of 100Kb msg  



                                    echo "########################################"

                                    echo "|  VERIFING DELIVERY OF 100KB MESSAGE |"

                                    echo "########################################"

                                    echo	



                                    msg_100kb=`cat 100K`

										



							        ## 100KB Message Delivery Starts

				                    echo

				                    echo "-------100KB Message Delivery Starts--------"

				                    echo "Connecting to $MTAHost on Port $SMTPPort";

				                    echo "Please wait ... "

				                    echo



				                    exec 3<>/dev/tcp/$MTAHost/$SMTPPort



     			                    echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                    echo -en "RCPT TO:$user20\r\n" >&3

				                    echo -en "DATA\r\n" >&3

				                    echo -en "Subject: $SUBJECT\r\n\r\n" >&3

				                    echo -en "$msg_100kb\r\n" >&3

				                    echo -en ".\r\n" >&3

				                    echo -en "QUIT\r\n" >&3

				                    echo



				                    imboxstats $MBXID_Sanity20 > boxstats.txt

				                    msgs_user=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                    msgs_user=`echo $msgs_user | tr -d " "`

				                    echo "============================================"

				                    if [ "$msgs_user" == "1" ]

				                    then

					                    echo

					                    echo $msgs_user" Mails were delivered successfully."

										summary "100KB message delivery" 0

				                    else

					                    echo

					                    echo "ERROR: "$msgs_user" Mails were delivered only."

					                    echo "ERROR: Mails delivery failed. Please check this Manually."

										summary "100KB message delivery" 1

				                    fi



				                    echo

				                    echo "-------100KB Message Delivery Sessions Ends--------"

                                    echo						



                                    echo "XX-----Testing delivery of 100Kb msg is completed-----XX"

                                    echo



									start_time_tc large_msg_delivery_tc



									### Testing delivery of 200Kb msg  

                                    echo "########################################"

                                    echo "|  VERIFING DELIVERY OF 200KB MESSAGE |"

                                    echo "########################################"

                                    echo	

     

                                    ## 200KB Message Delivery Starts

				                    echo "-------200KB Message Delivery Starts--------"

				                    echo "Connecting to $MTAHost on Port $SMTPPort";

				                    echo "Please wait ... "

				                    echo



				                    exec 3<>/dev/tcp/$MTAHost/$SMTPPort



				                    echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                    echo -en "RCPT TO:$user20\r\n" >&3

				                    echo -en "DATA\r\n" >&3

				                    echo -en "Subject: $SUBJECT\r\n\r\n" >&3

				                    echo -en "$msg_100kb\r\n" >&3

							        echo -en "$msg_100kb\r\n" >&3

				                    echo -en ".\r\n" >&3

				                    echo -en "QUIT\r\n" >&3

        		                    echo



				                    imboxstats $MBXID_Sanity20 > boxstats.txt

				                    msgs_user=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                    msgs_user=`echo $msgs_user | tr -d " "`

				                    echo "============================================"

				                    if [ "$msgs_user" == "2" ]

				                    then

									    echo $msgs_user" Mails were delivered successfully."

                                        summary "200KB message delivery" 0

				                    else

					                    echo "ERROR: "$msgs_user" Mails were delivered only."

					                    echo "ERROR: Mails delivery failed. Please check this Manually."

										summary "200KB message delivery" 1

				                    fi



           				            echo

			         	            echo "-------200KB Message Delivery Sessions Ends--------"

				                    echo



									start_time_tc large_msg_delivery_tc

									### Testing delivery of 1MB msg  

                                    echo "######################################"

                                    echo "|  VERIFING DELIVERY OF 1MB MESSAGE |"

                                    echo "######################################"

                                    echo	



							        msg_1mb=`cat 1MB`

							        ## 1MB Message Delivery Starts

				                    echo "-------1MB Message Delivery Starts--------"

				                    echo "Connecting to $MTAHost on Port $SMTPPort";

				                    echo "Please wait ... "

				                    echo



				                    exec 3<>/dev/tcp/$MTAHost/$SMTPPort



				                    echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                    echo -en "RCPT TO:$user20\r\n" >&3

				                    echo -en "DATA\r\n" >&3

				                    echo -en "Subject: $SUBJECT\r\n\r\n" >&3

				                    echo -en "$msg_1mb\r\n" >&3

				                    echo -en ".\r\n" >&3

				                    echo -en "QUIT\r\n" >&3

                                    echo



				                    imboxstats $MBXID_Sanity20 > boxstats.txt

				                    msgs_user=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                    msgs_user=`echo $msgs_user | tr -d " "`

				                    echo "============================================"

				                    if [ "$msgs_user" == "3" ]

				                    then

					                    echo $msgs_user" Mails were delivered successfully."

										summary "1MB message delivery" 0

				                    else

					                    echo "ERROR: "$msgs_user" Mails were delivered only."

					                    echo "ERROR: Mails delivery failed. Please check this Manually."

										summary "1MB message delivery" 1

				                    fi



         			                echo

			         	            echo "-------1MB Message Delivery Sessions Ends--------"

				                    echo "XX-----Testing delivery of 1MB msg is completed-----XX"

                                    echo



									start_time_tc large_msg_delivery_tc

									### Testing delivery of 10MB msg  

                                    echo "#######################################"

                                    echo "|  VERIFING DELIVERY OF 10MB MESSAGE |"

                                    echo "#######################################"

                                    echo	



                                    msg_10mb=`cat 10MB`

							        ## 10MB Message Delivery Starts

				                    echo "-------10MB Message Delivery Starts--------"

				                    echo "Connecting to $MTAHost on Port $SMTPPort";

				                    echo "Please wait ... "

				                    echo



				                    exec 3<>/dev/tcp/$MTAHost/$SMTPPort



				                    echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                    echo -en "RCPT TO:$user20\r\n" >&3

				                    echo -en "DATA\r\n" >&3

				                    echo -en "Subject: $SUBJECT\r\n\r\n" >&3

				                    echo -en "$msg_10mb\r\n" >&3

				                    echo -en ".\r\n" >&3

				                    echo -en "QUIT\r\n" >&3

				                    echo



				                    imboxstats $MBXID_Sanity20 > boxstats.txt

				                    msgs_user=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                    msgs_user=`echo $msgs_user | tr -d " "`

				                    echo "============================================"

				                    if [ "$msgs_user" == "4" ]

				                    then

					                    echo $msgs_user" Mails were delivered successfully."

										summary "10MB message delivery" 0

				                    else

					                    echo "ERROR: "$msgs_user" Mails were delivered only."

					                    echo "ERROR: Mails delivery failed. Please check this Manually."

										summary "10MB message delivery" 1

				                    fi



				                    echo

				                    echo "-------10MB Message Delivery Sessions Ends--------"

				                    echo "XX-----Testing delivery of 10MB msg is completed-----XX"

                                    echo



									start_time_tc large_msg_delivery_tc

                                    ## POP OPERATIONS TO RETRIEVE MESSAGES OF VARIOUS SIZE

						            echo "==========================="

						            echo " PERFORMING POP OPERATIONS "

						            echo "==========================="

						            echo

						            echo "Connecting to $POPHost on Port $POPPort";

 					                echo "Please wait ... "

						            echo

                                    echo > temporary.txt



						            exec 3<>/dev/tcp/$POPHost/$POPPort



						            echo -en "user $user20\r\n" >&3

						            echo -en "pass $user20\r\n" >&3

						            echo -en "list\r\n" >&3

						            echo -en "retr 1\r\n" >&3

						            echo -en "retr 2\r\n" >&3

						 			echo -en "quit\r\n" >&3

						            echo "+++++++++++++++++++++++++"

				                    cat <&3 >>temporary.txt

						            echo "+++++++++++++++++++++++++"

						            echo



									### Check for errors in pop logs

						            cat $INTERMAIL/log/popserv.log| egrep -i "erro;|urgt;|fatl;" > pop_errors.txt

						            echo "Errors logged in POP Logs:"

						            echo "=========================="

						            cat pop_errors.txt

						            echo

						            echo "=========================="

									

									err=$(cat pop_errors.txt | wc -l)

									err=` echo $err | tr -d " "`

									if [ "$err" == "0" ]

									then

									     echo " Retrieval large messages through POP is working fine "

									     summary "Retrieving large messages through POP" 0

									else

									    echo " Retrieval large messages through POP is not working "

									    summary "Retrieving large messages through POP" 1 

                                    fi									

									echo "XX-----Testing retrieving of large messages through POP is completed-----XX"

                                    echo

									

									start_time_tc large_msg_delivery_tc

                                    ## IMAP OPERATIONS START FROM HERE

							     	echo "=========================="

							        echo "PERFORMING IMAP OPERATIONS"

							        echo "=========================="

							        echo

							        echo "Connecting to $IMAPHost on Port $IMAPPort";

							        echo "Please wait ... "

							        echo



                                    echo > temporary.txt



							        exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



     						        echo -en "a login $user20@openwave.com $user20\r\n" >&3

							        echo -en "a select INBOX\r\n" >&3

							        echo -en "a fetch 1:2 rfc822\r\n" >&3	

                                    echo -en "a logout\r\n" >&3

							        echo "+++++++++++++++++++++++++"

							        cat <&3 >>temporary.txt

							        echo "+++++++++++++++++++++++++"

							        echo



							        ### Check for errors in the imap logs

							        cat $INTERMAIL/log/imapserv.log| egrep -i "erro;|urgt;|fatl;" > imap_errors.txt

							        echo "Errors logged in IMAP Logs:"

							        echo "=========================="

							        cat imap_errors.txt

							        echo

							        echo "=========================="

                                   

						  			echo "XX-----Testing retrieving of large messages through IMAP is completed-----XX"

                                    echo						

                                    

									err=$(cat imap_errors.txt | wc -l)

									err=` echo $err | tr -d " "`

									if [ "$err" == "0" ]

									then

									     echo " Retrieval large messages through IMAP is working fine "

									     summary "Retrieving large messages through IMAP" 0

									else

									    echo " Retrieval large messages through IMAP is not working "

									    summary "Retrieving large messages through IMAP" 1 

                                    fi		

							

}

function imboxtest_utility() {

                                start_time_tc imboxtest_utility_tc

								echo "########################"

								echo "|  TESTING IMBOXTEST  |"

								echo "########################"

								echo



								imboxtest $POPHost $user1 $user1 > boxtest_sanity1.txt

								error_cnt=$(cat boxtest_sanity1.txt | grep -i "ERR" | wc -l)

								error_cnt=`echo $error_cnt | tr -d " "`



								if [ "$error_cnt" == "0" ]

								then

								    echo "================================================================"

									cat boxtest_sanity1.txt

									echo

									echo "imboxtest utility is working fine."

									summary "Imboxtest" 0

									echo

								else

                                    echo "ERROR: imboxtest utility is not showing proper results."

									summary "Imboxtest" 1

									echo

								fi



								echo "XX-----Testing of IMBOXTEST utility is completed-----XX"

}

function imboxget_utility() {

                                start_time_tc imboxget_utility_tc

								echo "#######################"

								echo "|  TESTING IMBOXGET  |"

								echo "#######################"

								echo


                                imboxget $user1@openwave.com > boxget_sanity.txt



                                value1=`cat boxget_sanity.txt |grep -i Host | wc -l`

                                value2=`cat boxget_sanity.txt |grep -i mailbox | wc -l`

								host=$( cat boxget_sanity.txt | cut -d " " -f1 | cut -d "=" -f2 )

								host=` echo $host | tr -d " " `



								mailboxid=$( cat boxget_sanity.txt | cut -d " " -f2 | cut -d "=" -f2 )

								mailboxid=` echo $mailboxid | tr -d " " `



                                if [ "$value1" == "1" ] && [ "$value2" == "1" ] && [ "$host" == "$Cluster" ] && [ "$mailboxid" == "$MBXID_Sanity1" ] 



                                then



                                    echo "================================================================"

									cat boxget_sanity.txt

									echo

									echo "imboxget utility is working fine."

									summary "Imboxget" 0

									echo

								else

                                    echo "ERROR: imboxget utility is not showing proper results."

									summary "Imboxget" 1

									echo

								fi



                                echo "XX-----Testing of IMBOXGET utility is completed-----XX"

 

}

function iminvctrl_utility() {

                                start_time_tc iminvctrl_utility_tc

								echo "########################"

								echo "|  TESTING IMINVCTRL  |"

								echo "########################"

								echo


								iminvctrl recalc -u $user1@openwave.com > invctrl_sanity.txt



								value1=`cat invctrl_sanity.txt | grep -i failed | wc -l`

								mailboxid=`cat invctrl_sanity.txt | cut -d ":" -f1 | cut -d " " -f2`

								cluster_value=`cat invctrl_sanity.txt | cut -d ":" -f1 | cut -d " " -f1`



								if [ "$value1" == "0" ] && [ "$cluster_value" == "$Cluster" ] && [ "$mailboxid" == "$MBXID_Sanity1" ] 

								then

								    echo

								    echo "================================================================"

									cat invctrl_sanity.txt

									echo

									echo "iminvctrl utility is working fine."

									summary "Iminvctrl" 0

									echo

								else

								    echo "ERROR: iminvctrl utility is not showing proper results."

									summary "Iminvctrl" 1 "MX-230"

									echo

								fi



								echo "XX-----Testing of IMINVCTRL utility is completed-----XX"

								echo

}

function imboxdirdelete_utility() {

                                start_time_tc imboxdirdelete_utility_tc

								echo "##########################"

                                echo "|  TESTING IMBOXDIRDELETE  |"

                                echo "##########################"

                                echo

                                

								imboxdirdelete $Cluster $MBXID_Sanity20



                                imboxstats $MBXID_Sanity20 > chk_mailbox.txt



                                mbx_del=$(cat chk_mailbox.txt | grep -i failed | wc -l)

                                mbx_del=`echo $mbx_del | tr -d " "`



                                if [ "$mbx_del" == "1" ]

                                then

	                               echo 

                                   echo "Mailbox deleted successfully."

	                            else

                                   echo " ERROR : Mailbox is not deleted..."

	                            fi



                                userscnt=$(imdbcontrol la | grep -i @ | grep -i $user20 | wc -l)

                                userscnt=`echo $userscnt | tr -d " "`

                                if [ "$userscnt" == "1" ]

                                then

	                                echo

	                                echo " ERROR : Account not deleted "

									summary "Imboxdirdelete" 1

                                else

	                                echo

									summary "Imboxdirdelete" 0

	                                echo "Account also deleted"

						        fi		



                               echo "XX-----Testing of IMBOXDIRDELETE utility is completed-----XX"

							   echo

}

function immsscmpms_utility() {

                               start_time_tc immsscmpms_utility_tc

							   echo "##########################"

                               echo "|  TESTING IMMSSCMPMS |"

                               echo "##########################"

                               echo



                               immsscmpms -h1 $Cluster -i1 $MBXID_Sanity1 -h2 $Cluster -i2 $MBXID_Sanity2 &>cmpms_output.txt



						       result=$(cat cmpms_output.txt | wc -l)

							   result=`echo $result | tr -d " "`



							   if [ "$result" == "0" ]

                               then

                                  echo " ERROR : No Result for immsscmpms . Please check manually ... "

                               else

                                  no_err=$(cat cmpms_output.txt | grep -i failed | wc -l)

	                              no_err=`echo $no_err | tr -d " "`

	                              if [ $no_err -gt 0 ]

	                              then 

	                                  echo

		                              echo " ERROR : immsscmpms is not working correctly "

									  summary "Immsscmpms" 1

	                              else

                                      echo

		                              echo " immsscmpms is working correctly ...."

									  summary "Immsscmpms" 0

                                  fi		

                                fi   



							   echo						

                               echo "XX-----Testing of IMMSSCMPMS utility is completed-----XX"

							   echo

}

function fep_hash_range_utility() {

                               start_time_tc fep_hash_range_utility_tc

       						   echo "##########################################################"

                               echo "|  VERIFICATION OF FEP HASH RANGE MODIFICATION SCENARIO |"

                               echo "##########################################################"

                               echo							

                             
                              deleteMessages $user4 $user4 $MBXID_Sanity4
							  imboxstats $MBXID_Sanity4 > boxstats_Sanity155.txt

				              msgs=$(grep "Total Messages Stored"  boxstats_Sanity155.txt | cut -d " " -f6)

				              msgs=`echo $msgs | tr -d " "`
                             
                             
				              imconfcontrol -install -key /$Site-$cluster/common/clusterHashMap="$Cluster=$FirstMSS:0-499,$SecondMSS:500-999" >> trash.txt

							  imconfcontrol -install -key /$Site/common/clusterHashMap="$Cluster=$FirstMSS:0-499,$SecondMSS:500-999" >> trash.txt



							   updtd_key_value=$(imconfget clusterHashMap )	

							   updtd_key_value=`echo $updtd_key_value | tr -d " "`



							   if [ "$updtd_key_value" == "$Cluster=$FirstMSS:0-499,$SecondMSS:500-999" ]

							   then

							       echo " FEP Hashing Range is : $Cluster=$FirstMSS:0-499,$SecondMSS:500-999"

							   else

							       echo " ERROR : FEP Hashing Range is not set correctly "

								   summary "FEP hash range modification test " 1

                                   return 0

                               fi



  						       ## Sending message to user4

							   echo "-------Sending Message Starts--------"

							   echo "Connecting to $MTAHost on Port $SMTPPort";

							   echo "Please wait ... "

							   echo >mail.txt



							   exec 3<>/dev/tcp/$MTAHost/$SMTPPort



							   echo -en "MAIL FROM:$MAILFROM\r\n" >&3

							   echo -en "RCPT TO:$user4\r\n" >&3

                               echo -en "DATA\r\n" >&3

							   echo -en "Subject: $SUBJECT\r\n\r\n" >&3

							   echo -en "$DATA\r\n" >&3

							   echo -en ".\r\n" >&3

							   echo -en "QUIT\r\n" >&3

	   						   cat <&3 >> mail.txt

							   echo

							   echo "-------Message Sent--------"

							   echo



							   msgid=$(grep "Message received:" mail.txt| cut -d ":" -f2 | sed -e 's/^\s*//' -e 's/\s*$//' | tail -1 | cut -d "[" -f1) 

							   msgid=` echo $msgid | tr -d " "`

							   imboxstats $MBXID_Sanity4 > boxstats_Sanity1.txt

				               msgs1=$(grep "Total Messages Stored"  boxstats_Sanity1.txt | cut -d " " -f6)

				               msgs1=`echo $msgs1 | tr -d " "`
                          
				               if [ "$msgs1" != "1" ]

				               then

							       echo " ERROR : mail not delivered "

								   summary "FEP hash range modification test " 1

								   return 0

							   else	   

	   					           ownerMSS=$(cat $INTERMAIL/log/mta.log| egrep -i delivered | grep -i $user4 | grep -i $msgid | cut -d ":" -f4 | cut -d "=" -f2 | sort -u)

							       ownerMSS=`echo $ownerMSS | tr -d " "`

							       echo "Owner MSS of the mailbox used is: "$ownerMSS

							       echo

						   

							       echo " Changing FEP Hashing Range ......"

							       echo

						   

							       imconfcontrol -install -key /$Site-$Cluster/common/clusterHashMap="$Cluster=$FirstMSS:500-999,$SecondMSS:0-499" > temp.txt

							       imconfcontrol -install -key /$Site/common/clusterHashMap="$Cluster=$FirstMSS:500-999,$SecondMSS:0-499"  > temp.txt

							       updtd_key_value=$(imconfget clusterHashMap )	

							       updtd_key_value=`echo $updtd_key_value | tr -d " "`



							       if [ "$updtd_key_value" == "$Cluster=$FirstMSS:500-999,$SecondMSS:0-499" ]

							       then

							           echo " FEP Hashing Range is : $Cluster=$FirstMSS:500-999,$SecondMSS:0-499 "

							       else

							           echo " ERROR : FEP Hashing Range is not set correctly "

									   summary "FEP hash range modification test " 1

									   return 0

                                   fi



     							   ## Sending message to user4

							       echo "-------Sending Message Starts--------"

							       echo "Connecting to $MTAHost on Port $SMTPPort";

					               echo "Please wait ... "

							       echo 

                                   echo >mail.txt

							       exec 3<>/dev/tcp/$MTAHost/$SMTPPort



							       echo -en "MAIL FROM:$user3\r\n" >&3

							       echo -en "RCPT TO:$user4\r\n" >&3

							       echo -en "DATA\r\n" >&3

							       echo -en "Subject: $SUBJECT\r\n\r\n" >&3

							       echo -en "$DATA\r\n" >&3

							       echo -en ".\r\n" >&3

							       echo -en "QUIT\r\n" >&3

								   cat <&3 >> mail.txt

							       echo

							       echo "-------Message Sent--------"

							       echo

                                   msgid=$(grep "Message received:" mail.txt| cut -d ":" -f2 | sed -e 's/^\s*//' -e 's/\s*$//' | tail -1 | cut -d "[" -f1) 

							       msgid=` echo $msgid | tr -d " "`

								   imboxstats $MBXID_Sanity4 > boxstats_Sanity1.txt

				                   msgs2=$(grep "Total Messages Stored"  boxstats_Sanity1.txt | cut -d " " -f6)

				                   msgs2=`echo $msgs2 | tr -d " "`
                                       
				                   if [ "$msgs2" != "2" ]

				                   then

							           echo " ERROR : mail not delivered "

									   summary "FEP hash range modification test " 1

								       return 0

								   else	   

						               ownerMSS1=$(cat $INTERMAIL/log/mta.log| egrep -i delivered | grep -i $user4 |grep -i $msgid | cut -d ":" -f4 | cut -d "=" -f2 | sort -u )

							           ownerMSS1=`echo $ownerMSS1 | tr -d " "`

						               echo "Owner MSS of the mailbox used is: "$ownerMSS1

 							           echo



							           if [ "$ownerMSS" == "$ownerMSS1" ]

 							           then

 							               echo  " ERROR : FEP Hashing range modification is not working fine "

										   summary "FEP hash range modification test " 1

								           echo

							           else

                                           echo  " FEP Hashing range modification is working fine "

										   summary "FEP hash range modification test " 0

								           echo

                                       fi

                             



						               

                                   fi

							   fi	   

							           imconfcontrol -install -key /$Site-$Cluster/common/clusterHashMap="$Cluster=$FirstMSS:0-499,$SecondMSS:500-999" > temp.txt

							           imconfcontrol -install -key /$Site/common/clusterHashMap="$Cluster=$FirstMSS:0-499,$SecondMSS:500-999" > temp.txt



							           updtd_key_value=$(imconfget clusterHashMap )	

							           updtd_key_value=`echo $updtd_key_value | tr -d " "`



							           if [ "$updtd_key_value" == "$Cluster=$FirstMSS:0-499,$SecondMSS:500-999" ]

							           then

							               echo " FEP Hashing Range is set back to : $Cluster=$FirstMSS:0-499,$SecondMSS:500-999"

							           else

							               echo " ERROR : FEP Hashing Range is not set back correctly "

                                       fi   						

                              echo "XX-----Testing of FEP hash range modification utility is completed-----XX"

                              echo

							 

}

function ldap_add_test() {

	                          start_time_tc ldap_add_test_tc

 					          echo "##########################"

                              echo "|  VERIFICATION LDAPADD  |"

                              echo "##########################"

                              echo	



		                      DIRHost=$(cat config/config.db | grep -i dirserv_run  | grep -i on | cut -d "/" -f2)						 

					          DIRPort=$(cat config/config.db | grep -i ldapPort | grep -v cache  |cut -d "[" -f2 | cut -d "]" -f1)	



							  echo " Directory Host = "$DIRHost

							  echo " Directory Port = "$DIRPort



						      user=$user_prefix"34"



							  mailboxid="1"$(date +%S%k%M%S)

							  mailboxid=`echo $mailboxid | tr -d " "`



    						  echo "dn: mail=$user@openwave.com,dc=openwave,dc=com" > add.ldif

							  echo "objectclass: top" >> add.ldif

							  echo "objectclass: person" >> add.ldif

							  echo "objectclass: mailuser" >> add.ldif

							  echo "objectclass: mailuserprefs" >> add.ldif

							  echo "mailpassword: $user" >> add.ldif

							  echo "mailpasswordtype: C" >> add.ldif

							  echo "mailboxstatus: A" >> add.ldif

							  echo "maillogin: $user" >> add.ldif

							  echo "mail: $user@openwave.com" >> add.ldif

							  echo "mailmessagestore: $DIRHost" >> add.ldif

                              echo "mailautoreplyhost: $DIRHost" >> add.ldif

                              echo "mailboxid: $mailboxid" >> add.ldif

							  echo "cn: $user@openwave.com" >> add.ldif

 							  echo "sn: $user@openwave.com" >> add.ldif

							  echo "adminpolicydn: cn=default,cn=admin root" >> add.ldif

				  

							  echo " Adding account $user@openwave.com ..... "



							  ldapadd -D cn=root -w secret -p $DIRPort -f add.ldif > add_result.txt



							  code=$(cat add_result.txt | grep -i " return code" | cut -d " " -f7 )

							  code=`echo $code | tr -d " "`

							  if [ "$code" == "0" ]

							  then

							      echo						

                                  echo " Account created successfully ...."

							  else

                                  echo						

                                  echo " ERROR : Account cannot be created ...."

							  fi

		  

							 imdbcontrol la | grep  -i $user | grep @ >add_result.txt

							 added=$(cat add_result.txt|  cut -d ":" -f2 | cut -d "@" -f1 )

							 added=`echo $added | tr -d " "`

							 if [ "$added" == "$user" ]

							 then

							     echo " LDAPADD is working correctly "

								  summary "LDAPADD " 0

                                 echo

							 else

               					 echo " ERROR : LDAPADD is not working correctly "

								 summary "LDAPADD " 1

                                 echo

                             fi								 



        					 echo "XX-----Testing of ldapadd utility is completed-----XX"

							 echo

							 

}

function ldap_search_utilty() {

                              start_time_tc ldap_search_utilty_tc

							  echo "##########################"

                              echo "|  VERIFICATION LDAPSEARCH  |"

                              echo "##########################"

                              echo	

		         

							  echo " Searching accoun $user@openwave.com ..... "



							  ldapsearch -D cn=root -w secret -p $DIRPort "mail=$user*" > search_result.txt



							  found=$(cat search_result.txt | grep -i "mail" | cut -d "," -f1 | cut -d "=" -f2 |head -1|cut -d "@" -f1 )

							  found=`echo $found | tr -d " "`

							  if [ "$found" == "$user" ]

							  then

							      echo						

                                  echo " Account found ...."

								  echo " Ldap search is working properly "

								  summary "LDAPSEARCH " 0

								  echo

							  else

                                  echo						

                                  echo " ERROR : Account is not found ...."

								  echo " ERROR : Ldap search is not working properly "

								  summary "LDAPSEARCH " 1

								  echo

							  fi



                             echo "XX-----Testing of ldapsearch utility is completed-----XX"

                             echo

							 

}

function ldap_modify_utility() {

                              start_time_tc ldap_modify_utility_tc

							  echo "############################"

                              echo "|  VERIFICATION LDAPMODIFY |"

                              echo "############################"

                              echo	



		                      echo "dn: mail=$user@openwave.com,dc=openwave,dc=com" > modify.ldif

                              echo "changetype: modify" >> modify.ldif

                              echo "replace: mailpassword" >> modify.ldif

                              echo "mailpassword: abcd" >> modify.ldif



     						  echo " Modifying accoun $user@openwave.com ..... "

							  ldapmodify -D cn=root -w secret -p $DIRPort -f modify.ldif >modify_result.txt 



							  result=$(cat modify_result.txt | grep -i " return code" | cut -d " " -f6 )

							  result=`echo $result | tr -d " "`

							  if [ "$result" == "0" ]

							  then

							      echo						

                                  echo " Account maillpassword modified  successfully ...."

							  else

                                  echo						

                                  echo " ERROR : Account mailpassword cannot be modified  ...."

							  fi



							 ldapsearch -D cn=root -w secret -p $DIRPort "mail=$user*" > modify_result1.txt

							 modified=$(cat modify_result1.txt| grep -i mailpassword| cut -d "=" -f2 |tail -1  )

							 modified=`echo $modified | tr -d " "`

							 if [ "$modified" == "abcd" ]

							 then

							     echo " LDAPMODIFY is working correctly "

								  summary "LDAPMODIFY " 0

                                 echo

							 else

               					 echo " ERROR : LDAPMODIFY is not working correctly "

								 summary "LDAPMODIFY " 1

                                 echo

                             fi								 



                            echo "XX-----Testing of ldapmodify utility is completed-----XX"

							echo

                            

}

function ldap_delete_utility() {

                              start_time_tc ldap_delete_utility_tc

							  echo "############################"

                              echo "|  VERIFICATION LDAPDELETE |"

                              echo "############################"

                              echo	



		                      echo "mail=$user@openwave.com,dc=openwave,dc=com"  > del.ldif

							  echo " Deleting accoun $user@openwave.com ..... "

  

							  ldapdelete -D cn=root -w secret -p $DIRPort -f del.ldif 

  

							  imdbcontrol la |grep @ |grep -i $user > delete_result.txt

							  deleted=$(cat delete_result.txt | wc -l  )

							  deleted=`echo $deleted | tr -d " "`

							  if [ "$deleted" == "0" ]

							  then

							      echo						

                                  echo " Account deleted ...."

								  echo

								  echo " Ldap delete is working properly "

								  summary "LDAPDELETE " 0

							  else

                                  echo						

                                  echo " Account is not deleted ...."

								  echo

								  echo " Ldap delete is not working "

								  summary "LDAPDELETE " 1

							  fi

	

                             echo "XX-----Testing of ldapdelete utility is completed-----XX"

							 echo

							 

}

function no_flag_msg() {

                             start_time_tc no_flag_msg_tc

							  echo "########################################################"

                              echo "|  VERIFICATION THAT NO FLAGS ARE SET IN SECOND SESSION |"

                              echo "########################################################"

                              echo	



    						  imboxstats $MBXID_Sanity4 > boxstats_Sanity155.txt

				              msgs=$(grep "Total Messages Stored"  boxstats_Sanity155.txt | cut -d " " -f6)

				              msgs=`echo $msgs | tr -d " "`

				              msgs=$(($msgs+1))
							   



							   ## Sending message to user4



							   echo "-------Sending Message Starts--------"

							   echo

							   echo "Connecting to $MTAHost on Port $SMTPPort";

							   echo "Please wait ... "

							   echo



    					       exec 3<>/dev/tcp/$MTAHost/$SMTPPort



							   echo -en "MAIL FROM:$user2\r\n" >&3

							   echo -en "RCPT TO:$user4\r\n" >&3

							   echo -en "DATA\r\n" >&3

							   echo -en "Subject: $SUBJECT\r\n\r\n" >&3

							   echo -en "$DATA\r\n" >&3

							   echo -en ".\r\n" >&3

							   echo -en "QUIT\r\n" >&3

							   echo

							   echo "-------Message Sent--------"

							   echo 

                               

							   imboxstats $MBXID_Sanity4 > boxstats_Sanity1.txt

				               msgs1=$(grep "Total Messages Stored"  boxstats_Sanity1.txt | cut -d " " -f6)

				               msgs1=`echo $msgs1 | tr -d " "`

				               if [ "$msgs1" == "$msgs" ]

				               then

				                   echo " Mail delivered "

							   else

							       echo " ERROR : Mail is not delivered "

								   summary "RECENT flag is not set in second session " 1

								   return 0

                               fi

							   

        					## CHECKING FLAGS THRUGH IMAP 

							echo

							echo "Connecting to $IMAPHost on Port $IMAPPort";

							echo "Please wait ... "

							echo

							echo > temporary.txt



     						exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



							echo -en "a login $user4@openwave.com $user4\r\n" >&3

							echo -en "a select INBOX\r\n" >&3

							echo -en "a logout\r\n" >&3



							cat <&3 >> temporary.txt

    						echo



    						recent=$(cat temporary.txt |grep -i "recent" |cut -d " " -f2)

                            recent=`echo $recent | tr -d " "`

							if [ "$recent" == "1" ]

							then

							   	echo " Flag is Recent ..."

								echo

							else

							    echo " ERROR : Flag is not Recent ..."

								echo

							fi	



     	                  ## CHECKING FLAGS THRUGH IMAP 

							echo "Connecting to $IMAPHost on Port $IMAPPort";

							echo "Please wait ... "

							echo

							echo > temporary1.txt



							exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



							echo -en "a login $user4@openwave.com $user4\r\n" >&3

							echo -en "a select INBOX\r\n" >&3

    						echo -en "a logout\r\n" >&3



							cat <&3 >> temporary1.txt

							echo



							recent=$(cat temporary.txt |grep -i "recent" |cut -d " " -f2)

                            recent=`echo $recent | tr -d " "`

							if [ "$recent" == "0" ]

							then

								echo " ERROR : Flag is Recent ..."

								echo " ERROR : Utility is not working "

								summary "RECENT flag is not set in second session " 1

								echo

							else

     							echo " Flag is not Recent ..."

								echo " Utility is working fine "

								summary "RECENT flag is not set in second session " 0

								echo

							fi



							### Check for errors in the imap logs

							cat $INTERMAIL/log/imapserv.log| egrep -i "erro;|urgt;|fatl;" > imap_errors.txt

							echo "Errors logged in IMAP Logs:"

							echo "=========================="

							cat imap_errors.txt

							echo

							echo "=========================="

							



                          echo "XX-----Testing that no flags are set for messages when checked through imap in second consecutive session utility is completed-----XX"

						  echo

}

function imboxcopy_cluster_mss() {

                                start_time_tc imboxcopy_cluster_mss_tc

								echo "#############################################"

								echo "|  TESTING IMBOXCOPY  FROM CLUSTER TO MSS  |"

								echo "#############################################"

								echo



  						        imboxstats $MBXID_Sanity2 > boxstats.txt

								msg_bxcp1=$(grep "Total Messages Stored" boxstats.txt | cut -d " " -f6)

								msg_bxcp1=`echo $msg_bxcp1 | tr -d " "`

								echo "Currently "$msg_bxcp1" messages are stored in the mailbox"

								echo



								echo "Running imboxcopy utility, please wait.."

								imboxcopy ms://$Cluster/DB/$MBXID_Sanity2 ms://$FirstMSS/DB/$MBXID_Sanity21 > boxcopy.txt



								imboxstats $MBXID_Sanity21 > boxstats1.txt

								msg_bxcp2=$(grep "Total Messages Stored" boxstats1.txt | cut -d " " -f6)

								msg_bxcp2=`echo $msg_bxcp2 | tr -d " "`

								if [ $msg_bxcp2 == $msg_bxcp1 ]

								then

									echo

									echo "Currently "$msg_bxcp2" messages are stored in the mailbox"

									echo

									echo "imboxcopy utility is working fine"

									summary "Imboxcopy from cluster to mss " 0

								else

    								echo

									echo "ERROR: Currently "$msg_bxcp2" messages are stored in the mailbox"

									echo

									echo "ERROR: imboxcopy utility is not working fine. Please check Manually."

									summary "Imboxcopy from cluster to mss " 1

								fi

								

								echo

								echo "XX-----Testing of IMBOXCOPY utility is completed-----XX"

}

function imboxcopy_mss_cluster() {

                               start_time_tc imboxcopy_mss_cluster_tc

							    echo "#############################################"

								echo "|  TESTING IMBOXCOPY  FROM MSS TO CLUSTER  |"

								echo "#############################################"

								echo

				

								imboxstats $MBXID_Sanity3 > boxstats_Sanity1.txt

				                msgs=$(grep "Total Messages Stored"  boxstats_Sanity1.txt | cut -d " " -f6)

				                msgs=`echo $msgs | tr -d " "`
                                
				                					   

								imboxstats $MBXID_Sanity21 > boxstats.txt

								msg_bxcp1=$(grep "Total Messages Stored" boxstats.txt | cut -d " " -f6)

								msg_bxcp1=`echo $msg_bxcp1 | tr -d " "`
                                msgs=$(($msgs+$msg_bxcp1))
								echo "Currently "$msg_bxcp1" messages are stored in the mailbox"

								echo



								echo "Running imboxcopy utility, please wait.."

								imboxcopy ms://$FirstMSS/DB/$MBXID_Sanity21 ms://$Cluster/DB/$MBXID_Sanity3 > boxcopy.txt



								imboxstats $MBXID_Sanity3 > boxstats1.txt

								msg_bxcp2=$(grep "Total Messages Stored" boxstats1.txt | cut -d " " -f6)

								msg_bxcp2=`echo $msg_bxcp2 | tr -d " "`

								if [ $msg_bxcp2 == $msgs ]

								then

									echo

     								echo "Currently "$msg_bxcp2" messages are stored in the mailbox"

									echo

									echo "imboxcopy utility is working fine"

									summary "Imboxcopy from mss to cluster " 0

								else

									echo

									echo "ERROR: Currently "$msg_bxcp2" messages are stored in the mailbox"

									echo

									echo "ERROR: imboxcopy utility is not working fine. Please check Manually."

									summary "Imboxcopy from mss to cluster " 1

								fi



								

								echo

								echo "XX-----Testing of IMBOXCOPY utility is completed-----XX"

								

}

function imboxcopy_mss_mss() {

                                start_time_tc  imboxcopy_mss_mss_tc

								echo "#######################################"

								echo "|  TESTING IMBOXCOPY FROM MSS TO MSS |"

								echo "######################################"

								echo



								imboxstats $MBXID_Sanity21 > boxstats.txt

								msg_bxcp1=$(grep "Total Messages Stored" boxstats.txt | cut -d " " -f6)

								msg_bxcp1=`echo $msg_bxcp1 | tr -d " "`

								echo "Currently "$msg_bxcp1" messages are stored in the mailbox"

								echo



								echo "Running imboxcopy utility, please wait.."

								imboxcopy ms://$FirstMSS/DB/$MBXID_Sanity21 ms://$SecondMSS/DB/$MBXID_Sanity22 > boxcopy.txt



								imboxstats $MBXID_Sanity22 > boxstats1.txt

								msg_bxcp2=$(grep "Total Messages Stored" boxstats1.txt | cut -d " " -f6)

								msg_bxcp2=`echo $msg_bxcp2 | tr -d " "`

								if [ $msg_bxcp2 == $msg_bxcp1 ]

								then

									echo

									echo "Currently "$msg_bxcp2" messages are stored in the mailbox"

    								echo

									echo "imboxcopy utility is working fine"

									summary "Imboxcopy from mss to mss " 0

								else

									echo

									echo "ERROR: Currently "$msg_bxcp2" messages are stored in the mailbox"

									echo

									echo "ERROR: imboxcopy utility is not working fine. Please check Manually."

									summary "Imboxcopy from mss to mss " 1

								fi

     

                                echo

								echo "XX-----Testing of IMBOXCOPY utility is completed-----XX"

                                

}

function rm_api_test() {



                                start_time_tc rm_api_test_tc



								echo "##########################"



								echo "| TESTING RM OF APITEST   |"



								echo "##########################"



								echo







							    ### apitest operations



								{



								    echo ca test1@openwave.com $Cluster 77777 test1 



									echo cb 77777  



									echo quit;    



								} | apitest $Cluster &> trace1.txt



								echo



								echo "ERRORS During apitest testing"



								echo "=================================="



								cat trace1.txt | egrep -i "erro|fatl|fail|urgt|warn"



								echo







                                userscnt=$(imdbcontrol la | grep  @ | grep -i test1 | wc -l)



                                userscnt=`echo $userscnt | tr -d " "`



                                if [ "$userscnt" == "1" ]



                                then



                                    echo "Successfully Created User"



                                else



                                    echo "ERROR : User is not created properly. Please check Manually"



									summary "rm of Apitest " 1 



									return 0



                                fi		







                                ## Checking for mailbox created ##



                                imboxstats 77777 > chk_mailbox.txt



                                mbx_del=$(cat chk_mailbox.txt | grep -i failed | wc -l)



                                mbx_del=`echo $mbx_del | tr -d " "`



                                if [ "$mbx_del" == "0" ]



                                then



	                                echo "Mailbox is created successfully."



	                            else



                                    echo "Mailbox is not created..."



									summary "rm of Apitest " 1



									return 0



	                            fi



								



								## Sending message to test



							    echo "-------Sending Message Starts--------"



							    echo



							    echo "Connecting to $MTAHost on Port $SMTPPort";



							    echo "Please wait ... "



							    echo



                                msg="this is to test mail " 



                                



								exec 3<>/dev/tcp/$MTAHost/$SMTPPort







							    echo -en "MAIL FROM:$MAILFROM\r\n" >&3



							    echo -en "RCPT TO:test1\r\n" >&3



							    echo -en "DATA\r\n" >&3



							    echo -en "$msg\r\n" >&3



							    echo -en ".\r\n" >&3



							    echo -en "QUIT\r\n" >&3



							    echo



							    echo "-------Message Sent--------"



							    echo







								### apitest operations



								{



                                    echo rf 77777 INBOX  



					 	            echo quit;   



								} | apitest $Cluster &> trace1.txt







								msgref=$(cat trace1.txt | grep -i "not_private" |cut -d ":" -f2| cut -d " " -f2 )



								msgref=` echo $msgref | tr -d " "`



								echo " msg ref is "$msgref



								echo



								echo "ERRORS During apitest testing"



								echo "=================================="



								cat trace1.txt | egrep -i "erro|fatl|fail|urgt|warn"



								echo







								### apitest operations



								{



								  echo rm 77777 INBOX $msgref 



								  echo quit;



								 } | apitest $Cluster &> trace2.txt



								no_id=$(cat trace2.txt | grep -i msgid | cut -d "\"" -f2)



                                no_id=` echo $no_id | tr -d " "`



                                if [ "$no_id" == "<NULL>" ]



                                then



                                    echo



                                    echo "  Message id is null . Test fail"



									summary "rm of Apitest " 1 "ITS - 1326512"



                                else



                                    echo



                                    echo " Message id is not null . Test PASS "



									summary "rm of Apitest " 0



                                fi







								echo



								echo "ERRORS During apitest testing"



								echo "=================================="



								cat trace2.txt | egrep -i "erro|fatl|fail|urgt|warn"



								echo







			                    imboxdelete $Cluster 77777



								imdbcontrol da test1 openwave.com



								imboxstats 77777 > chk_mailbox.txt



                                mbx_del=$(cat chk_mailbox.txt | grep -i failed | wc -l)



                                mbx_del=`echo $mbx_del | tr -d " "`







                                if [ "$mbx_del" == "1" ]



                                then



	                               echo 



                                   echo "Mailbox deleted successfully."



	                            else



                                   echo " ERROR : Mailbox is not deleted..."



	                            fi







                                userscnt=$(imdbcontrol la | grep -i @ | grep -i test1 | wc -l)



                                userscnt=`echo $userscnt | tr -d " "`



                                if [ "$userscnt" == "1" ]



                                then



	                                echo



	                                echo " ERROR : Account not deleted "



                                else



	                                echo



	                                echo "Account also deleted"



                               fi



							   echo						



                               echo "XX-----Testing rm of apitest is completed -----XX"



}

function api_test_2() {

                               start_time_tc api_test_2_tc

                                ###APITEST START FROM HERE

								### COPYING APITEST AND APITEST UTILITES FOR TESTING

								echo

								echo "######################"

								echo "|  APITEST TESTING  |"

								echo "######################"

								echo



								echo "Please wait while we copy apitest utilities to testing location..."

								echo

								#capi_path=$(strings $INTERMAIL/lib/mta | grep -i build_root | cut -d "=" -f2)

								#capi_path=`echo $capi_path | tr -d " "`

								#cp "$capi_path"/mercury/c-api/apitest .

								echo

								echo "API utilities copied. Starting operations..."

								echo

                            

								### apitest operations

								{

								    echo ca testk@openwave.com $Cluster 7777 testk     ## create account

									echo ca fwd@openwave.com $Cluster 5555  fwd

									echo cb 7777                                     ## create mailbox 

                                    echo cb 5555									
                                } | apitest $Cluster &> trace.txt

								

								echo

								echo "ERRORS During apitest testing"

								echo "=================================="

								cat trace.txt | egrep -i "erro|fatl|fail|urgt|warn"

								err=$(cat trace.txt | egrep -i "erro|fatl|fail|urgt|warn" |wc -l)

								err=` echo $err | tr -d " "`

								echo

								    for ((i=1;i<=2;i++))

                                    do
                                      	if [ "$err" == "0" ]
            							then
                                          summary "APITEST Test case $i" 0
                                        else
     								      summary "APITEST Test case $i" 1 "MX-748"
                                        fi					
                                    done
									
								{
								
									echo cm 7777 INBOX                               ## create message

									echo " This is the first message "

									echo +++

									echo cm 7777 INBOX                              

									echo " This is the second message "

									echo +++

									echo rf 7777 INBOX                               ## read folder

                                    echo cf 7777 NEW                                 ## create folder 

                                    echo cm 7777 NEW

  									echo " This is the second message "

									echo +++

									echo rf 7777 NEW

									echo nf 7777 NEW LATEST                          ## Rename Folder

									echo rf 7777 LATEST 

									echo mvf 7777 INBOX LATEST                       ## Move multiple messages

									echo rf 7777 LATEST

									echo rf 7777 INBOX
                                } | apitest $Cluster &> trace.txt

								

								echo

								echo "ERRORS During apitest testing"

								echo "=================================="

								cat trace.txt | egrep -i "erro|fatl|fail|urgt|warn"

								err=$(cat trace.txt | egrep -i "erro|fatl|fail|urgt|warn" |wc -l)

								err=` echo $err | tr -d " "`

								echo

								    for ((i=3;i<=7;i++))

                                    do
                                      	if [ "$err" == "0" ]
            							then
                                          summary "APITEST Test case $i" 0
                                        else
     								      summary "APITEST Test case $i" 1 "MX-748"
                                        fi					
                                    done
									
								{
									
                      				echo cl testk@openwave.com  testacc@openwave.com  ## create alias

                                    echo rl testk@openwave.com                        ## read alias	

									echo cw testk@openwave.com fwd@openwave.com       ## create forward

                                    echo fe testk@openwave.com                        ## Enable forward

								    echo quit;

								} | apitest $Cluster &> trace.txt

								

								echo

								echo "ERRORS During apitest testing"

								echo "=================================="

								cat trace.txt | egrep -i "erro|fatl|fail|urgt|warn"

								err=$(cat trace.txt | egrep -i "erro|fatl|fail|urgt|warn" |wc -l)

								err=` echo $err | tr -d " "`

								echo

								    for ((i=8;i<=11;i++))

                                    do
                                      	if [ "$err" == "0" ]
            							then
                                          summary "APITEST Test case $i" 0
                                        else
     								      summary "APITEST Test case $i" 1 "MX-748"
                                        fi					
                                    done

							    			



								## Sending message to test

							    echo "-------Sending Message Starts--------"

							    echo

							    echo "Connecting to $MTAHost on Port $SMTPPort";

							    echo "Please wait ... "

							    echo

                                msg="this is to check mail forwarding " 

                                exec 3<>/dev/tcp/$MTAHost/$SMTPPort



							    echo -en "MAIL FROM:$MAILFROM\r\n" >&3

							    echo -en "RCPT TO:testk\r\n" >&3

							    echo -en "DATA\r\n" >&3

							    echo -en "$msg\r\n" >&3

							    echo -en ".\r\n" >&3

							    echo -en "QUIT\r\n" >&3

							    echo "-------Message Sent--------"

							    echo

							   

								### apitest operations

								{

								    echo rf 7777 INBOX                               ## To check mail forwarded or not

									echo rf 5555 INBOX                               

									echo uca default mailautoreplymode               ## Unset COS Attribute

									echo quit;

								} | apitest $Cluster &> trace.txt

								

								echo

								echo "ERRORS During apitest testing"

								echo "=================================="

								cat trace.txt | egrep -i "erro|fatl|fail|urgt|warn"

								err=$(cat trace.txt | egrep -i "erro|fatl|fail|urgt|warn" |wc -l)

								err=` echo $err | tr -d " "`

								echo

								

								    for ((i=12;i<=13;i++))

                                    do
                                      if [ "$err" == "0" ]
								      then
                                           summary "APITEST Test case $i" 0
                                      else
             							    summary "APITEST Test case $i" 1 "ITS-"
                                      fi		

                                    done

							    
								imdbcontrol sc default | grep -i mailautoreplymode > cos_unset.txt

								ans=$(cat cos_unset.txt | grep -i '^mail'| wc -l)

								ans=`echo $ans | tr -d " "`

								

								if [ "$ans" == "0" ]

								then

								    echo

									echo " The cos attribute is unset successfully "

									echo

							    else

								    echo

									echo " The cos attribute is unset successfully "

									echo

                                fi

								

								### apitest operations

								{

									echo sca default mailautoreplymode N             ## Set COS Attribute

                                    echo us testk@openwave.com newtest@openwave.com   ## update account address	

                                    echo db 5555                                     ## Delete Mailbox

                                    echo db 7777 									

									echo quit;

								} | apitest $Cluster &> trace.txt

								

								echo

								echo "ERRORS During apitest testing"

								echo "=================================="

								cat trace.txt | egrep -i "erro|fatl|fail|urgt|warn"

								err=$(cat trace.txt | egrep -i "erro|fatl|fail|urgt|warn" |wc -l)

								err=` echo $err | tr -d " "`

								echo

								
								    for ((i=14;i<=16;i++))

                                    do

                                      if [ "$err" == "0" ]
								      then
                                           summary "APITEST Test case $i" 0
                                      else
             							    summary "APITEST Test case $i" 1 "MX-748"
                                      fi		

                                    done

							    

								imdbcontrol da newtest openwave.com

								imdbcontrol da fwd openwave.com

								echo

								echo "APITEST Testing is completed."

								echo

}

function ITS_1329310() {

                                start_time_tc ITS_1329310_tc

								##ITS - Issue 1329310 

								##IMAP Server giving improper response for some messages for FETCH (ENVELOPE) command



								echo "########################################"

                                echo "| VERIFY RESPONSE OF FETCH (ENVELOPE) |"

                                echo "########################################"

                                echo	

                                

								echo "From: $MAILFROM@openwave.com "  > message.txt 

							    echo "To: All" >> message.txt

								echo "Subject: Test mail" >> message.txt

							    echo >> message.txt

							    echo "This is a test mail ..."  >> message.txt

							   

							   

								

								msg=`cat message.txt`

                                ## Message Delivery Starts

				                echo

				                echo "-------Message Delivery Starts--------"

				                echo "Connecting to $MTAHost on Port $SMTPPort";

				                echo "Please wait ... "

				                echo

				                exec 3<>/dev/tcp/$MTAHost/$SMTPPort



				                echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                echo -en "RCPT TO:$user4\r\n" >&3

				                echo -en "DATA\r\n" >&3

				                echo -en "$msg\r\n" >&3

				                echo -en ".\r\n" >&3

								echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                echo -en "RCPT TO:$user4\r\n" >&3

				                echo -en "DATA\r\n" >&3

				                echo -en "$msg\r\n" >&3

				                echo -en ".\r\n" >&3

								echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                echo -en "RCPT TO:$user4\r\n" >&3

				                echo -en "DATA\r\n" >&3

				                echo -en "$msg\r\n" >&3

				                echo -en ".\r\n" >&3

				                echo -en "QUIT\r\n" >&3

				               

				                imboxstats $MBXID_Sanity4 > boxstats.txt

				                msgs_user=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                msgs_user=`echo $msgs_user | tr -d " "`

																

								if [ $msgs_user -gt 0 ]

								then

								     ## CASE 1 : SAME MAILBOX WITH INVALID TO: HEADER ###

									 echo "##############################################"

									 echo " CASE 1: SAME MAILBOX WITH INVALID TO: HEADER "

									 echo "##############################################"

								     ## IMAP OPERATIONS START FROM HERE

									 echo "================================="

							         echo "PERFORMING IMAP FETCH (ENVELOPE)"

							         echo "================================="

							         echo

							         echo "Connecting to $IMAPHost on Port $IMAPPort";

							         echo "Please wait ... "

							         echo

                                     echo > temporary.txt

							         exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



							         echo -en "a login $user4@openwave.com $user4\r\n" >&3

							         echo -en "a select INBOX\r\n" >&3

									 echo -en "a fetch 1:* envelope\r\n" >&3

									 echo -en "a logout\r\n" >&3

									 cat <&3 >>temporary.txt

									 

									 

									 invalid_field=$(cat temporary.txt | grep -i "()" | wc -l)

									 invalid_field=`echo $invalid_field | tr -d " " `

									 

									 if [ $invalid_field -gt 0 ]

									 then

									     echo

									     echo " Invalid header field found in fetch envelope "

										 echo " Scenario 1 is not working fine "	

                                         summary " Fetch(Envelope) with invalid TO: header " 1 "ITS-1329310 "										 

									 else

									     echo

									     echo " Invalid header field not found in fetch envelope "

										 echo " Scenario 1 is working fine "

                                         summary "Fetch(Envelope) with invalid TO: header " 0										 

									 fi

									 

									 ## CASE 2 : FOR CUSTOM FOLDER ###

									 echo "#############################"

									 echo " CASE 2 : FOR CUSTOM FOLDER "

									 echo "#############################"

								     ## IMAP OPERATIONS START FROM HERE

									 echo "================================="

							         echo "PERFORMING IMAP FETCH (ENVELOPE)"

							         echo "================================="

							         echo

							         echo "Connecting to $IMAPHost on Port $IMAPPort";

							         echo "Please wait ... "

							         echo

                                     echo > temporary.txt

							         exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



							         echo -en "a login $user4@openwave.com $user4\r\n" >&3

									 echo -en "a create NEWFOLDER\r\n" >&3

							         echo -en "a select INBOX\r\n" >&3

									 echo -en "a copy 1:* NEWFOLDER\r\n" >&3

									 echo -en "a select NEWFOLDER\r\n" >&3

									 echo -en "a fetch 1:* envelope\r\n" >&3

									 echo -en "a logout\r\n" >&3

									 cat <&3 >>temporary.txt

									 

									 

									 invalid_field=$(cat temporary.txt | grep -i "()" | wc -l)

									 invalid_field=`echo $invalid_field | tr -d " " `

									 

									 if [ $invalid_field -gt 0 ]

									 then

									     echo

									     echo " Invalid header field found in fetch envelope "

										 echo " Scenario 2 is not working fine "

                                         summary "Fetch(Envelope) for custom folder " 1 "ITS-1329310 "										 

									 else

									     echo

									     echo " Invalid header field not found in fetch envelope "

										 echo " Scenario 2 is working fine "	

										 summary "Fetch(Envelope) for custom folder " 0	

									 fi

									 

									 ## CASE 3 : FOR VARIOUS INVALID/VALID COMBINATION OF TO HEADER ###

									 echo "###############################################################"

									 echo " CASE 3 :  FOR VARIOUS INVALID/VALID COMBINATION OF TO HEADER "

									 echo "###############################################################"

									 

									 echo "From: $MAILFROM@openwave.com "  > message.txt 

							         echo "To: $user4@openwave.com; anyuser " >> message.txt

								     echo "Subject: Test mail" >> message.txt

							         echo >> message.txt

							         echo "This is a test mail ..."  >> message.txt

									 

									 echo "From: $MAILFROM@openwave.com "  > message1.txt 

							         echo "To: all; anyuser " >> message1.txt

								     echo "Subject: Test mail" >> message1.txt

							         echo >> message1.txt

							         echo "This is a test mail ..."  >> message1.txt

									 

									 msg=`cat message.txt`

									 msg1=`cat message1.txt`

                                     ## Message Delivery Starts

				                     echo

				                     echo "-------Message Delivery Starts--------"

				                     echo "Connecting to $MTAHost on Port $SMTPPort";

				                     echo "Please wait ... "

				                     echo

				                     exec 3<>/dev/tcp/$MTAHost/$SMTPPort



				                     echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                     echo -en "RCPT TO:$user4\r\n" >&3

				                     echo -en "DATA\r\n" >&3

				                     echo -en "$msg\r\n" >&3

				                     echo -en ".\r\n" >&3

								     echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                     echo -en "RCPT TO:$user4\r\n" >&3

				                     echo -en "DATA\r\n" >&3

				                     echo -en "$msg1\r\n" >&3

				                     echo -en ".\r\n" >&3

								     echo -en "QUIT\r\n" >&3

				                     

									 

									 imboxstats $MBXID_Sanity4 > boxstats.txt

				                     msgs_user1=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                     msgs_user1=`echo $msgs_user1 | tr -d " "`

									 if [ $msgs_user1 -gt 3 ]

									 then

									      ## IMAP OPERATIONS START FROM HERE

									      echo "================================="

							              echo "PERFORMING IMAP FETCH (ENVELOPE)"

							              echo "================================="

							              echo

							              echo "Connecting to $IMAPHost on Port $IMAPPort";

							              echo "Please wait ... "

							              echo

                                          echo > temporary.txt

							              exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



							              echo -en "a login $user4@openwave.com $user4\r\n" >&3

									      echo -en "a select INBOX\r\n" >&3

									      echo -en "a fetch 4:* envelope\r\n" >&3

									      echo -en "a logout\r\n" >&3

									      cat <&3 >>temporary.txt

									     

									 

									      invalid_field=$(cat temporary.txt | grep -i "()" | wc -l)

									      invalid_field=`echo $invalid_field | tr -d " " `

									 

									      if [ $invalid_field -gt 0 ]

									      then

									          echo

									          echo " Invalid header field found in fetch envelope "

										      echo " Scenario 3 is not working fine "	

                                              summary "Fetch(Envelope) for invalid/valid TO header " 1 "ITS-1329310 "												  

									     else

									          echo

									          echo " Invalid header field not found in fetch envelope "

										      echo " Scenario 3 is working fine "

                                              summary "Fetch(Envelope) for invalid/valid TO header " 0												  

									     fi

									 else

                                          echo " Mails not delivered "

										  summary "Fetch(Envelope) for invalid/valid TO header " 1

                                     fi

									 

									 ## CASE 4 : FOR DOC/PDF ATTACHMENT ###

									 echo "###################################"

									 echo " CASE 4 :  FOR DOC/PDF ATTACHMENT "

									 echo "###################################"

									 

									 echo "From: $MAILFROM@openwave.com "  > message.txt 

							         echo "To: $user4@openwave.com; anyuser " >> message.txt

								     echo "Subject: Test mail" >> message.txt

							         echo >> message.txt

									 msg=`cat 32k-mime_doc`

							         echo "$msg"  >> message.txt

									 

									 echo "From: $MAILFROM@openwave.com "  > message1.txt 

							         echo "To: all; anyuser " >> message1.txt

								     echo "Subject: Test mail" >> message1.txt

							         echo >> message1.txt

									 msg=`cat 64k-mime_pdf`

							         echo "$msg"  >> message1.txt

									 

									 msg=`cat message.txt`

									 msg1=`cat message1.txt`

                                     ## Message Delivery Starts

				                     echo

				                     echo "-------Message Delivery Starts--------"

				                     echo "Connecting to $MTAHost on Port $SMTPPort";

				                     echo "Please wait ... "

				                     echo

				                     exec 3<>/dev/tcp/$MTAHost/$SMTPPort



				                     echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                     echo -en "RCPT TO:$user4@openwave.com\r\n" >&3

				                     echo -en "DATA\r\n" >&3

				                     echo -en "$msg\r\n" >&3

				                     echo -en ".\r\n" >&3

								     echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                     echo -en "RCPT TO:$user4\r\n" >&3

				                     echo -en "DATA\r\n" >&3

				                     echo -en "$msg1\r\n" >&3

				                     echo -en ".\r\n" >&3

								     echo -en "QUIT\r\n" >&3

				                    

									 

									 imboxstats $MBXID_Sanity4 > boxstats.txt

				                     msgs_user1=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                     msgs_user1=`echo $msgs_user1 | tr -d " "`

									 if [ $msgs_user1 -gt 5 ]

									 then

									      ## IMAP OPERATIONS START FROM HERE

									      echo "================================="

							              echo "PERFORMING IMAP FETCH (ENVELOPE)"

							              echo "================================="

							              echo

							              echo "Connecting to $IMAPHost on Port $IMAPPort";

							              echo "Please wait ... "

							              echo

                                          echo > temporary.txt

							              exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



							              echo -en "a login $user4@openwave.com $user4\r\n" >&3

									      echo -en "a select INBOX\r\n" >&3

									      echo -en "a fetch 6:* envelope\r\n" >&3

									      echo -en "a logout\r\n" >&3

									      cat <&3 >>temporary.txt

									      

									 

									      invalid_field=$(cat temporary.txt | grep -i "()" | wc -l)

									      invalid_field=`echo $invalid_field | tr -d " " `

									 

									      if [ $invalid_field -gt 0 ]

									      then

									          echo

									          echo " Invalid header field found in fetch envelope "

										      echo " Scenario 4 is not working fine "

                                              summary "Fetch(Envelope) for doc/pdf attachment " 1 "ITS-1329310 "												  

									     else

									          echo

									          echo " Invalid header field not found in fetch envelope "

										      echo " Scenario 4 is working fine "

                                              summary "Fetch(Envelope) for doc/pdf attachment " 0												  

									     fi

										 

								     else

                                          echo " Mails not delivered "

										  summary "Fetch(Envelope) for doc/pdf attachment " 1

                                     fi

									 

									 ## CASE 5 : FOR INVALID CC HEADER ###

									 echo "###################################"

									 echo " CASE 5 :  FOR INVALID CC HEADER "

									 echo "###################################"

									 

									 echo "From: $MAILFROM@openwave.com "  > message.txt 

							         echo "To: $user4@openwave.com; anyuser " >> message.txt

									 echo " CC: unknown" >> message.txt

								     echo "Subject: Test mail" >> message.txt

							         echo >> message.txt

									 echo "This is a test mail ..."  >> message.txt

									 

									 msg=`cat message.txt`

  									 ## Message Delivery Starts

				                     echo

				                     echo "-------Message Delivery Starts--------"

				                     echo "Connecting to $MTAHost on Port $SMTPPort";

				                     echo "Please wait ... "

				                     echo

				                     exec 3<>/dev/tcp/$MTAHost/$SMTPPort



				                     echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                     echo -en "RCPT TO:$user4\r\n" >&3

				                     echo -en "DATA\r\n" >&3

				                     echo -en "$msg\r\n" >&3

				                     echo -en ".\r\n" >&3

      							     echo -en "QUIT\r\n" >&3

				                     

				                     						 

									 imboxstats $MBXID_Sanity4 > boxstats.txt

				                     msgs_user1=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                     msgs_user1=`echo $msgs_user1 | tr -d " "`

									 if [ $msgs_user1 -gt 7 ]

									 then

									      ## IMAP OPERATIONS START FROM HERE

									      echo "================================="

							              echo "PERFORMING IMAP FETCH (ENVELOPE)"

							              echo "================================="

							              echo

							              echo "Connecting to $IMAPHost on Port $IMAPPort";

							              echo "Please wait ... "

							              echo

                                          echo > temporary.txt

							              exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



							              echo -en "a login $user4@openwave.com $user4\r\n" >&3

									      echo -en "a select INBOX\r\n" >&3

									      echo -en "a fetch 8:* envelope\r\n" >&3

									      echo -en "a logout\r\n" >&3

									      cat <&3 >>temporary.txt

									      

									 

									      invalid_field=$(cat temporary.txt | grep -i "()" | wc -l)

									      invalid_field=`echo $invalid_field | tr -d " " `

									 

									      if [ $invalid_field -gt 0 ]

									      then

									          echo

									          echo " Invalid header field found in fetch envelope "

										      echo " Scenario 5 is not working fine "	

                                              summary "Fetch(Envelope) for invalid CC header " 1 "ITS-1329310 "											  

									     else

									          echo

									          echo " Invalid header field not found in fetch envelope "

										      echo " Scenario 5 is working fine "

                                              summary "Fetch(Envelope) for invalid CC header " 0 									  

									     fi

										 

								     else

                                          echo " Mails not delivered "

										  summary "Fetch(Envelope) for invalid CC header " 1

                                     fi

									 

									 ## CASE 6 : FOR INVALID BCC HEADER ###

									 echo "###################################"

									 echo " CASE 6 :  FOR INVALID BCC HEADER "

									 echo "###################################"

									 

									 echo "From: $MAILFROM@openwave.com "  > message.txt 

							         echo "To: $user4@openwave.com; anyuser " >> message.txt

									 echo " BCC: unknown" >> message.txt

								     echo "Subject: Test mail" >> message.txt

							         echo >> message.txt

									 echo "This is a test mail ..."  >> message.txt

									 

									 msg=`cat message.txt`

  									 ## Message Delivery Starts

				                     echo

				                     echo "-------Message Delivery Starts--------"

				                     echo "Connecting to $MTAHost on Port $SMTPPort";

				                     echo "Please wait ... "

				                     echo

				                     exec 3<>/dev/tcp/$MTAHost/$SMTPPort



				                     echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                     echo -en "RCPT TO:$user4\r\n" >&3

				                     echo -en "DATA\r\n" >&3

				                     echo -en "$msg\r\n" >&3

				                     echo -en ".\r\n" >&3

      							     echo -en "QUIT\r\n" >&3

				                         

									 

									 imboxstats $MBXID_Sanity4 > boxstats.txt

				                     msgs_user1=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                     msgs_user1=`echo $msgs_user1 | tr -d " "`

									 if [ $msgs_user1 -gt 8 ]

									 then

									      ## IMAP OPERATIONS START FROM HERE

									      echo "================================="

							              echo "PERFORMING IMAP FETCH (ENVELOPE)"

							              echo "================================="

							              echo

							              echo "Connecting to $IMAPHost on Port $IMAPPort";

							              echo "Please wait ... "

							              echo

                                          echo > temporary.txt

							              exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



							              echo -en "a login $user4@openwave.com $user4\r\n" >&3

									      echo -en "a select INBOX\r\n" >&3

									      echo -en "a fetch 9:* envelope\r\n" >&3

									      echo -en "a logout\r\n" >&3

									      cat <&3 >>temporary.txt

									      

									 

									      invalid_field=$(cat temporary.txt | grep -i "()" | wc -l)

									      invalid_field=`echo $invalid_field | tr -d " " `

									 

									      if [ $invalid_field -gt 0 ]

									      then

									          echo

									          echo " Invalid header field found in fetch envelope "

										      echo " Scenario 6 is not working fine "

                                              summary "Fetch(Envelope) for invalid BCC header " 1 "ITS-1329310 "											  

									     else

									          echo

									          echo " Invalid header field not found in fetch envelope "

										      echo " Scenario 6 is working fine "	

											  summary "Fetch(Envelope) for invalid BCC header " 0

									     fi

										 

								     else

                                          echo " Mails not delivered "

										  summary "Fetch(Envelope) for invalid BCC header " 1

                                     fi

									 

									  ## CASE 7 : FOR UTF-8 CHARACTERS IN SUBJECT LINE ###

									 echo "################################################"

									 echo " CASE 7 :  FOR UTF-8 CHARACTERS IN SUBJECT LINE "

									 echo "################################################"

									 

									 echo "From: $MAILFROM@openwave.com "  > message.txt 

							         echo "To: $user4@openwave.com; anyuser " >> message.txt

									 echo " BCC: unknown" >> message.txt

								     echo "Subject: \x6a \x6b \x6c \x6d \x6e \x6f" >> message.txt

							         echo >> message.txt

									 echo "This is a test mail ..."  >> message.txt

									 

									 msg=`cat message.txt`

  									 ## Message Delivery Starts

				                     echo

				                     echo "-------Message Delivery Starts--------"

				                     echo "Connecting to $MTAHost on Port $SMTPPort";

				                     echo "Please wait ... "

				                     echo

				                     exec 3<>/dev/tcp/$MTAHost/$SMTPPort



				                     echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                     echo -en "RCPT TO:$user4\r\n" >&3

				                     echo -en "DATA\r\n" >&3

				                     echo -en "$msg\r\n" >&3

				                     echo -en ".\r\n" >&3

      							     echo -en "QUIT\r\n" >&3

				                         

									 

									 imboxstats $MBXID_Sanity4 > boxstats.txt

				                     msgs_user1=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                     msgs_user1=`echo $msgs_user1 | tr -d " "`

									 if [ $msgs_user1 -gt 9 ]

									 then

									      ## IMAP OPERATIONS START FROM HERE

									      echo "================================="

							              echo "PERFORMING IMAP FETCH (ENVELOPE)"

							              echo "================================="

							              echo

							              echo "Connecting to $IMAPHost on Port $IMAPPort";

							              echo "Please wait ... "

							              echo

                                          echo > temporary.txt

							              exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



							              echo -en "a login $user4@openwave.com $user4\r\n" >&3

									      echo -en "a select INBOX\r\n" >&3

									      echo -en "a fetch 10:* envelope\r\n" >&3

									      echo -en "a logout\r\n" >&3

									      cat <&3 >>temporary.txt

									      

									 

									      invalid_field=$(cat temporary.txt | grep -i "()" | wc -l)

									      invalid_field=`echo $invalid_field | tr -d " " `

									 

									      if [ $invalid_field -gt 0 ]

									      then

									          echo

									          echo " Invalid header field found in fetch envelope "

										      echo " Scenario 7 is not working fine "	

                                              summary "Fetch(Envelope) for UTF-8 characters in subject " 1 "ITS-1329310 "											  

									     else

									          echo

									          echo " Invalid header field not found in fetch envelope "

										      echo " Scenario 7 is working fine "

                                              summary "Fetch(Envelope) for UTF-8 characters in subject " 0										  

									     fi

										 

								     else

                                          echo " Mails not delivered "

										  summary "Fetch(Envelope) for UTF-8 characters in subject " 1

                                     fi

								else

								    echo " ERROR : No messages in the inbox "

									summary "FETCH (envelope) - all test cases " 7

								fi

								

                                echo "XX-----Testing of Fetch (envelope) utility is completed-----XX"

								echo

}

function ITS_1318609() {

                                start_time_tc ITS_1318609_tc

								##  ITS - Issue 1318609

                                ## MSS clients can't recognize that MSS has been downed.



                                echo "#####################################################"

                                echo "| VERIFY MSS CLIENT CAN RECOGNIZE WHEN MSS IS DOWN |"

                                echo "#####################################################"

                                echo	

                               

							    imconfcontrol -install -key /*/popserv/traceOutputLevel="msspool=1" >>trash.txt

                                

								updtd_key_value=$(cat config/config.db | grep -i "/*/popserv/traceOutputLevel" | cut -d "[" -f2 | cut -d "]" -f1)	

							    updtd_key_value=`echo $updtd_key_value | tr -d " "`

								

								if [ "$updtd_key_value" == "msspool=1" ]

								then

								    echo " Trace output level is set " 

									

									imctrl $FirstMSS kill mss.1 >>trash.txt

							        imservping -h $FirstMSS mss > ping_resp.txt

							        resp_count=$(cat ping_resp.txt | grep -i "unreachable" | wc -l)

							        resp_count=`echo $resp_count | tr -d " "`

									

									imctrl $SecondMSS kill mss.1 >>trash.txt

							        imservping -h $SecondMSS mss > ping_resp.txt

							        resp_count1=$(cat ping_resp.txt | grep -i "unreachable" | wc -l)

							        resp_count1=`echo $resp_count1 | tr -d " "`

									

									if [ "$resp_count" == "2" ] && [ "$resp_count1" == "2" ]

									then

									     echo " MSS is down "

										 ## POP OPERATIONS START FROM HERE

						                 echo " PERFORMING POP OPERATIONS "

						  				 echo

						                 echo "Connecting to $POPHost on Port $POPPort";

						                 echo "Please wait ... "

						                 echo



						                 exec 3<>/dev/tcp/$POPHost/$POPPort



						                 echo -en "user $user1\r\n" >&3

						                 echo -en "pass $user1\r\n" >&3

						                 echo -en "quit\r\n" >&3

						                 

										 

										 failed=$(cat $INTERMAIL/log/popserv.trace | grep -i "MSSCnctMgr::newConnection failed" | wc -l)

                                         failed=` echo $failed | tr -d " "`	

                                         if [ "$failed" == "1" ]

                                         then

										      echo " New connection failed event found in pop server's trace " 

                                         else

										      echo " New connection failed event not found in pop server's trace "

                                         fi		

                                         

										 failed2=$(cat $INTERMAIL/log/popserv.log | grep -i "Urgt;NioConnServerFail" | wc -l)

                                         failed2=` echo $failed2 | tr -d " "`	

										 failed1=$(cat $INTERMAIL/log/popserv.log | grep -i "Note;NioMssConnectionUnavailable" | wc -l)

                                         failed1=` echo $failed1 | tr -d " "`	

                                         if [ "$failed2" == "1" ]

                                         then

										      echo " Urgt;NioConnServerFail event found in pop server's log " 

                                         else

										      echo " Urgt;NioConnServerFail event not found in pop server's log "

                                         fi	

                                         

										 if [ "$failed1" == "1" ]

                                         then

										      echo " Note;NioMssConnectionUnavailable event found in pop server's log " 

											  echo " Utility working fine"

											  summary "MSS client can recognise when mss is down " 0

                                         else

										      echo " Note;NioMssConnectionUnavailable event not found in pop server's log "

											  echo " Utility not working "

											  summary "MSS client can recognise when mss is down " 1 "ITS-1318609"

                                         fi	

									     

                                         imctrl $FirstMSS start mss.1 >>trash.txt

							             imservping -h $FirstMSS mss > ping_resp.txt

							             resp_count=$(cat ping_resp.txt | grep -i "unreachable" | wc -l)

							             resp_count=`echo $resp_count | tr -d " "`

									

									     imctrl $SecondMSS start mss.1 >>trash.txt

							             imservping -h $SecondMSS mss > ping_resp.txt

							             resp_count1=$(cat ping_resp.txt | grep -i "unreachable" | wc -l)

							             resp_count1=`echo $resp_count1 | tr -d " "`

										 

										 if [ "$resp_count" == "0" ] && [ "$resp_count1" == "0" ]

									     then

										     echo " MSS started again "

										 else

										     echo " ERROR : Not able to start MSS "

										 fi

									else

									     " ERROR: MSS is not down "

										 summary "MSS client can recognise when mss is down " 1

									fi

									

									imconfcontrol -install -key /*/popserv/traceOutputLevel="" >keys_set.txt

                                    updtd_key_value=$(cat config/config.db | grep -i "/*/popserv/traceOutputLevel" | cut -d "[" -f2 | cut -d "]" -f1)	

							        updtd_key_value=`echo $updtd_key_value | tr -d " "` 

									if [ "$updtd_key_value" == "" ]

									then

									    echo " Trace output level is set back to original value " 

									else

									    echo " ERROR: Trace output level is not set back to original value " 

									fi

									

								else

								    echo " ERROR: Trace output level is not set " 

									summary "MSS client can recognise when mss is down " 1

								fi

								echo

								echo "XX-----Testing MSS clients can't recognize that MSS has been downed is completed-----XX"

							    echo

}

function ITS_1319590() {

                                start_time_tc ITS_1319590_tc

								##ITS - Issue 1319590

                                ## utility 'immsgdelete' throws error when run to delete messages from mailbox on Mx8.1.5, Mx8.1.6 and Mx8.1.5.x

                       

                                echo "##############################"

                                echo "| VERIFY IMMSGDELETE UTILITY |"

                                echo "###############################"

                                echo	

                               

                                imboxstats $MBXID_Sanity2 > boxstats.txt

				                msgs_user=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                msgs_user=`echo $msgs_user | tr -d " "`

								

				                if [ $msgs_user -gt 0 ]

								then

								    immsgdelete $user2@openwave.com -all

									imboxstats $MBXID_Sanity2 > boxstats.txt

				                    msgs_user1=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                    msgs_user1=`echo $msgs_user1 | tr -d " "`

									

									if [ "$msgs_user1" == "0" ]

									then

									    echo " Mail box emptied "

										echo " immsgdelete -all utility is working fine "

									else

									    echo " ERROR : Mail box is not emptied "

										echo " ERROR : immsgdelete -all utility is not working  "

									fi

								fi

								## Sending message 

							    echo "-------Sending Message Starts--------"

							    echo "Connecting to $MTAHost on Port $SMTPPort";

							    echo "Please wait ... "

							    echo

                                echo > mail.txt

							    exec 3<>/dev/tcp/$MTAHost/$SMTPPort



							    echo -en "MAIL FROM:$MAILFROM\r\n" >&3

							    echo -en "RCPT TO:$user2\r\n" >&3

                                echo -en "DATA\r\n" >&3

							    echo -en "Subject: $SUBJECT\r\n\r\n" >&3

							    echo -en "$DATA\r\n" >&3

							    echo -en ".\r\n" >&3

							    echo -en "QUIT\r\n" >&3

							    cat <&3 >> mail.txt

							    echo "-------Message Sent--------"

							    echo

								   

								imboxstats $MBXID_Sanity2 > boxstats.txt

				                msgs_user=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                msgs_user=`echo $msgs_user | tr -d " "`

				                if [ $msgs_user -gt 0 ]

				                then

					                echo $msgs_user" Mail is delivered successfully."

									msgid=$(grep "Message received:" mail.txt| cut -d ":" -f2 | sed -e 's/^\s*//' -e 's/\s*$//' | tail -1 | cut -d "[" -f1) 

							        msgid=` echo $msgid | tr -d " "`

									immsgdelete $user2@openwave.com $msgid > result.txt

									

									failed=$(cat result.txt | grep -i "failed" | wc -l)

									failed=`echo $failed | tr -d " " `

									

									if [ "$failed" == "0" ]

									then

									    echo " Mail deleted "

										echo " immsgdelete utility for single message is working fine "

										summary "Immsgdelete for single message " 0

									else

									    echo " Mail not deleted "

										echo " immsgdelete utility for single message is not working  "

										summary "Immsgdelete for single message " 1 "ITS-1319590"

									fi

																		

							    else

								    echo " Mail cannot be delivered "

									summary "Immsgdelete for single message " 1

                                fi							

  

                                echo "XX-----Testing of immsgdelete utility is completed-----XX"

							    echo

}

function ITS_1326578() {

                                start_time_tc ITS_1326578_tc

                                ##  ITS - Issue 1326578 

                                ## folder name is not correctly changed by rename command

                       

                                echo "##################################"

                                echo "| VERIFY RENAME COMMAND IN IMAP |"

                                echo "##################################"

                                echo	

                               

                                ## IMAP OPERATIONS START FROM HERE

							    echo "=========================="

							    echo "PERFORMING IMAP OPERATIONS"

							    echo "=========================="

							    echo

							    echo "Connecting to $IMAPHost on Port $IMAPPort";

							    echo "Please wait ... "

							    echo

                                echo > temporary.txt

							    exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



							    echo -en "a login $user1@openwave.com $user1\r\n" >&3

                                echo -en "a create inbox_new\r\n" >&3	

								echo -en "a list \"\" *\r\n" >&3

                                echo -en "a rename inbox_new inbox_other\r\n" >&3	

                                echo -en "a list \"\" *\r\n" >&3

                                echo -en "a delete inbox_other\r\n" >&3										

                                echo -en "a logout\r\n" >&3

							    

							    cat <&3 >>temporary.txt

								

 

							    ### Check for errors in the imap logs

							    cat $INTERMAIL/log/imapserv.log| egrep -i "erro;|urgt;|fatl;" > imap_errors.txt

							    echo "Errors logged in IMAP Logs:"

							    echo "=========================="

							    cat imap_errors.txt

							    echo

							    echo "=========================="

								

								invalid=$( cat temporary.txt | grep "INBOX_other" |wc -l)

								invalid=`echo $invalid | tr -d  " "`

								if [ "$invalid" == "0" ]

								then

								    echo " The folder is renamed properly "

									echo " Rename utilty is working fine "

									summary "Rename in IMAP " 0

								else

								    echo " ERROR : The folder is not renamed properly "

									echo " ERROR : Rename utilty is not working "

									summary "Rename in IMAP " 1 "ITS-1326578 "

								fi

								echo

                                echo "XX-----Testing of rename utility is completed-----XX"

							    echo

}

function mailbox_creation_smtp() {

                                    start_time_tc mailbox_creation_smtp_tc

                                   

                                    

                                      ### Testing creation of mailbox using smtp

                                    echo "#############################################"

                                    echo "|  VERIFING CREATION OF MAILBOX USING SMTP |"

                                    echo "#############################################"

                                    echo	

						

						            					

							        ##  Message Delivery Starts

				                    echo

				                    echo "------- Message Delivery Starts--------"

				                    echo "Connecting to $MTAHost on Port $SMTPPort";

				                    echo "Please wait ... "

				                    echo >mail.txt

				                    exec 3<>/dev/tcp/$MTAHost/$SMTPPort



				                    echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                    echo -en "RCPT TO:$user27\r\n" >&3

				                    echo -en "DATA\r\n" >&3

				                    echo -en "Subject: $SUBJECT\r\n\r\n" >&3

				                    echo -en "$DATA\r\n" >&3

				                    echo -en ".\r\n" >&3

				                    echo -en "QUIT\r\n" >&3

									cat <&3 >> mail.txt

				                    echo

									msgid=$(grep "Message received:" mail.txt| cut -d ":" -f2 | sed -e 's/^\s*//' -e 's/\s*$//' | tail -1 | cut -d "[" -f1) 

							        msgid=` echo $msgid | tr -d " "`

									ownerMSS=$(cat $INTERMAIL/log/mta.log| egrep -i delivered | grep -i $user27 | grep -i $msgid | cut -d ":" -f4 | cut -d "=" -f2 | sort -u)

							        ownerMSS=`echo $ownerMSS | tr -d " "`

							        echo "Owner MSS of the mailbox used is: "$ownerMSS

							        echo

									sleep 1

									created=$(cat $INTERMAIL/log/mta.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity27 | wc -l)

									created=`echo $created | tr -d " " `

									echo "$created1"

									if [ "$created" > "0" ]

									then

									    echo " Mailbox created "

										echo " Utility is working fine "

										summary "Mailbox creation through SMTP " 0

									else

									    echo " ERROR : Mailbox not created "

										echo " Utility is not working fine "

										summary "Mailbox creation through SMTP " 1

									fi

									

									imboxdelete $Cluster $MBXID_Sanity27

									imboxstats $MBXID_Sanity27 > chk_mailbox.txt

                                    mbx_del=$(cat chk_mailbox.txt | grep -i failed | wc -l)

                                    mbx_del=`echo $mbx_del | tr -d " "`

                                    if [ "$mbx_del" == "1" ]

                                    then

	                                    echo 

	                                    echo "Mailbox deleted successfully."

	                                else

                                        echo " ERROR: Mailbox is not deleted..."

	                                fi

							        echo						

                                    echo "XX-----Testing creation of mailbox using smtp is completed-----XX"

                                    echo									

}

function mailbox_creation_pop(){

                                   start_time_tc mailbox_creation_pop_tc

                                    ### Testing creation of mailbox using pop

                                    echo "############################################"

                                    echo "|  VERIFING CREATION OF MAILBOX USING POP |"

                                    echo "############################################"

                                    echo

		                            

									## POP OPERATIONS 

						            echo "==========================="

						            echo " PERFORMING POP OPERATIONS "

						            echo "==========================="

						            echo

						            echo "Connecting to $POPHost on Port $POPPort";

						            echo "Please wait ... "

						            echo

                                    						

						            exec 3<>/dev/tcp/$POPHost/$POPPort



						            echo -en "user $user27\r\n" >&3

						            echo -en "pass $user27\r\n" >&3

						            echo -en "list\r\n" >&3

						           	echo -en "quit\r\n" >&3

						           	sleep 1							

									created1=$(cat log/popserv.log | grep -i "Note;MsMailboxCreated" | grep $user27 | wc -l)

									created=`echo $created | tr -d " " `

									echo "$created1"

									if [ "$created1" > "0" ]

									then

									    echo " Mailbox created "

										echo " Utility is working fine "

										summary "Mailbox creation through POP " 0

									else

									    echo " ERROR : Mailbox not created "

										echo " Utility is not working fine "

										summary "Mailbox creation through POP " 1

									fi

									

									imboxdelete $Cluster $MBXID_Sanity27

									imboxstats $MBXID_Sanity27 > chk_mailbox.txt

                                    mbx_del=$(cat chk_mailbox.txt | grep -i failed | wc -l)

                                    mbx_del=`echo $mbx_del | tr -d " "`

                                    if [ "$mbx_del" == "1" ]

                                    then

	                                    echo 

	                                    echo "Mailbox deleted successfully."

	                                else

                                        echo " ERROR: Mailbox is not deleted..."

	                                fi

									

									echo						

                                    echo "XX-----Testing creation of mailbox using pop is completed-----XX"

                                    echo

}

function mailbox_creation_imap(){

                                    start_time_tc mailbox_creation_imap_tc

                                    ### Testing creation of mailbox using imap

                                    echo "############################################"

                                    echo "|  VERIFING CREATION OF MAILBOX USING IMAP |"

                                    echo "############################################"

                                    echo

									

									## IMAP OPERATIONS START FROM HERE

							     	echo "=========================="

							        echo "PERFORMING IMAP OPERATIONS"

							        echo "=========================="

							        echo

							        echo "Connecting to $IMAPHost on Port $IMAPPort";

							        echo "Please wait ... "

							        echo

                                   

							        exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



							        echo -en "a login $user27@openwave.com $user27\r\n" >&3

							        echo -en "a select INBOX\r\n" >&3

							        echo -en "a logout\r\n" >&3

							        

									sleep 1

									created=$(cat $INTERMAIL/log/imapserv.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity27 | wc -l)

									created=`echo $created | tr -d " " `

									

									if [ "$created" > "0" ]

									then

									    echo " Mailbox created "

										echo " Utility is working fine "

										summary "Mailbox creation through IMAP " 0

									else

									    echo " ERROR : Mailbox not created "

										echo " Utility is not working fine "

										summary "Mailbox creation through IMAP " 1

									fi

									

		                            imboxdelete $Cluster $MBXID_Sanity27

									imboxstats $MBXID_Sanity27 > chk_mailbox.txt

                                    mbx_del=$(cat chk_mailbox.txt | grep -i failed | wc -l)

                                    mbx_del=`echo $mbx_del | tr -d " "`

                                    if [ "$mbx_del" == "1" ]

                                    then

	                                    echo 

	                                    echo "Mailbox deleted successfully."

	                                else

                                        echo " ERROR: Mailbox is not deleted..."

	                                fi

									

									echo						

                                    echo "XX-----Testing creation of mailbox using imap is completed-----XX"

                                    echo

}

function mailbox_creation_immtamassmail(){

                               start_time_tc mailbox_creation_immtamassmail_tc

							   ### Testing new immtamassmail utility for creation of mailbox

                               echo "######################################################"

                               echo "| VERIFY IMMTAMASSMAIL UTILTY FOR MAILBOX CREATION  |"

                               echo "######################################################"

                               echo			

                            

							   imboxdelete $Cluster $MBXID_Sanity2

							   imboxdelete $Cluster $MBXID_Sanity3

							   imboxdelete $Cluster $MBXID_Sanity4

							   

							   imboxstats  $MBXID_Sanity2 > chk_mailbox.txt

							   msg2=$(cat chk_mailbox.txt | grep -i failed | wc -l)

							   msg2=`echo $msg2 | tr -d " "`

							   

							   imboxstats  $MBXID_Sanity3 > chk_mailbox.txt

							   msg3=$(cat chk_mailbox.txt | grep -i failed | wc -l)

							   msg3=`echo $msg3 | tr -d " "`

							   

							   imboxstats  $MBXID_Sanity4 > chk_mailbox.txt

							   msg4=$(cat chk_mailbox.txt | grep -i failed | wc -l)

							   msg4=`echo $msg4 | tr -d " "`

							   

							   if [ "$msg2" == "1" ]

							   then

							       if [ "$msg3" == "1" ]

								   then

								       if [ "$msg4" == "1" ]

									   then

									        echo "$user2" > recepient.txt

											echo "$user3" >> recepient.txt

											echo "$user4" >> recepient.txt

											echo "Recepients : "

											

											

											echo "From: $user1@openwave.com" > message.txt

											echo "To: All" >> message.txt

											echo "Subject: Mass mail" >> message.txt

											echo  >> message.txt

											echo "This is mail to test immtamassmail utility " >> message.txt

											echo "Message file : "

											

											

											immtamassmail recepient.txt message.txt 

                                            

											#created=$(cat $INTERMAIL/log/mta.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity2 | wc -l)

									        #created=`echo $created | tr -d " " `

									        

										    #created1=$(cat $INTERMAIL/log/mta.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity3 | wc -l)

									        #created1=`echo $created1 | tr -d " " `

											

											#created2=$(cat $INTERMAIL/log/mta.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity4 | wc -l)

									        #created2=`echo $created1 | tr -d " " `

											imboxstats  $MBXID_Sanity2 > boxstats.txt

							                msg21=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

							                msg21=`echo $msg21 | tr -d " "`

							   

							                imboxstats  $MBXID_Sanity3 > boxstats.txt

							                msg31=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

							                msg31=`echo $msg31 | tr -d " "`

							   

							                imboxstats  $MBXID_Sanity4 > boxstats.txt

							                msg41=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

							                msg41=`echo $msg41 | tr -d " "`

											

									        if [ "$msg21" == "1" ] && [ "$msg31" == "1" ] && [ "$msg41" == "1" ]

									        #if [ "$created" == "1" ] && [ "$created1" == "1" ] && [ "$created2" == "1" ] 

									        then

									             echo " Mailbox created "

										         echo " Utility is working fine "

												 summary "Mailbox creation through immtamassmail " 0

									        else

									             echo " ERROR : Mailbox not created "

										         echo " Utility is not working fine "

												 summary "Mailbox creation through immtamassmail " 1

									        fi

							                

									   else

									       echo "ERROR : Mailbox of "$user4" cannot be deleted "

									   fi

								   else

								       echo "ERROR : Mailbox of "$user3" cannot be deleted "

								   fi

							   else

							       echo "ERROR : Mailbox of "$user2" cannot be deleted "

							   fi

							   

							   echo						

                               echo "XX-----Testing immtamassmail utility for creation of mailbox is completed -----XX"

                               echo

							   

}

function mailbox_creation_immsgtrace(){

 start_time_tc mailbox_creation_immsgtrace_tc

### Testing new immsgtrace utility for creation of mailbox

                               echo "######################################################"

                               echo "| VERIFY IMMSGTRACE UTILTY FOR MAILBOX CREATION  |"

                               echo "######################################################"

                               echo		

							   

							   immsgtrace -to $user27 -size 5 >output.txt

                               sleep 1

							   created=$(cat $INTERMAIL/log/mta.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity27 | wc -l)

							   created=`echo $created | tr -d " " `

									

									if [ "$created" > "0" ]

									then

									    echo " Mailbox created "

										echo " Utility is working fine "

										summary "Mailbox creation through immsgtrace " 0

									else

									    echo " ERROR : Mailbox not created "

										echo " Utility is not working fine "

										summary "Mailbox creation through immsgtrace " 1

									fi

									

									imboxdelete $Cluster $MBXID_Sanity27

									imboxstats $MBXID_Sanity27 > chk_mailbox.txt

                                    mbx_del=$(cat chk_mailbox.txt | grep -i failed | wc -l)

                                    mbx_del=`echo $mbx_del | tr -d " "`

                                    if [ "$mbx_del" == "1" ]

                                    then

	                                    echo 

	                                    echo "Mailbox deleted successfully."

	                                else

                                        echo " ERROR: Mailbox is not deleted..."

	                                fi

							   

							   echo						

                               echo "XX-----Testing immsgtrace utility for creation of mailbox is completed -----XX"

                               echo

							   

							   

}

function mailbox_creation_smtp2() {

start_time_tc mailbox_creation_smtp2_tc

### Verifying mailbox creation through various operations using surrogate

									 

									echo "Please wait while we shutdown owner MSS... "

							        imctrl $ownerMSS kill mss.1

							        imservping -h $ownerMSS mss > ping_resp.txt

							        resp_count=$(cat ping_resp.txt | grep -i "unreachable" | wc -l)

							        resp_count=`echo $resp_count | tr -d " "`

							        if [ "$resp_count" == "2" ]

							        then

								        echo "MSS on "$ownerMSS" is successfully shutdown"

									else

									    echo "MSS on "$ownerMSS" is not successfully shutdown" 

									fi

									

									 ### Testing creation of mailbox using smtp when owner mss is down

                                    echo "###################################################################"

                                    echo "|  VERIFING CREATION OF MAILBOX USING SMTP WHEN OWNER MSS IS DOWN|"

                                    echo "###################################################################"

                                    echo	

						

						            					

							        ##  Message Delivery Starts

				                    echo

				                    echo "------- Message Delivery Starts--------"

				                    echo "Connecting to $MTAHost on Port $SMTPPort";

				                    echo "Please wait ... "

				                    echo

				                    exec 3<>/dev/tcp/$MTAHost/$SMTPPort



				                    echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                    echo -en "RCPT TO:$user27\r\n" >&3

				                    echo -en "DATA\r\n" >&3

				                    echo -en "Subject: $SUBJECT\r\n\r\n" >&3

				                    echo -en "$DATA\r\n" >&3

				                    echo -en ".\r\n" >&3

				                    echo -en "QUIT\r\n" >&3

				                    

									sleep 1

									created=$(cat $INTERMAIL/log/mta.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity27 | wc -l)

									created=`echo $created | tr -d " " `

									

									if [ "$created" > "0" ]

									then

									    echo " Mailbox created "

										echo " Utility is working fine "

										summary " Mailbox creation through SMTP using surrogate" 0

									else

									    echo " ERROR : Mailbox not created "

										echo " Utility is not working fine "

										summary " Mailbox creation through SMTP using surrogate" 1

									fi

									

									imboxdelete $Cluster $MBXID_Sanity27

									imboxstats $MBXID_Sanity27 > chk_mailbox.txt

                                    mbx_del=$(cat chk_mailbox.txt | grep -i failed | wc -l)

                                    mbx_del=`echo $mbx_del | tr -d " "`

                                    if [ "$mbx_del" == "1" ]

                                    then

	                                    echo 

	                                    echo "Mailbox deleted successfully."

	                                else

                                        echo " ERROR: Mailbox is not deleted..."

	                                fi

							        echo						

                                    echo "XX-----Testing creation of mailbox using smtp when owner mss is down is completed-----XX"

                                    echo

									

}

function mailbox_creation_pop2(){

start_time_tc mailbox_creation_pop2_tc

### Testing creation of mailbox using pop when owner mss is down

                                    echo "##################################################################"

                                    echo "|  VERIFING CREATION OF MAILBOX USING POP WHEN OWNER MSS IS DOWN|"

                                    echo "##################################################################"

                                    echo

		                            

									## POP OPERATIONS 

						            echo "==========================="

						            echo " PERFORMING POP OPERATIONS "

						            echo "==========================="

						            echo

						            echo "Connecting to $POPHost on Port $POPPort";

						            echo "Please wait ... "

						            echo

                                    						

						            exec 3<>/dev/tcp/$POPHost/$POPPort



						            echo -en "user $user27\r\n" >&3

						            echo -en "pass $user27\r\n" >&3

						            echo -en "list\r\n" >&3

						           	echo -en "quit\r\n" >&3

						            

									sleep 1

									created=$(cat $INTERMAIL/log/popserv.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity27 | wc -l)

									created=`echo $created | tr -d " " `

									

									if [ "$created" > "0" ]

									then

									    echo " Mailbox created "

										echo " Utility is working fine "

										summary " Mailbox creation through POP using surrogate" 0

									else

									    echo " ERROR : Mailbox not created "

										echo " Utility is not working fine "

										summary " Mailbox creation through POP using surrogate" 1

									fi

									

									imboxdelete $Cluster $MBXID_Sanity27

									imboxstats $MBXID_Sanity27 > chk_mailbox.txt

                                    mbx_del=$(cat chk_mailbox.txt | grep -i failed | wc -l)

                                    mbx_del=`echo $mbx_del | tr -d " "`

                                    if [ "$mbx_del" == "1" ]

                                    then

	                                    echo 

	                                    echo "Mailbox deleted successfully."

	                                else

                                        echo " ERROR: Mailbox is not deleted..."

	                                fi

									

									echo						

                                    echo "XX-----Testing creation of mailbox using pop when owner mss is down is completed-----XX"

                                    echo

		   

}

function mailbox_creation_imap2() {

start_time_tc mailbox_creation_imap2_tc

 ### Testing creation of mailbox using imap when owner mss is down

                                    echo "####################################################################"

                                    echo "|  VERIFING CREATION OF MAILBOX USING IMAP WHEN OWNER MSS IS DOWN|"

                                    echo "####################################################################"

                                    echo

									

									## IMAP OPERATIONS START FROM HERE

							     	echo "=========================="

							        echo "PERFORMING IMAP OPERATIONS"

							        echo "=========================="

							        echo

							        echo "Connecting to $IMAPHost on Port $IMAPPort";

							        echo "Please wait ... "

							        echo

                                   

							        exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



							        echo -en "a login $user27@openwave.com $user27\r\n" >&3

							        echo -en "a select INBOX\r\n" >&3

							        echo -en "a logout\r\n" >&3

							        

									sleep 1

									created=$(cat $INTERMAIL/log/imapserv.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity27 | wc -l)

									created=`echo $created | tr -d " " `

									

									if [ "$created" > "0" ]

									then

									    echo " Mailbox created "

										echo " Utility is working fine "

										summary " Mailbox creation through IMAP using surrogate" 0

									else

									    echo " ERROR : Mailbox not created "

										echo " Utility is not working fine "

										summary " Mailbox creation through IMAP using surrogate" 1

									fi

									

		                            imboxdelete $Cluster $MBXID_Sanity27

									imboxstats $MBXID_Sanity27 > chk_mailbox.txt

                                    mbx_del=$(cat chk_mailbox.txt | grep -i failed | wc -l)

                                    mbx_del=`echo $mbx_del | tr -d " "`

                                    if [ "$mbx_del" == "1" ]

                                    then

	                                    echo 

	                                    echo "Mailbox deleted successfully."

	                                else

                                        echo " ERROR: Mailbox is not deleted..."

	                                fi

									

									echo						

                                    echo "XX-----Testing creation of mailbox using imap when owner mss is down is completed-----XX"

                                    echo

}

function mailbox_creation_immtamassmail2(){

start_time_tc mailbox_creation_immtamassmail2_tc

### Testing new immtamassmail utility for creation of mailbox through surrogate

                               echo "#######################################################################"

                               echo "| VERIFY IMMTAMASSMAIL UTILTY FOR MAILBOX CREATION THROUGH SURROGATE  |"

                               echo "#######################################################################"

                               echo			

                            

							   imboxdelete $Cluster $MBXID_Sanity3

							   imboxdelete $Cluster $MBXID_Sanity4

							   

							   imboxstats  $MBXID_Sanity27 > chk_mailbox.txt

							   msg2=$(cat chk_mailbox.txt | grep -i failed | wc -l)

							   msg2=`echo $msg2 | tr -d " "`
                              msg2=$(($msg2+1))
							   

							   imboxstats  $MBXID_Sanity3 > chk_mailbox.txt

							   msg3=$(cat chk_mailbox.txt | grep -i failed | wc -l)

							   msg3=`echo $msg3 | tr -d " "`
                               msg3=$(($msg3+1))
							   

							   imboxstats  $MBXID_Sanity4 > chk_mailbox.txt

							   msg4=$(cat chk_mailbox.txt | grep -i failed | wc -l)

							   msg4=`echo $msg4 | tr -d " "`
                               msg4=$(($msg4+1))
							   

							   if [ "$msg2" == "1" ]

							   then

							       if [ "$msg3" == "1" ]

								   then

								       if [ "$msg4" == "1" ]

									   then

									        echo "$user27@openwave.com" > recepient.txt

											echo "$user3@openwave.com" >> recepient.txt

											echo "$user4@openwave.com" >> recepient.txt

											echo "Recepients : "

											cat recepient.txt

											

											echo "From: $user1@openwave.com" > message.txt

											echo "To: All" >> message.txt

											echo "Subject: Mass mail" >> message.txt

											echo  >> message.txt

											echo "This is mail to test immtamassmail utility " >> message.txt

											echo "Message file : "

											cat message.txt

											

											immtamassmail recepient.txt message.txt

                                            

											#created=$(cat $INTERMAIL/log/mta.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity2 | wc -l)

									        #created=`echo $created | tr -d " " `

									        

										    #created1=$(cat $INTERMAIL/log/mta.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity3 | wc -l)

									        #created1=`echo $created1 | tr -d " " `

											

											#created2=$(cat $INTERMAIL/log/mta.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity4 | wc -l)

									        #created2=`echo $created1 | tr -d " " `

											imboxstats  $MBXID_Sanity2 > boxstats.txt

							                msg21=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

							                msg21=`echo $msg21 | tr -d " "`

							   

							                imboxstats  $MBXID_Sanity3 > boxstats.txt

							                msg31=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

							                msg31=`echo $msg31 | tr -d " "`

							   

							                imboxstats  $MBXID_Sanity4 > boxstats.txt

							                msg41=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

							                msg41=`echo $msg41 | tr -d " "`

											

									        if [ "$msg21" == "$msg2" ] && [ "$msg31" == "$msg3" ] && [ "$msg41" == "$msg4" ]

									        #if [ "$created" == "1" ] && [ "$created1" == "1" ] && [ "$created2" == "1" ] 


									        then

									             echo " Mailbox created "

										         echo " Utility is working fine "

												 summary " Mailbox creation through immtamassmail using surrogate" 0

									        else

									             echo " ERROR : Mailbox not created "

										         echo " Utility is not working fine "

												 summary " Mailbox creation through immtamassmail using surrogate" 1

									        fi

							                

									   else

									       echo "ERROR : Mailbox of "$user4" cannot be deleted "

									   fi

								   else

								       echo "ERROR : Mailbox of "$user3" cannot be deleted "

								   fi

							   else

							       echo "ERROR : Mailbox of "$user2" cannot be deleted "

							   fi

							   

							   echo						

                               echo "XX-----Testing immtamassmail utility for creation of mailbox through surrogate is completed -----XX"

                               echo 

}

function ITS_1324873() {

        					start_time_tc ITS_1324873_tc

							

							start_field=$( cat $INTERMAIL/log/mss.trace | grep -i "start CassStore::readMessageBytes" | wc -l )

							start_field=` echo $start_field | tr -d " "`

							start_field1=$(($start_field+3))

							

							stop_field=$( cat $INTERMAIL/log/mss.trace | grep -i "stop CassStore::readMessageBytes" | wc -l )

							stop_field=` echo $stop_field | tr -d " "`

							stop_field1=$(($stop_field+3))
                            echo " start === "$start_field1
							echo " stop === "$stop_field1

							   ##  ITS - Issue 1324873 

                               ## imap fetch rfc822 need not read header summary column/file on cassandra

                               echo "#################"

                               echo " ITS 1324873 " 

							   echo "#################"



							   imconfcontrol -install -key /*/common/traceOutputLevel="cluster=3,cassInfo=3D2" >keys_set.txt

							   updtd_key_value=$(imconfget traceOutputLevel )	

							   updtd_key_value=`echo $updtd_key_value | tr -d " "`

							   

							   if [ "$updtd_key_value" == "cluster=3,cassInfo=3D2" ]

							   then

							       echo " Traceoutput level set "

								  

									   msg_100kb=`cat 100K`

														

							           ##  Message Delivery Starts

				                       echo

				                       echo "-------Message Delivery Starts--------"

				                       echo "Connecting to $MTAHost on Port $SMTPPort";

				                       echo "Please wait ... "

				                       echo



				                       exec 3<>/dev/tcp/$MTAHost/$SMTPPort



				                       echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                       echo -en "RCPT TO:$user25\r\n" >&3

				                       echo -en "DATA\r\n" >&3

				                       echo -en "Subject: $SUBJECT\r\n\r\n" >&3

				                       echo -en "$msg_100kb\r\n" >&3

									   echo -en "$DATA\r\n" >&3

				                       echo -en ".\r\n" >&3

									   echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                       echo -en "RCPT TO:$user25\r\n" >&3

				                       echo -en "DATA\r\n" >&3

				                       echo -en "Subject: $SUBJECT\r\n\r\n" >&3

				                       echo -en "$DATA\r\n" >&3

				                       echo -en ".\r\n" >&3

									   echo -en "MAIL FROM:$MAILFROM\r\n" >&3

				                       echo -en "RCPT TO:$user25\r\n" >&3

				                       echo -en "DATA\r\n" >&3

				                       echo -en "Subject: $SUBJECT\r\n\r\n" >&3

				                       echo -en "$DATA\r\n" >&3

				                       echo -en ".\r\n" >&3

				                       echo -en "QUIT\r\n" >&3

    			                       

				                       imboxstats $MBXID_Sanity25 > boxstats.txt

				                       msgs_user=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                       msgs_user=`echo $msgs_user | tr -d " "`

				                       echo "============================================"

				                       if [ "$msgs_user" == "3" ]

				                       then

					                       echo

					                       echo $msgs_user" Mails were delivered successfully."



										   ### Testing fetch n rfc822

                                           echo "###########################################################"

                                           echo "| VERIFY IMAP FETCH N RFC822 DOES NOT READ HEADER SUMMARY|"

                                           echo "###########################################################"

                                           echo		



										   ## IMAP OPERATIONS START FROM HERE

							               echo "=========================="

							               echo "PERFORMING IMAP OPERATIONS"

							               echo "=========================="

							               echo

							               echo "Connecting to $IMAPHost on Port $IMAPPort";

							               echo "Please wait ... "

							               echo



                                           echo >mail.txt



							               exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



										   echo -en "a login $user25@openwave.com $user25\r\n" >&3

							               echo -en "a select INBOX\r\n" >&3

							               echo -en "a fetch 1:* rfc822\r\n" >&3

										   echo -en "a logout\r\n" >&3

							               

							               cat <&3 >> mail.txt

										   

										   

    									   ### Check for errors in the imap logs

							               cat $INTERMAIL/log/imapserv.log| egrep -i "erro;|urgt;|fatl;" > imap_errors.txt

							               echo "Errors logged in IMAP Logs:"

							               echo "=========================="

							               cat imap_errors.txt

							               echo

							               echo "=========================="

										   

    									   header_summary=$( cat log/mss.trace | grep -i "CassStore::getHeaderSummaryByMsgUUID" | wc -l )

										   header_summary=` echo $header_summary | tr -d " "`

										   if [ "$header_summary" == "0" ]

										   then

										       echo " HeaderSummary is not read "

										   else

										       echo " ERROR :HeaderSummary is read "

										   fi

										   

										   start_field=$( cat $INTERMAIL/log/mss.trace | grep -i "start CassStore::readMessageBytes" | wc -l )

										   start_field=` echo $start_field | tr -d " "`

										   

										   stop_field=$( cat $INTERMAIL/log/mss.trace | grep -i "stop CassStore::readMessageBytes" | wc -l )

										   stop_field=` echo $stop_field | tr -d " "`

			                              echo " start === "$start_field
							              echo " stop === "$stop_field

										   if [ "$start_field" == "$start_field1" ] && [ "$stop_field" == "$stop_field1" ]

										   then

										       echo " Start/Stop traces present  "

											   summary "IMAP fetch n rfc822 should not read header summary"  0

										   else

										       echo " ERROR : Start/Stop traces not present "

											   summary "IMAP fetch n rfc822 should not read header summary"  1  "ITS-1324873"

										   fi



										   echo

										   echo "XX-----Testing fetch n rfc822 is completed -----XX"

                                           echo	



										   ### Testing fetch n body[]

                                           echo "###########################################################"

                                           echo "| VERIFY IMAP FETCH N BODY[] DOES NOT READ HEADER SUMMARY|"

                                           echo "###########################################################"

                                           echo		

											start_field1=$(($start_field+3))

                                          	stop_field1=$(($stop_field+3))
                                              echo " start === "$start_field1
							                  echo " stop === "$stop_field1                                         

										   ## IMAP OPERATIONS START FROM HERE

							               echo "=========================="

							               echo "PERFORMING IMAP OPERATIONS"

							               echo "=========================="

							               echo

							               echo "Connecting to $IMAPHost on Port $IMAPPort";

							               echo "Please wait ... "

							               echo



                                           echo >mail.txt



							               exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



    									   echo -en "a login $user25@openwave.com $user25\r\n" >&3

							               echo -en "a select INBOX\r\n" >&3

							               echo -en "a fetch 1:* body[]\r\n" >&3

										   echo -en "a logout\r\n" >&3

							               

							               cat <&3 >> mail.txt

										   



										   ### Check for errors in the imap logs

							               cat $INTERMAIL/log/imapserv.log| egrep -i "erro;|urgt;|fatl;" > imap_errors.txt

							               echo "Errors logged in IMAP Logs:"

							               echo "=========================="

							               cat imap_errors.txt

							               echo

							               echo "=========================="

										   

    									   header_summary=$( cat log/mss.trace | grep -i "CassStore::getHeaderSummaryByMsgUUID" | wc -l )

										   header_summary=` echo $header_summary | tr -d " "`

										   if [ "$header_summary" == "0" ]

										   then

										       echo " HeaderSummary is not read "

										   else

										       echo " ERROR :HeaderSummary is read "

										   fi

										   

    									   start_field=$( cat $INTERMAIL/log/mss.trace | grep -i "start CassStore::readMessageBytes" | wc -l )

										   start_field=` echo $start_field | tr -d " "`

										   stop_field=$( cat $INTERMAIL/log/mss.trace | grep -i "stop CassStore::readMessageBytes" | wc -l )

										   stop_field=` echo $stop_field | tr -d " "`
                                           echo " start === "$start_field
							                echo " stop === "$stop_field
										 
										   if [ "$start_field" == "$start_field1" ] && [ "$stop_field" == "$stop_field1" ]

										   then

										       echo " Start/Stop traces present  "

											   summary "IMAP fetch n body[] should not read header summary"  0

										   else

										       echo " ERROR : Start/Stop traces not present "

											   summary "IMAP fetch n body[] should not read header summary"  1  "ITS-1324873"

										   fi



										   echo

										   echo "XX-----Testing fetch n body[] is completed -----XX"

                                           echo	



    									   ### Testing fetch n body[text]

                                           echo "###############################################################"

                                           echo "| VERIFY IMAP FETCH N BODY[TEXT] DOES NOT READ HEADER SUMMARY|"

                                           echo "###############################################################"

                                           echo		

								           start_field1=$(($start_field+3))

                                          	stop_field1=$(($stop_field+3))
                                           echo " start === "$start_field1
							               echo " stop === "$stop_field1
											

											

										   ## IMAP OPERATIONS START FROM HERE

							               echo "=========================="

							               echo "PERFORMING IMAP OPERATIONS"

							               echo "=========================="

							               echo

							               echo "Connecting to $IMAPHost on Port $IMAPPort";

							               echo "Please wait ... "

							               echo

                                           echo >mail.txt



							               exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



										   echo -en "a login $user25@openwave.com $user25\r\n" >&3

							               echo -en "a select INBOX\r\n" >&3

							               echo -en "a fetch 1:* body[text]\r\n" >&3

										   echo -en "a logout\r\n" >&3

							              

							               cat <&3 >> mail.txt

										   



										   ### Check for errors in the imap logs

							               cat $INTERMAIL/log/imapserv.log| egrep -i "erro;|urgt;|fatl;" > imap_errors.txt

							               echo "Errors logged in IMAP Logs:"

							               echo "=========================="

							               cat imap_errors.txt

							               echo

							               echo "=========================="

				   

    									   header_summary=$( cat log/mss.trace | grep -i "CassStore::getHeaderSummaryByMsgUUID" | wc -l )

										   header_summary=` echo $header_summary | tr -d " "`

										   if [ "$header_summary" == "0" ]

										   then

										       echo " HeaderSummary is not read "

										   else

										       echo " ERROR :HeaderSummary is read "

										   fi

								   

    									   start_field=$( cat $INTERMAIL/log/mss.trace | grep -i "start CassStore::readMessageBytes" | wc -l )

										   start_field=` echo $start_field | tr -d " "`

										  

										   stop_field=$( cat $INTERMAIL/log/mss.trace | grep -i "stop CassStore::readMessageBytes" | wc -l )

										   stop_field=` echo $stop_field | tr -d " "`

			                               echo " start === "$start_field
							               echo " stop === "$stop_field

										   if [ "$start_field" == "$start_field1" ] && [ "$stop_field" == "$start_field1" ]

										   then

										       echo " Start/Stop traces present  "

											   summary "IMAP fetch n body[text] should not read header summary" 0

										   else

										       echo " ERROR : Start/Stop traces not present "

											   summary "IMAP fetch n body[text] should not read header summary"  1  "ITS-1324873"

										   fi



										   echo

										   echo "XX-----Testing fetch n body[text] is completed -----XX"

                                           echo	

						   

										   ### Testing fetch n body[header]

                                           echo "#################################################################"

                                           echo "| VERIFY IMAP FETCH N BODY[HEADER] DOES NOT READ HEADER SUMMARY|"

                                           echo "#################################################################"

                                           echo		

                                           start_field1=$(($start_field+3))

                                          	stop_field1=$(($stop_field+3))
                                           echo " start === "$start_field1
							              echo " stop === "$stop_field1
										   ## IMAP OPERATIONS START FROM HERE

							               echo "=========================="

							               echo "PERFORMING IMAP OPERATIONS"

							               echo "=========================="

							               echo

							               echo "Connecting to $IMAPHost on Port $IMAPPort";

							               echo "Please wait ... "

							               echo



                                           echo >mail.txt



							               exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



										   echo -en "a login $user25@openwave.com $user25\r\n" >&3

							               echo -en "a select INBOX\r\n" >&3

							               echo -en "a fetch 1:* body[header]\r\n" >&3

										   echo -en "a logout\r\n" >&3

							               

							               cat <&3 >> mail.txt

										   



										   ### Check for errors in the imap logs

							               cat $INTERMAIL/log/imapserv.log| egrep -i "erro;|urgt;|fatl;" > imap_errors.txt

							               echo "Errors logged in IMAP Logs:"

							               echo "=========================="

							               cat imap_errors.txt

							               echo

							               echo "=========================="



										   header_summary=$( cat log/mss.trace | grep -i "CassStore::getHeaderSummaryByMsgUUID" | wc -l )

										   header_summary=` echo $header_summary | tr -d " "`

										   if [ "$header_summary" == "0" ]

										   then

										       echo " HeaderSummary is not read "

										   else

										       echo " ERROR :HeaderSummary is read "

										   fi



										   start_field=$( cat $INTERMAIL/log/mss.trace | grep -i "start CassStore::readMessageBytes" | wc -l )

										   start_field=` echo $start_field | tr -d " "`

										 

										   stop_field=$( cat $INTERMAIL/log/mss.trace | grep -i "stop CassStore::readMessageBytes" | wc -l )

										   stop_field=` echo $stop_field | tr -d " "`
                                          echo " start === "$start_field
							              echo " stop === "$stop_field
										  

										   if [ "$start_field" == "$start_field1" ] && [ "$stop_field" == "$stop_field1" ]

										   then

										       echo " Start/Stop traces present  "

											   summary "IMAP fetch n body[header] should not read header summary"  0

										   else

										       echo " ERROR : Start/Stop traces not present "

											   summary "IMAP fetch n body[header] should not read header summary"  1  "ITS-1324873"

										   fi



										   echo

										   echo "XX-----Testing fetch n body[header] is completed -----XX"

                                           echo



										   ### Testing fetch n envelope

                                           echo "#################################################################"

                                           echo "| VERIFY IMAP FETCH N ENVELOPE DOES NOT READ HEADER SUMMARY|"

                                           echo "#################################################################"

                                           echo		

									       

										   start_field=$( cat $INTERMAIL/log/mss.trace | grep -i "start CassStore::getSummaryByMsgUUID" | wc -l )

										   start_field=` echo $start_field | tr -d " "`

										   start_field1=$(($start_field+6))

										   		

										   stop_field=$( cat $INTERMAIL/log/mss.trace | grep -i " END CassStore::getSummaryByMsgUUID" | wc -l )

										   stop_field=` echo $stop_field | tr -d " "`

										   stop_field1=$(($stop_field+6))

										  	

										   ## IMAP OPERATIONS START FROM HERE

							               echo "=========================="

							               echo "PERFORMING IMAP OPERATIONS"

							               echo "=========================="

							               echo

							               echo "Connecting to $IMAPHost on Port $IMAPPort";

							               echo "Please wait ... "

							               echo



                                           echo >mail.txt



							               exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



										   echo -en "a login $user25@openwave.com $user25\r\n" >&3

							               echo -en "a select INBOX\r\n" >&3

							               echo -en "a fetch 1:* envelope\r\n" >&3

										   echo -en "a logout\r\n" >&3

							               

							               cat <&3 >> mail.txt

										   



										   ### Check for errors in the imap logs

							               cat $INTERMAIL/log/imapserv.log| egrep -i "erro;|urgt;|fatl;" > imap_errors.txt

							               echo "Errors logged in IMAP Logs:"

							               echo "=========================="

							               cat imap_errors.txt

							               echo

							               echo "=========================="



										   header_summary=$( cat log/mss.trace | grep -i "CassStore::getHeaderSummaryByMsgUUID" | wc -l )

										   header_summary=` echo $header_summary | tr -d " "`

										   if [ "$header_summary" == "0" ]

										   then

										       echo " HeaderSummary is not read "

										   else

										       echo " ERROR :HeaderSummary is read "

										   fi



										   start_field=$( cat $INTERMAIL/log/mss.trace | grep -i "start CassStore::getSummaryByMsgUUID" | wc -l )

										   start_field=` echo $start_field | tr -d " "`

										   

										   stop_field=$( cat $INTERMAIL/log/mss.trace | grep -i " END CassStore::getSummaryByMsgUUID" | wc -l )

										   stop_field=` echo $stop_field | tr -d " "`

                                          

									       if [ "$start_field" == "$start_field1" ] && [ "$stop_field" == "$stop_field1" ]

										   then

										       echo " Start/Stop traces present  "

											   summary "IMAP fetch n envelope should not read header summary"  0

										   else

										       echo " ERROR : Start/Stop traces not present "

											   summary "IMAP fetch n envelope should not read header summary"  1  "ITS-1324873"

										   fi



										   echo

										   echo "XX-----Testing fetch n envelope is completed -----XX"

                                           echo



										   ### Testing fetch n body[]<n1.n2>

                                           echo "#################################################################"

                                           echo "| VERIFY IMAP FETCH N BODY[]<N1.N2> DOES NOT READ HEADER SUMMARY|"

                                           echo "#################################################################"

                                           echo		

										   start_field1=$(($start_field+6))

										   stop_field1=$(($stop_field+6))

										   ## IMAP OPERATIONS START FROM HERE

							               echo "=========================="

							               echo "PERFORMING IMAP OPERATIONS"

							               echo "=========================="

							               echo

							               echo "Connecting to $IMAPHost on Port $IMAPPort";

							               echo "Please wait ... "

							               echo



                                           echo >mail.txt



							               exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



										   echo -en "a login $user25@openwave.com $user25\r\n" >&3

							               echo -en "a select INBOX\r\n" >&3

							               echo -en "a fetch 1:* body[]<0.2048>\r\n" >&3

										   echo -en "a logout\r\n" >&3

							              

							               cat <&3 >> mail.txt

										   



										   ### Check for errors in the imap logs

							               cat $INTERMAIL/log/imapserv.log| egrep -i "erro;|urgt;|fatl;" > imap_errors.txt

							               echo "Errors logged in IMAP Logs:"

							               echo "=========================="

							               cat imap_errors.txt

							               echo

							               echo "=========================="

				   

										   header_summary=$( cat log/mss.trace | grep -i "CassStore::getHeaderSummaryByMsgUUID" | wc -l )

										   header_summary=` echo $header_summary | tr -d " "`

										   if [ "$header_summary" == "0" ]

										   then

										       echo " HeaderSummary is not read "

										   else

										       echo " ERROR :HeaderSummary is read "

										   fi

	   

										   start_field=$( cat $INTERMAIL/log/mss.trace | grep -i "start CassStore::readMessageBytes" | wc -l )

										   start_field=` echo $start_field | tr -d " "`

										  

										   stop_field=$( cat $INTERMAIL/log/mss.trace | grep -i "stop CassStore::readMessageBytes" | wc -l )

										   stop_field=` echo $stop_field | tr -d " "`

								           

										   if [ "$start_field" == "$start_field1" ] && [ "$stop_field" == "$stop_field1" ]

										   then

										       echo " Start/Stop traces present  "

											   summary "IMAP fetch n body[]<n1.n2> should not read header summary"  0

										   else

										       echo " ERROR : Start/Stop traces not present "

											   summary "IMAP fetch n body[]<n1.n2> should not read header summary"  1  "ITS-1324873"

										   fi



										   echo

										   echo "XX-----Testing fetch n body[]<n1.n2> is completed -----XX"

                                           echo



										   ### Testing fetch n body.peek[]

                                           echo "#################################################################"

                                           echo "| VERIFY IMAP FETCH N BODY.PEEK[] DOES NOT READ HEADER SUMMARY|"

                                           echo "#################################################################"

                                           echo		

										   start_field1=$(($start_field+3))

										   stop_field1=$(($stop_field+3))

    									   ## IMAP OPERATIONS START FROM HERE

							               echo "=========================="

							               echo "PERFORMING IMAP OPERATIONS"

							               echo "=========================="

							               echo

							               echo "Connecting to $IMAPHost on Port $IMAPPort";

							               echo "Please wait ... "

							               echo

                                           echo >mail.txt



							               exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



										   echo -en "a login $user25@openwave.com $user25\r\n" >&3

							               echo -en "a select INBOX\r\n" >&3

							               echo -en "a fetch 1:* body.peek[]\r\n" >&3

										   echo -en "a logout\r\n" >&3

							               

							               cat <&3 >> mail.txt

										   

										   

										   ### Check for errors in the imap logs

							               cat $INTERMAIL/log/imapserv.log| egrep -i "erro;|urgt;|fatl;" > imap_errors.txt

							               echo "Errors logged in IMAP Logs:"

							               echo "=========================="

							               cat imap_errors.txt

							               echo

							               echo "=========================="

										   

										   header_summary=$( cat log/mss.trace | grep -i "CassStore::getHeaderSummaryByMsgUUID" | wc -l )

										   header_summary=` echo $header_summary | tr -d " "`

										   if [ "$header_summary" == "0" ]

										   then

										       echo " HeaderSummary is not read "

										   else

										       echo " ERROR :HeaderSummary is read "

										   fi



     									   start_field=$( cat $INTERMAIL/log/mss.trace | grep -i "start CassStore::readMessageBytes" | wc -l )

										   start_field=` echo $start_field | tr -d " "`

										  

										   stop_field=$( cat $INTERMAIL/log/mss.trace | grep -i "stop CassStore::readMessageBytes" | wc -l )

										   stop_field=` echo $stop_field | tr -d " "`

										  

										   if [ "$start_field" == "$start_field1" ] && [ "$stop_field" == "$stop_field1" ]

										   then

										       echo " Start/Stop traces present  "

											   summary "IMAP fetch n body.peek[] should not read header summary"  0

										   else

										       echo " ERROR : Start/Stop traces not present "

											   summary "IMAP fetch n body.peek[] should not read header summary"  1  "ITS-1324873"

										   fi



										   echo

										   echo "XX-----Testing fetch n body.peek[] is completed -----XX"

                                           echo



				                       else

									       echo

										   echo " ERROR : Mails were not delivered "

										   summary "IMAP fetch should not read header summary" 7 

									   fi

   								  

                                   imconfcontrol -install -key /*/common/traceOutputLevel="cluster=3" >keys_set.txt

							       updtd_key_value=$(imconfget traceOutputLevel )	

							       updtd_key_value=`echo $updtd_key_value | tr -d " "`

								   if [ "$updtd_key_value" == "cluster=3" ]

							       then

								       echo " Key is set back to original value "

								   else

								       echo " ERROR : Key is not set back to its original value "

								   fi

							   else

                                   echo " ERROR : Traceoutput level key is not set "

								   summary "IMAP fetch should not read header summary" 7 

                               fi	



                               echo	

                               echo "XX-----Testing ITS 1324873 is completed -----XX"

                               echo	
							   

}

function fep_unsymmetric() {

                     start_time_tc fep_unsymmetric_tc

						   echo "#########################################################"

                           echo "|  VERIFICATION OF FEP UNSYMMTERIC HASH RANGE SCENARIO |"

                           echo "#########################################################"

                           echo							


                           deleteMessages $user4 $user4 $MBXID_Sanity4
       					   imboxstats $MBXID_Sanity4 > boxstats.txt

				            msgs_user=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				            msgs_user=`echo $msgs_user | tr -d " "`
                           
				            
							   imconfcontrol -install -key /$Site-$cluster/common/clusterHashMap="$Cluster=$FirstMSS:0-533,$SecondMSS:534-999" >>trash.txt

							   imconfcontrol -install -key /$Site/common/clusterHashMap="$Cluster=$FirstMSS:0-533,$SecondMSS:534-999" >>trash.txt



							   updtd_key_value=$(imconfget clusterHashMap )	

							   updtd_key_value=`echo $updtd_key_value | tr -d " "`



							   if [ "$updtd_key_value" == "$Cluster=$FirstMSS:0-533,$SecondMSS:534-999" ]

							   then

							       echo " FEP Hashing Range is set as : $Cluster=$FirstMSS:0-533,$SecondMSS:534-999"

								   ## Sending message to user4

							       echo "-------Sending Message Starts--------"

							       echo "Connecting to $MTAHost on Port $SMTPPort";

							       echo "Please wait ... "

							       echo

                                   echo > mail.txt



							       exec 3<>/dev/tcp/$MTAHost/$SMTPPort



							       echo -en "MAIL FROM:$MAILFROM\r\n" >&3

							       echo -en "RCPT TO:$user4\r\n" >&3

                                   echo -en "DATA\r\n" >&3

							       echo -en "Subject: $SUBJECT\r\n\r\n" >&3

							       echo -en "$DATA\r\n" >&3

							       echo -en ".\r\n" >&3

							       echo -en "QUIT\r\n" >&3

							       cat <&3 >> mail.txt

							       

							       echo "-------Message Sent--------"

							       echo



								   imboxstats $MBXID_Sanity4 > boxstats.txt

				                   msgs_user1=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

				                   msgs_user1=`echo $msgs_user1 | tr -d " "`

				                   echo "============================================"

				                   if [ "$msgs_user1" == "1" ]

				                   then

					                   echo $msgs_user1" Mail is delivered successfully."

									   msgid=$(grep "Message received:" mail.txt| cut -d ":" -f2 | sed -e 's/^\s*//' -e 's/\s*$//' | tail -1 | cut -d "[" -f1) 

							           msgid=` echo $msgid | tr -d " "`

							           mss_used_smtp=$(cat $INTERMAIL/log/mta.log| egrep -i delivered | grep -i $user4 | grep -i $msgid | cut -d ":" -f4 | cut -d "=" -f2 | sort -u)

							           mss_used_smtp=`echo $mss_used_smtp | tr -d " "`

							           echo " Mail was delivered using OWNER MSS : "$mss_used_smtp

									   summary "Testing unsymmteric FEP hashing range using SMTP" 0

							           echo

								   

								       ## Accessing the mail through POP									

								       echo "Connecting to $POPHost on Port $POPPort";

						               echo "Please wait ... "

						               echo



						               exec 3<>/dev/tcp/$POPHost/$POPPort



						               echo -en "user $user4\r\n" >&3

						               echo -en "pass $user4\r\n" >&3

						               echo -en "retr 1\r\n" >&3

								       echo -en "quit\r\n" >&3

						               echo

						            



                    			       ### Check for errors in pop logs

						               cat $INTERMAIL/log/popserv.log| egrep -i "erro;|urgt;|fatl;" > pop_errors.txt

						               echo "Errors logged in POP Logs:"

						               echo "=========================="

						               cat pop_errors.txt

						               echo

						               echo "=========================="



						               ### Check for mss used for pop operations

						               mss_used_pop=$(cat $INTERMAIL/log/popserv.log |  grep -i $user4 | cut -d ":" -f3 |grep -i mss |cut -d "=" -f2 | sort -u)

						               echo

						               echo "MSS used for pop operations was on host: "$mss_used_pop

							           if [ "$mss_used_pop" == "$mss_used_smtp" ]

    							       then

									       echo

									       echo " Mails were accessed using the owner mss ."

										   summary "Testing unsymmteric FEP hashing range by POP" 0

								       else

									       echo

									       echo "ERROR : Mails were accessed using Surrogate MSS  "

										   summary "Testing unsymmteric FEP hashing range by POP" 1

								       fi



								       ## IMAP OPERATIONS START FROM HERE

							           echo "=========================="

							           echo "PERFORMING IMAP OPERATIONS"

							           echo "=========================="

							           echo

							           echo "Connecting to $IMAPHost on Port $IMAPPort";

							           echo "Please wait ... "

							           echo



							           exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



    							       echo -en "a login $user4@openwave.com $user4\r\n" >&3

        				               echo -en "a select INBOX\r\n" >&3

							           echo -en "a fetch 1:1 rfc822\r\n" >&3	

								       echo -en "a logout\r\n" >&3

							           



                                       ### Check for errors in the imap logs

							           cat $INTERMAIL/log/imapserv.log| egrep -i "erro;|urgt;|fatl;" > imap_errors.txt

							           echo "Errors logged in IMAP Logs:"

							           echo "=========================="

							           cat imap_errors.txt

							           echo

							           echo "=========================="



								       ### Check for mss used for imap operations

							           mss_used_imap=$(cat $INTERMAIL/log/imapserv.log |  grep -i mss | grep -i retr | grep -i $user4 | cut -d ":" -f4 | cut -d "=" -f2 | tail -1)

								       mss_used_imap=` echo $mss_used_imap | tr -d " "`

							           echo

							           echo "MSS used for imap operations was on host: "$mss_used_imap



								       if [ "$mss_used_imap" == "$mss_used_smtp" ]

								       then

									       echo

									       echo " IMAP operations are performed using owner MSS only ...."

										   summary "Testing unsymmteric FEP hashing range by IMAP" 0

								       else

									       echo

									       echo " ERROR: IMAP operations are redirected ....  "

										   summary "Testing unsymmteric FEP hashing range by IMAP" 1

								       fi

									   

     							   else

								       echo " ERROR : Mail was not delivered "

									   summary "Testing unsymmteric FEP hashing range using SMTP " 1

                           		   fi					   

							   else

							       echo " ERROR : FEP Hashing Range is not set correctly "

								   summary "Testing unsymmteric FEP hashing range " 3

                               fi



    						   imconfcontrol -install -key /$Site-$Cluster/common/clusterHashMap="$Cluster=$FirstMSS:0-499,$SecondMSS:500-999" > temp.txt

							   imconfcontrol -install -key /$Site/common/clusterHashMap="$Cluster=$FirstMSS:0-499,$SecondMSS:500-999" > temp.txt



							   updtd_key_value=$(imconfget clusterHashMap )	

							   updtd_key_value=`echo $updtd_key_value | tr -d " "`

							   if [ "$updtd_key_value" == "$Cluster=$FirstMSS:0-499,$SecondMSS:500-999" ]

							   then

							       echo

							       echo " FEP Hashing Range is set back to : $Cluster=$FirstMSS:0-499,$SecondMSS:500-999"

							   else

							       echo

							       echo " FEP Hashing Range is not set back correctly "

                               fi


                            echo						

                            echo "XX-----Testing of FEP unsymmetric hash range utility is completed-----XX"

                           

}

function owner_mss_down_imap_pop_mta() {

                              start_time_tc  owner_mss_down_imap_pop_mta_tc

							  echo "###########################################################################"

                              echo "| WHEN THE OWNER MSS IS DOWN , SMTP/POP/IMAP OPERATIONS THROUGH SAME MSS |"

                              echo "###########################################################################"

                              echo							

                              deleteMessages $user3 $user3 $MBXID_Sanity3
                              imboxstats $MBXID_Sanity3 > boxstats_Sanity1.txt

				              msgs=$(grep "Total Messages Stored"  boxstats_Sanity1.txt | cut -d " " -f6)

				              msgs=`echo $msgs | tr -d " "`
                              				              
    							   

						      echo "-------Sending Message Starts--------"

							   echo "Connecting to $MTAHost on Port $SMTPPort";

							   echo "Please wait ... "

							   echo

                               echo > mail.txt

							   exec 3<>/dev/tcp/$MTAHost/$SMTPPort



							   echo -en "MAIL FROM:$user6\r\n" >&3

							   echo -en "RCPT TO:$user3\r\n" >&3

							   echo -en "DATA\r\n" >&3

							   echo -en "$DATA\r\n" >&3

							   echo -en ".\r\n" >&3

							   echo -en "QUIT\r\n" >&3

							   cat <&3 >> mail.txt

							   echo

							   echo "-------Message Sent--------"

							   echo

							   

							   imboxstats $MBXID_Sanity3 > boxstats_Sanity1.txt

				               msgs1=$(grep "Total Messages Stored"  boxstats_Sanity1.txt | cut -d " " -f6)

				               msgs1=`echo $msgs1 | tr -d " "`

				               if [ "$msgs1" == "1" ]

				               then

				                   echo " Mail delivered "

								   msgid=$(grep "Message received:" mail.txt| cut -d ":" -f2 | sed -e 's/^\s*//' -e 's/\s*$//' | tail -1 | cut -d "[" -f1) 

							       msgid=` echo $msgid | tr -d " "`

							   else

							       echo " ERROR : Mail was not delivered "

								   summary "When owner is down , smtp/pop/imap operations through same mss " 3

								   return 0

                               fi

							   ownerMSS=$(cat $INTERMAIL/log/mta.log| egrep -i delivered | grep -i $user3 | grep -i $msgid | cut -d ":" -f4 | cut -d "=" -f2 | sort -u | head -1)

							   ownerMSS=`echo $ownerMSS | tr -d " "`

							   echo "Owner MSS of the mailbox used is: "$ownerMSS

							   echo

						    

							   ## Bringing down owner mss

							   echo "Please wait while we shutdown owner MSS... "

							   imctrl $ownerMSS kill mss.1 >>trash.txt

							   imservping -h $ownerMSS mss > ping_resp.txt

							   resp_count=$(cat ping_resp.txt | grep -i "unreachable" | wc -l)

							   resp_count=`echo $resp_count | tr -d " "`

							   if [ "$resp_count" == "2" ]

							   then

								    echo

								    echo "MSS on "$ownerMSS" is successfully shutdown"

								    sleep 20

									echo "Processing further.... "

									echo 



									## Checking mail delivery

									echo "-------Mail Delivery Starts--------"

									echo

									echo "Connecting to $MTAHost on Port $SMTPPort";

									echo "Please wait ... "

									echo

                                    echo >mail.txt

									exec 3<>/dev/tcp/$MTAHost/$SMTPPort



									echo -en "MAIL FROM:$MAILFROM\r\n" >&3

									echo -en "RCPT TO:$user3\r\n" >&3

									echo -en "DATA\r\n" >&3

									echo -en "Subject: $SUBJECT\r\n\r\n" >&3

									echo -en "$DATA\r\n" >&3

									echo -en ".\r\n" >&3

									echo -en "QUIT\r\n" >&3

									cat <&3 >>mail.txt

									echo

									echo "-------Mail Delivery Ends--------"

									echo


                                  
    								## Check if mails were delivered successfully or not

									imboxstats $MBXID_Sanity3 > boxstats_Sanity3.txt

									msgs2=$(grep "Total Messages Stored"  boxstats_Sanity3.txt | cut -d " " -f6)

									msgs2=`echo $msgs2 | tr -d " "`

									if [ "$msgs2" == "2" ]

									then

									    echo $msgs" Mails were delivered successfully."

									    echo



								     	## Check for mta errors in logs 

									    cat $INTERMAIL/log/mta.log| egrep -i "erro;|urgt;|fatl;" > mta_errors.txt

									    echo "Errors logged in MTA Logs:"

									    echo "=========================="

									    cat mta_errors.txt

									    echo

									    echo "=========================="



     							        ### Check for mss used for delivery

										 msgid=$(grep "Message received:" mail.txt| cut -d ":" -f2 | sed -e 's/^\s*//' -e 's/\s*$//' | tail -1 | cut -d "[" -f1) 

							             msgid=` echo $msgid | tr -d " "`

	   						            mss_used_smtp=$(cat $INTERMAIL/log/mta.log| egrep -i delivered | grep -i $msgid | grep -i $user3 | grep -i $MAILFROM | cut -d ":" -f4 | cut -d "=" -f2 | tail -1 | sort -u)

									    mss_used_smtp=`echo $mss_used_smtp | tr -d " "`

										if [ "$mss_used_smtp" == "$ownerMSS" ]

										then

											echo

											echo "ERROR: Mails were delivered using the owner mss only."

											echo "ERROR: Please check if redirection is working or not."

											summary "When owner is down , smtp operation through same surrogate mss " 1

										else

											echo

											echo "Mails were delivered successfully using Surrogate MSS  "$mss_used_smtp

											summary "When owner is down , smtp operation through same surrogate mss " 0

									    fi



                                       ## Accessing the mail through POP									

									    echo

						                echo "Connecting to $POPHost on Port $POPPort";

						                echo "Please wait ... "

						                echo



    					                exec 3<>/dev/tcp/$POPHost/$POPPort



						                echo -en "user $user3\r\n" >&3

						                echo -en "pass $user3\r\n" >&3

						                echo -en "retr 1\r\n" >&3

									    echo -en "quit\r\n" >&3

						                

										### Check for errors in pop logs

						                cat $INTERMAIL/log/popserv.log| egrep -i "erro;|urgt;|fatl;" > pop_errors.txt

						                echo "Errors logged in POP Logs:"

						                echo "=========================="

						                cat pop_errors.txt

						                echo

						                echo "=========================="





						               ### Check for mss used for pop operations

						               mss_used_pop=$(cat $INTERMAIL/log/popserv.log |  grep -i $user3 | cut -d ":" -f3 |grep -i mss |cut -d "=" -f2 | sort -u| tail -1 | head -1)

						               echo

						               echo "MSS used for pop operations was on host: "$mss_used_pop



									   if [ "$mss_used_pop" == "$mss_used_smtp" ]

									   then

										   echo

										   echo " Same MSS used for POP operations ...."

										   summary "When owner is down , pop operation through same surrogate mss " 0

									   else

										   echo

										   echo " ERROR : Different MSS used for POP operations ....  "

										   summary "When owner is down , pop operation through same surrogate mss " 1

									   fi



    								   ## IMAP OPERATIONS START FROM HERE

							           echo

							           echo "=========================="

							           echo "PERFORMING IMAP OPERATIONS"

							           echo "=========================="

							           echo

							           echo "Connecting to $IMAPHost on Port $IMAPPort";

							           echo "Please wait ... "

							           echo



    						           exec 3<>/dev/tcp/$IMAPHost/$IMAPPort



							           echo -en "a login $user3@openwave.com $user3\r\n" >&3

							           echo -en "a select INBOX\r\n" >&3

							           echo -en "a fetch 2:2 rfc822\r\n" >&3	

									   echo -en "a logout\r\n" >&3

							           echo



							           ### Check for errors in the imap logs

							           cat $INTERMAIL/log/imapserv.log| egrep -i "erro;|urgt;|fatl;" > imap_errors.txt

							           echo "Errors logged in IMAP Logs:"

							           echo "=========================="

							           cat imap_errors.txt

							           echo

							           echo "=========================="

							          sleep 1

							           ### Check for mss used for imap operations

							           mss_used_imap=$(cat $INTERMAIL/log/imapserv.log |  grep -i mss | grep -i retr | grep -i $user3 | cut -d ":" -f4 | cut -d "=" -f2 | tail -1 | head -1)

							           echo

							           echo "MSS used for imap operations was on host: "$mss_used_imap

									   if [ "$mss_used_imap" == "$mss_used_smtp" ]

									   then

										   echo

										   echo " Same MSS used for IMAP operations ...."

										   summary "When owner is down , imap operation through same surrogate mss " 0

									   else

										   echo

										   echo " ERROR : Different MSS used for IMAP operations ....  "

										   summary "When owner is down , imap operation through same surrogate mss " 1

									   fi

                        

								   else

									   echo " mail was not delivered "

									   summary "When owner is down , smtp/pop/imap operations through same mss " 3

								       return 0

								   fi 

                               else

                                   echo " Owner MSS cannot be stoped ..."

                                   summary "When owner is down , smtp/pop/imap operations through same mss " 3

								   return 0								   

                               fi									



						       ### Starting Owner MSS 

     						   echo

    						   echo "Please wait owner mss is starting up again..."

    						   imctrl $ownerMSS start mss.1 >>trash.txt

  							   imservping -h $ownerMSS mss > ping_resp.txt

							   resp_count=$(cat ping_resp.txt | grep -i "respond" | wc -l)

       						   resp_count=`echo $resp_count | tr -d " "`

 							   if [ "$resp_count" == "1" ]

							   then

							        echo

									echo "MSS on "$ownerMSS" is successfully started"

		     				   else

									echo

									echo "ERROR: OWNER MSS on host "$ownerMSS" could not be started, please start manually."

        					   fi



							   echo						

                               echo "XX-----Testing when the owner MSS is down , SMTP/POP/IMAP connected through same surrogate is completed -----XX"

                               

}

function mailbox_creation_immsgtrace2(){

start_time_tc mailbox_creation_immsgtrace2_tc

 ### Testing new immsgtrace utility for creation of mailbox through surrogate

                               echo "###################################################################"

                               echo "| VERIFY IMMSGTRACE UTILTY FOR MAILBOX CREATION THROUGH SURROGATE |"

                               echo "###################################################################"

                               echo		

							   

							   immsgtrace -to $user27 -size 5 >output.txt

            
                               sleep 7
							   created=$(cat $INTERMAIL/log/mta.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity27 | wc -l)

							   created=`echo $created | tr -d " " `

									

									if [ "$created" == "5" ]

									then

									    echo " Mailbox created "

										echo " Utility is working fine "

										summary "Mailbox creation through immsgtrace using surrogate" 0

									else

									    echo " ERROR : Mailbox not created "

										echo " ERROR : Utility is not working fine "

										summary "Mailbox creation through immsgtrace using surrogate" 1

									fi

									

 

							   echo						

                               echo "XX-----Testing immsgtrace utility for creation of mailbox through surrogate is completed -----XX"

                               echo

							   

									### Starting Owner MSS 

							        echo "Please wait owner mss is starting up again..."

							        imctrl $ownerMSS start mss.1

							        imservping -h $ownerMSS mss > ping_resp.txt

							        resp_count=$(cat ping_resp.txt | grep -i "respond" | wc -l)

							        resp_count=`echo $resp_count | tr -d " "`

							        if [ "$resp_count" == "1" ]

							        then

								        echo "MSS on "$ownerMSS" is successfully started"

								        echo

							        else

								    	echo "ERROR: OWNER MSS on host "$ownerMSS" could not be started, please start manually."

								        echo

							        fi

                               

}

function immsgmassmail_utility() {
                               start_time_tc immsgmassmail_utility_tc
	                           ## ITS 1310611
							   ### Testing new immsgmassmail utility for local users 
                               echo "#################################################"
                               echo "| VERIFY IMMSGMASSMAIL UTILTY FOR LOCAL USERS  |"
                               echo "#################################################"
                               echo			
                            
							   
							   imboxstats  $MBXID_Sanity2 > boxstats.txt
							   msg2=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)
							   msg2=`echo $msg2 | tr -d " "`
							   msg2=$(($msg2+1))
							   imboxstats  $MBXID_Sanity3 > boxstats.txt
							   msg3=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)
							   msg3=`echo $msg3 | tr -d " "`
							        msg3=$(($msg3+1))
							   imboxstats  $MBXID_Sanity4 > boxstats.txt
							   msg4=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)
							   msg4=`echo $msg4 | tr -d " "`
							        msg4=$(($msg4+1))
							   
									        echo "$user2@openwave.com" > recepient.txt
											echo "$user3@openwave.com" >> recepient.txt
											echo "$user4@openwave.com" >> recepient.txt
											
											
											
											echo "From: $user1@openwave.com" > message.txt
											echo "To: All" >> message.txt
											echo "Subject: Mass mail" >> message.txt
											echo  >> message.txt
											echo "This is mail to test immsgmassmail utility " >> message.txt
											
											
											
											immsgmassmail -F message.txt -a recepient.txt 
                                            
							                imboxstats  $MBXID_Sanity2 > boxstats.txt
							                msg21=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)
							                msg21=`echo $msg21 | tr -d " "`
							   
							                imboxstats  $MBXID_Sanity3 > boxstats.txt
							                msg31=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)
							                msg31=`echo $msg31 | tr -d " "`
							   
							                imboxstats  $MBXID_Sanity4 > boxstats.txt
							                msg41=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)
							                msg41=`echo $msg41 | tr -d " "`
											
											if [ "$msg21" == "$msg2" ] && [ "$msg31" == "$msg3" ] && [ "$msg41" == "$msg4" ]
											then
											     echo "Mail delivered successfully to all the recepients "
												 echo "immsgmassmail utility is working fine "
												 summary "Immsgmassmail " 0
											else
											     echo "ERROR : Mail were not delivered successfully to all the recepients "
												 echo "ERROR : immsgmassmail utility is not working correctly "
												 summary "Immsgmassmail " 1 " ITS 1310611"
											fi
									   
							   
							   echo						
                               echo "XX-----Testing immsgmassmail utility for local users is completed -----XX"
                               echo
}

function mailbox_creation_immsgmassmail(){

                               ## ITS 1310611

							   ### Testing new immsgmassmail utility for local users 

                               echo "#################################################"

                               echo "| VERIFY IMMSGMASSMAIL UTILTY FOR LOCAL USERS  |"

                               echo "#################################################"

                               echo			

                            

							   imboxdelete $MSSHost $MBXID_Sanity2

							   imboxdelete $MSSHost $MBXID_Sanity3

							   imboxdelete $MSSHost $MBXID_Sanity4

							   

							   imboxstats  $MBXID_Sanity2 > chk_mailbox.txt

							   msg2=$(cat chk_mailbox.txt | grep -i failed | wc -l)

							   msg2=`echo $msg2 | tr -d " "`

							   

							   imboxstats  $MBXID_Sanity3 > chk_mailbox.txt

							   msg3=$(cat chk_mailbox.txt | grep -i failed | wc -l)

							   msg3=`echo $msg3 | tr -d " "`

							   

							   imboxstats  $MBXID_Sanity4 > chk_mailbox.txt

							   msg4=$(cat chk_mailbox.txt | grep -i failed | wc -l)

							   msg4=`echo $msg4 | tr -d " "`

							   

							   if [ "$msg2" == "1" ]

							   then

							       if [ "$msg3" == "1" ]

								   then

								       if [ "$msg4" == "1" ]

									   then

									        echo "$user2@openwave.com" > recepient.txt

											echo "$user3@openwave.com" >> recepient.txt

											echo "$user4@openwave.com" >> recepient.txt

																					

											

											echo "From: $user1@openwave.com" > message.txt

											echo "To: All" >> message.txt

											echo "Subject: Mass mail" >> message.txt

											echo  >> message.txt

											echo "This is mail to test immsgmassmail utility " >> message.txt

											

											

											

											immsgmassmail -F message.txt -a recepient.txt 

                                            #sleep 2

							                #created=$(cat log/immsgmassmail.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity2 | wc -l)

									        #created=`echo $created | tr -d " " `

									       

										    #created1=$(cat log/immsgmassmail.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity3 | wc -l)

									        #created1=`echo $created1 | tr -d " " `

											

											#created2=$(cat log/immsgmassmail.log | grep -i "Note;MsMailboxCreated" | grep $MBXID_Sanity4 | wc -l)

									        #created2=`echo $created2 | tr -d " " `

											imboxstats  $MBXID_Sanity2 > boxstats.txt

							                msg21=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

							                msg21=`echo $msg21 | tr -d " "`

							   

							                imboxstats  $MBXID_Sanity3 > boxstats.txt

							                msg31=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

							                msg31=`echo $msg31 | tr -d " "`

							   

							                imboxstats  $MBXID_Sanity4 > boxstats.txt

							                msg41=$(grep "Total Messages Stored"  boxstats.txt | cut -d " " -f6)

							                msg41=`echo $msg41 | tr -d " "`

											

									        #if [ "$created" == "1" ] && [ "$created1" == "1" ] && [ "$created2" == "1" ] 

											if [ "$msg21" == "1" ] && [ "$msg31" == "1" ] && [ "$msg41" == "1" ]

									        then

									             echo " Mailbox created "

										         echo " Utility is working fine "

												 summary "Mailbox creation through immsgmassmail " 0

									        else

									             echo " ERROR : Mailbox not created "

										         echo " Utility is not working fine "

												 summary "Mailbox creation through immsgmassmail " 1

									        fi

									   else

									       echo "ERROR : Mailbox of "$user4" cannot be emptied "

									   fi

								   else

								       echo "ERROR : Mailbox of "$user3" cannot be emptied "

								   fi

							   else

							       echo "ERROR : Mailbox of "$user2" cannot be emptied "

							   fi

							   

							   echo						

                               echo "XX-----Testing immsgmassmail utility for local users is completed -----XX"

                               echo

							   

		                            

}

function imap_check() {
                            # IMAP OPERATIONS START FROM HERE
							echo
							echo "=========================="
							echo "PERFORMING IMAP OPERATIONS"
							echo "=========================="
							echo
							echo "Connecting to $IMAPHost on Port $IMAPPort";
							echo "Please wait ... "
							echo
                            echo > check.txt
							exec 3<>/dev/tcp/$IMAPHost/$IMAPPort

							echo -en "a login $user1@openwave.com $user1\r\n" >&3
							echo -en "a select INBOX\r\n" >&3
							echo -en "a fetch 1 rfc822\r\n" >&3
							echo -en "a fetch 1:3 uid\r\n" >&3
							echo -en "a fetch 1 envelope\r\n" >&3
							echo -en "a logout\r\n" >&3
							echo "+++++++++++++++++++++++++"
							cat <&3 >> check.txt
							
							echo "+++++++++++++++++++++++++"
							echo

                            first=$(cat check.txt | grep "* 1 FETCH" | grep -i "UID" | cut -d " " -f5) 
							first=${first:0:4}
                            echo " first uid == "$first
							
							second=$(cat check.txt | grep "* 2 FETCH" | grep -i "UID" | cut -d " " -f5)
                            second=${second:0:4}							
							echo " second uid == "$second
							third=$(cat check.txt | grep "* 3 FETCH" | grep -i "UID" | cut -d " " -f5)
                            third=${third:0:4}							
							echo " third uid == "$third
							
							if [ "$first" == "1000" ] && [ "$second" == "1001" ] && [ "$third" == "1002" ]
							then
							   echo " UID's are correct "
							   summary "UID in IMAP is correct and in proper sequence " 0
							else
							   echo " ERROR : UID's are not correct "
							   summary "UID in IMAP is correct and in proper sequence " 1
							fi
							
							from=$(cat check.txt | grep "From:" | cut -d "<" -f2| cut -d ">" -f1 )
							from=` echo $from | tr -d " "`
							echo " from : "$from
							sub=$(cat check.txt | grep "Subject:" | cut -d " " -f2 )
							echo " subject : "$sub
							sub=` echo $sub | tr -d " "`
							to=$(cat check.txt | grep -i "for" | cut -d "<" -f2| cut -d ">" -f1|head -1)
							to=` echo $to | tr -d " "`
							echo " to: "$to
							
							if [ "$from" == "$MAILFROM@openwave.com" ]
							then
							    echo " From field is correct "
								summary "From field in IMAP is correct" 0
							else
							    echo " Error: From field is not correct "
								summary "From field in IMAP is correct" 1
							fi
							
					
							if [ "$sub" == "Sanity" ]
							then
							    echo " Subject field is correct "
								summary "Subject field in IMAP is correct" 0
							else
							    echo " Error: Subject field is not correct "
								summary "Subject field in IMAP is correct" 1
							fi
							
							if [ "$to" == "$user1@openwave.com" ]
							then
							    echo " To field is correct "
								summary "TO field in IMAP is correct" 0
							else
							    echo " Error: To field is not correct "
								summary "TO field in IMAP is correct" 1
							fi
							
							### Check for errors in the imap logs
							cat $INTERMAIL/log/imapserv.log| egrep -i "erro;|urgt;|fatl;" > imap_errors.txt
							echo "Errors logged in IMAP Logs:"
							echo "=========================="
							cat imap_errors.txt
							echo
							echo "=========================="
}

function immtamassmall_utility() {
                               start_time_tc immtamassmall_utility_tc
                               ## ITS 1325870
							   ### Testing new immtamassmail utility for local users 
                               echo "#################################################"
                               echo "| VERIFY IMMTAMASSMAIL UTILTY FOR LOCAL USERS  |"
                               echo "#################################################"
                               echo			
                            
							   deleteMessages $user2 $user2 $MBXID_Sanity2
							   #immsgdelete $user5 $user5 $MBXID_Sanity5
							   
							   
							   imboxstats  $MBXID_Sanity2 > boxstats122.txt
							   msg2=$(grep "Total Messages Stored"  boxstats122.txt | cut -d " " -f6)
							   msg2=`echo $msg2 | tr -d " "`
							   
							   imboxstats  $MBXID_Sanity5 > boxstats133.txt
							   msg3=$(grep "Total Messages Stored"  boxstats133.txt | cut -d " " -f6)
							   msg3=`echo $msg3 | tr -d " "`
							   
							   if [ "$msg2" == "0" ]
							   then
							       if [ "$msg3" == "0" ]
								   then
								      
									        echo "$user2@openwave.com" > recepient.txt
											echo "$user5@openwave.com" >> recepient.txt
											
											echo "From: $user1@openwave.com" > message.txt
											echo "To: All" >> message.txt
											echo "Subject: Mass mail" >> message.txt
											echo  >> message.txt
											echo "This is mail to test immtamassmail utility " >> message.txt
											
											

											immtamassmail recepient.txt message.txt

							                imboxstats  $MBXID_Sanity2 > boxstats122.txt
							                msg2=$(grep "Total Messages Stored"  boxstats122.txt | cut -d " " -f6)
							                msg2=`echo $msg2 | tr -d " "`
							   
							                imboxstats  $MBXID_Sanity5 > boxstats133.txt
							                msg3=$(grep "Total Messages Stored"  boxstats133.txt | cut -d " " -f6)
							                msg3=`echo $msg3 | tr -d " "`
							   
							               											
											if [ "$msg2" == "1" ] && [ "$msg3" == "1" ] 
											then
											     echo "Mail delivered successfully to all the recepients "
												 echo "immtamassmail utility is working fine "
												 summary "Immtamassmail " 0
											else
											     echo "ERROR : Mail were not delivered successfully to all the recepients "
												 echo "ERROR : immtamassmail utility is not working correctly "
												 summary "Immtamassmail " 1 "ITS-1325870"
											fi
									   
								   else
								       echo "ERROR : Mailbox of "$user5" cannot be emptied "
									   summary "Immtamassmail " 1
								   fi
							   else
							       echo "ERROR : Mailbox of "$user2" cannot be emptied "
								   summary "Immtamassmail " 1
							   fi

							   echo						
                               echo "XX-----Testing immtamassmail utility for local users is completed -----XX"
                               echo
							  
}
##############################################All TEST CASES END HERE###################################################################



##############################################MAIN SCRIPT EXECUTION####################################################################
#!/bin/bash
#set -x
if [[ "$1" == "-v" || "$1" == "-version" ]]

then
		#trap for error and ctrl+c and call cleanup fucntion
		trap cleanup SIGHUP SIGINT SIGTERM
		echo > summary.log
		#call function imsanity_version
        imsanity_version
else
		echo > summary.log
		#trap for error and ctrl+c and call cleanup fucntion
		trap cleanup SIGHUP SIGINT SIGTERM
		
		#call function imsanity_version to show the version of tool
		imsanity_version
		
		#call function log deletion to delete log
		#log_delete

		#call fucntion checksetuptype to cehck whether setup is stateless or stateful
		check_setup_type
		
		#call clean up fucntion for setup clean up
		cleanup
		
fi

##############################################MAIN SCRIPT EXECUTION END HERE####################################################################


