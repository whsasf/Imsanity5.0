#!/bin/bash
#Utilities
function Utilities_TestScenarios(){
	
	prints "=========================================" "TC_account_create" 
	prints "Utilities_TestScenarios" "TC_account_create" 
		
	TC_account_create
	prints "=============END OF TC_account_create =================" "TC_account_create" 
	
	prints "=========================================" "TC_account_delete" 
	prints "Utilities_TestScenarios" "TC_account_delete" 
		
	TC_account_delete
	prints "=============END OF TC_account_delete =================" "TC_account_delete" 
	
	prints "=========================================" "TC_imboxstats" 
	prints "Utilities_TestScenarios" "TC_imboxstats" 
	
	TC_imboxstats
	prints "=============END OF TC_imboxstats =================" "TC_imboxstats"
	
	prints "=========================================" "TC_immsgdelete" 
	prints "Utilities_TestScenarios" "TC_immsgdelete" 
	
	TC_immsgdelete
	prints "=============END OF TC_immsgdelete =================" "TC_immsgdelete"
	
	prints "=========================================" "TC_immssdescms" 
	prints "Utilities_TestScenarios" "TC_immssdescms" 
	
	TC_immssdescms
	prints "=============END OF TC_immssdescms =================" "TC_immssdescms"
	
	prints "=========================================" "TC_imfolderlist" 
	prints "Utilities_TestScenarios" "TC_imfolderlist" 
	
	TC_imfolderlist
	prints "=============END OF TC_imfolderlist =================" "TC_imfolderlist"
	
	prints "=========================================" "TC_immsgdump" 
	prints "Utilities_TestScenarios" "TC_immsgdump" 
	
	TC_immsgdump
	prints "=============END OF TC_immsgdump =================" "TC_immsgdump"
	
	prints "=========================================" "TC_immsgfind_Inbox" 
	prints "Utilities_TestScenarios" "TC_immsgfind_Inbox" 
	
	TC_immsgfind_Inbox
	prints "=============END OF TC_immsgfind_Inbox =================" "TC_immsgfind_Inbox"
	
	prints "=========================================" "TC_immsgfind_Trash" 
	prints "Utilities_TestScenarios" "TC_immsgfind_Trash" 
	
	TC_immsgfind_Trash
	prints "=============END OF TC_immsgfind_Trash =================" "TC_immsgfind_Trash"
		
	prints "=========================================" "TC_immsgtrace" 
	prints "Utilities_TestScenarios" "TC_immsgtrace" 
	
	TC_immsgtrace
	prints "=============END OF TC_immsgtrace =================" "TC_immsgtrace"
	
	#prints "=========================================" "TC_imboxattctrl" 
	#prints "Utilities_TestScenarios" "TC_imboxattctrl" 
	
	#TC_imboxattctrl
	#prints "=============END OF TC_imboxattctrl =================" "TC_imboxattctrl"
	
	#prints "=========================================" "TC_imfilterctrl" 
	#prints "Utilities_TestScenarios" "TC_imfilterctrl" 
	
	#TC_imfilterctrl
	#prints "=============END OF TC_imfilterctrl =================" "TC_imfilterctrl"
	
	prints "=========================================" "TC_imboxtest" 
	prints "Utilities_TestScenarios" "TC_imboxtest" 
	
	TC_imboxtest
	prints "=============END OF TC_imboxtest =================" "TC_imboxtest"
	
	prints "=========================================" "TC_imcheckfq" 
	prints "Utilities_TestScenarios" "TC_imcheckfq" 
	
	TC_imcheckfq
	prints "=============END OF TC_imcheckfq =================" "TC_imcheckfq"
	
	prints "=========================================" "TC_imboxmaint" 
	prints "Utilities_TestScenarios" "TC_imboxmaint" 
	
	TC_imboxmaint
	prints "=============END OF TC_imboxmaint =================" "TC_imboxmaint"
}

function account_create_fn() {
	
	start_time_tc account_create_fn_tc
	user_name=$1
	MBXID_user_name=$(echo $RANDOM)
	MBXID_user_name=`echo $MBXID_user_name | tr -d " "`
	account-create $user_name@openwave.com $user_name default -mboxid $MBXID_user_name &> log_account_create.txt
	count_create_mailbox_success=$(cat log_account_create.txt | grep -i Created |wc -l)
	count_create_mailbox_success1=$(cat log_account_create.txt | grep -i MailboxId | wc -l)
	count_create_mailbox_failure=$(cat log_account_create.txt | grep -i ERROR | wc -l)
	
	if [[ "$count_create_mailbox_success" == "1" && "$count_create_mailbox_success1" == "1" ]] 
	then 
		prints "Mailbox for $user_name with mailbox id $MBXID_user_name is created successfully" "account_create_fn" "2"
		Result="0"
	elif [ "$count_create_mailbox_failure" == "1" ]
	then
		prints "Mailbox for $user_name is not created" "account_create_fn" "1"
		Result="1"
	fi
}

function account_delete_fn() {

	user_name=$1
	account-delete $user_name@openwave.com &> log_account_delete.txt
	count_delete_mailbox_success=$(cat log_account_delete.txt | grep -i Deleted | wc -l)
	count_delete_mailbox_success=`echo $count_delete_mailbox_success | tr -d " "` 
	
	count_delete_mailbox_failure=$(cat log_account_delete.txt | grep -i ERROR | wc -l)
	count_delete_mailbox_failure=`echo $count_delete_mailbox_failure | tr -d " "` 
	
	cat log_account_delete.txt >> debug.log
	if [[ "$count_delete_mailbox_success" == "1" && "$count_delete_mailbox_failure" == "0" ]]
	then
		prints "Mailbox $user_name is deleted successfully" "account_delete_fn" "2"
		Result="0"
	else 
		prints "Mailbox $user_name is not deleted" "account_delete_fn" "1"
		Result="1"
	fi
}

function imboxstats_fn (){

	user_name=$1
	imboxstats $user_name@openwave.com &> log_imboxstats.txt
	
	cat log_imboxstats.txt >> Debug.log 
	imboxstats_result_success=$(cat log_imboxstats.txt | grep -i "Total Messages Stored" |wc -l)
	imboxstats_result_success=`echo $imboxstats_result_success | tr -d " "` 
	
	imboxstats_result_failure=$(cat log_imboxstats.txt | grep -i ERROR | wc -l)
	imboxstats_result_failure=`echo $imboxstats_result_failure | tr -d " "` 
	
	if [[ "$imboxstats_result_success" == "1" && "$imboxstats_result_failure" == "0" ]] 
	then
		prints "imboxstats for user $user_name is successful" "imboxstats_fn" "2"
		Result="0"
	else
		prints "imboxstats for user $user_name is failed" "imboxstats_fn" "1"
		Result="1"
	fi
}

function immsgdelete_utility() {
	
	start_time_tc immsgdelete_utility_tc
	user_name=$1
	option=$2
	
	imboxstats_fn "$user_name" 
	msg_msgdel1=$(cat log_imboxstats.txt | grep -i "Total Messages Stored" | cut -d ":" -f2)
	msg_msgdel1=`echo $msg_msgdel1 | tr -d " "`
	prints "Currently "$msg_msgdel1" messages are stored in the mailbox" "immsgdelete_utility" 
	
	immsgdelete $user_name@openwave.com $option  >&3
	cat <&3 > log_immsgdelete.txt
	cat log_immsgdelete.txt >> debug.log
	
	imboxstats_fn "$user_name"  
	msg_msgdel2=$(cat log_imboxstats.txt | grep -i "Total Messages Stored" | cut -d ":" -f2)
	msg_msgdel2=`echo $msg_msgdel2 | tr -d " "`
	if [ "$msg_msgdel2" == "0" ]
	then
		prints "Currently "$msg_msgdel2" messages are stored in the mailbox" "immsgdelete_utility" "2"
		prints "immsgdelete utility is successful" "immsgdelete_utility" "2"
		Result="0"
	else
		prints "ERROR: Currently "$msg_msgdel2" messages are stored in the mailbox" "immsgdelete_utility" "1"
		prints "ERROR: immsgdelete utility is not successful.Please check Manually." "immsgdelete_utility" "1"
		Result="1"
	fi 
}	

function immssdescms_utility() {
	start_time_tc immssdescms_utility_tc
	user_name=$1	

	imboxstats_fn "$user_name" 
	msg_exists=$(cat log_imboxstats.txt | grep -i "Total Messages Stored" | cut -d ":" -f2)
	msg_exists=`echo $msg_exists | tr -d " "`
	
	year=$(date +%Y)
	month=$(date +%b)
	immssdescms $user_name@openwave.com > log_immssdescms.txt
	msgid=$(cat log_immssdescms.txt | egrep -i "Message ID" | wc -l)
	msgid=`echo $msgid | tr -d " "`
	msartime_yr=$(cat log_immssdescms.txt | egrep -i "MSSArrivalTime" | grep -i "$year" | wc -l)
	msartime_yr=`echo $msartime_yr | tr -d " "`
	msartime_mon=$(cat log_immssdescms.txt | egrep -i "MSSArrivalTime" | grep -i "$month" | wc -l)
	msartime_mon=`echo $msartime_mon | tr -d " "`
	mbxartime_yr=$(cat log_immssdescms.txt | egrep -i "MailboxArrivalTime" | grep -i "$year" | wc -l)
	mbxartime_yr=`echo $mbxartime_yr | tr -d " "`
	mbxartime_mon=$(cat log_immssdescms.txt | egrep -i "MailboxArrivalTime" | grep -i "$month" | wc -l)
	mbxartime_mon=`echo $mbxartime_mon | tr -d " "`
	if [ "$msgid" == "$msg_exists" ]
	then
		if [ "$msartime_yr" == "$msg_exists" ]
		then
			if [ "$msartime_mon" == "$msg_exists" ]
			then
				if [ "$mbxartime_yr" == "$msg_exists" ]
				then
					if [ "$mbxartime_mon" == "$msg_exists" ]
					then
						Result="0"
						prints "imssdescms utility is working fine." "immssdescms_utility" "2"
					else
						Result="1"
						prints "ERROR: mailbox arrival time is not correct" "immssdescms_utility" "1"
					fi
				else
					Result="1"
					prints "ERROR: mailbox arrival time is not correct" "immssdescms_utility" "1"
				fi
			else
				Result="1"
				prints "ERROR: message arrival time is not correct" "immssdescms_utility" "1"
			fi
		else
			Result="1"
			prints "ERROR: message arrival time is not correct" "immssdescms_utility" "1"
		fi
	else
		Result="1"
		prints "ERROR: Number of Messages are not correct" "immssdescms_utility" "1"
	fi
		
}

function imfolderlist_utility() {

	start_time_tc imfolderlist_utility_tc
	user_name=$1
	
	imboxstats_fn "$user_name" 
	msg_exists=$(cat log_imboxstats.txt | grep -i "Total Messages Stored" | cut -d ":" -f2)
	msg_exists=`echo $msg_exists | tr -d " "`
	
	imfolderlist $user_name@openwave.com -folder INBOX > log_folderlist.txt
	msgs_fldr=$(cat log_folderlist.txt | grep -i "Message-Id" | wc -l)
	msgs_fldr=`echo $msgs_fldr | tr -d " "`
	cat log_folderlist.txt >> debug.log
	
	if [ "$msgs_fldr" == "$msg_exists" ]
	then
		prints "imfolderlist utility is working fine." "imfolderlist_utility" "2"
		Result="0"
	else
		prints "ERROR: imfolderlist utility is not showing proper results." "imfolderlist_utility" "1"
		Result="1"
	fi
}

function immsgdump_utility() {

	start_time_tc immsgdump_utility_tc
	user_name=$1
	msg_no=$2
	uidstart="100"
	uidno==$uidstart$msg_no
	
	immsgdump $user_name@openwave.com $msg_no  > log_immsgdump.txt
	check_msgfind=$(cat log_immsgdump.txt | wc -l)
	check_msgfind=`echo $check_msgfind | tr -d " "`
	verify_msgfind=$(cat log_immsgdump.txt | grep -i UID=$uidno | wc -l)
	verify_msgfind=`echo $verify_msgfind | tr -d " "`
	cat log_immsgdump.txt >> debug.log
	if [[ "$check_msgfind" > "1" || "$verify_msgfind" == "1" ]]
	then
		prints "immsgdump utility is working fine" "immsgdump_utility" "2"
		Result="0"
	else
		prints "ERROR: immsgdump utility output is not correct. Please check manually." "immsgdump_utility" "1"
		Result="1"
	fi
}

function immsgfind_utility() {

	start_time_tc immsgfind_utility_tc
	
	user_name=$1	
	mail_send "$user_name" "small" "2"
	immssdescms $user_name@openwave.com > descms_Sanity.txt
	msg_id=$(cat descms_Sanity.txt | grep -i "Message ID" | cut -d " " -f2 | cut -d "=" -f2 | head -1)
	msg_id=`echo $msg_id | tr -d " "`
	
	immsgfind $user_name@openwave.com "$msg_id" "/Inbox" > log_immsgfind.txt
	chk_msg_id=$(cat log_immsgfind.txt | egrep -i "MESSAGE-ID" | wc -l)
	chk_msg_id=`echo $chk_msg_id | tr -d " "`
	cat log_immsgfind.txt >> debug.log
	if [ "$chk_msg_id" == "1" ]
	then
		prints "immsgfind utility is working fine" "immsgfind_utility" "2"
		Result="0"
	else
		prints "ERROR: immsgfind utility output is not correct. Please check Manually." "immsgfind_utility" "1"
		Result="1"
	fi
	
}

function immsgtrace_utility() {

	start_time_tc immsgtrace_utility_tc
	
	user_name=$1					
	immsgtrace -to $user_name -size 2 > log_msgtrace.txt				
	msg_count1=$(cat log_msgtrace.txt | grep "sending message" | wc -l)
	msg_count1=`echo $msg_count1 | tr -d " "`					   
	if [ "$msg_count1" == "1" ]
	then
		prints " immsgtrace is working fine." "immsgtrace_utility" "2"
		Result="0"
	else
		prints " immsgtrace is not working fine. Please check manually" "immsgtrace_utility" "1"
		Result="1"
	fi
		
}

function imboxattctrl_utility() {

	start_time_tc imboxattctrl_utility_tc
	
	user_name=$1	
	type_att=$2
	imboxattrctrl > automsg
	
	imboxattrctrl set $type_att $user_name openwave.com automsg > log_imboxatt.txt
	check_setattr=$(cat log_imboxatt.txt | grep -i "1" | wc -l )
	check_setattr=`echo $check_setattr | tr -d " "`
	prints "We have set "$check_setattr" auto reply for the user $user_name" "imboxattctrl_utility" 
	
	imboxattrctrl get $type_att $user_name openwave.com > log_imboxatt_1.txt
	check_getattr=$(cat log_imboxatt_1.txt | grep -i "1" | head -1 | wc -l)
	check_getattr=`echo $check_getattr | tr -d " "`
	prints "We have got "$check_getattr" auto reply for the user $user_name" "imboxattctrl_utility" 
	if [[ "$check_setattr" == "1" && "$check_getattr" == "1" ]]
	then
		prints "imboxattrctrl utility is working fine" "imboxattctrl_utility" "2"
		Result="0"
	else
		prints "ERROR: imboxattrctrl utility is not working fine. Please check Manually." "imboxattctrl_utility" "1"
		Result="1"
	fi
		
}

function imboxtest_utility() {

	start_time_tc imboxtest_utility_tc
	user_name=$1	
	
	imboxtest $POPHost $user_name $user_name > log_imboxtest.txt
	error_cnt=$(cat log_imboxtest.txt | grep -i "ERR" | wc -l)
	error_cnt=`echo $error_cnt | tr -d " "`
	
	cat log_imboxtest.txt >> debug.log
	if [ "$error_cnt" == "0" ]
	then
		prints "imboxtest utility is working fine." "imboxtest_utility" "2"
		Result="0"
	else
		prints "ERROR: imboxtest utility is not showing proper results." "imboxtest_utility" "1"
		Result="1"
	fi

}

function imcheckfq_utility() {

	start_time_tc imcheckfq_utility_tc
	
	user_name=$1
	imdbcontrol SetCosAttribute default mailFolderQuota '/ all,AgeSeconds,1'
	
	imcheckfq -u $user_name@openwave.com > log_imcheckfq.txt
	check_imcheckfq=$(cat log_imcheckfq.txt | grep -i "AgeSeconds" | wc -l)
	check_imcheckfq=`echo $check_imcheckfq | tr -d " "`
	echo "check_imcheckfq" $check_imcheckfq
	
	if [ "$check_imcheckfq" == "1" ]
	then
		prints "Imcheckfq utility is working fine" "imcheckfq_utility" "2"
		Result="0"
	else
		prints "ERROR: Imcheckfq utility is not working fine. Please check Manually." "imcheckfq_utility" "1"
		Result="1"
	fi
	
}
