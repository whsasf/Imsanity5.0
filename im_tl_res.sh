#!/bin/bash
# generate xml report from summary.log

TL_ID="tlid.xml"
sum_log="summary.log"
TL_log="tlres_import.xml"

if [ ! -f $TL_ID ]; then
    echo "$TL_ID:No such file"
    exit 1
fi

if [ ! -f $sum_log ];then 
   echo "$sum_log:No such file"
   exit 1

fi

echo '<?xml version="1.0" encoding="UTF-8"?>' > $TL_log
echo '<results>' >> $TL_log

#get TC name and result
while read line
do

    if ! echo $line | grep -qE "\[PASS\]|\[FAIL\]"; then
      continue
    fi

    tc=$(echo $line | awk -F ":-" '{print $1}')
    tc=$(echo $tc | tr -d "")
    n=$(cat $TL_ID | grep -c "$tc")
    if [ $n -eq 1 ];then 
        id=$(cat ${TL_ID} |grep "${tc}" |cut -d'"' -f2)
        var=$(cat $sum_log | grep "${tc}" |cut -d "[" -f2)
        var=$(echo ${var:0:1})
        res=$(echo $var| tr "[A-Z]" "[a-z]")
        echo "<testcase id=\"$id\"><result>${res}</result></testcase>" >>$TL_log
    fi
done < $sum_log


echo '</results>' >>$TL_log
echo "Done!"
echo "Pls check ${TL_log}"

