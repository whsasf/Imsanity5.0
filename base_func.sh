#!/bin/bash
# It is used to check for the setup if it is steful or steless #
function check_setup_type() {

	check_clustername_count=$(cat $INTERMAIL/config/config.db | grep clusterName | grep -v mig | wc -l)
	#checksetup_type=$(imconfget -m mss statelessMSS)
	checksetup_type="true"
	if [[ "$check_clustername_count" -ge "1" && "$checksetup_type" == "true" ]]
	then
		setuptype="stateless"
	else
		setuptype="stateful"
	fi
	if [ "$setuptype" == "stateless" ]
		then
		# function call for stateless execution
		stateless
		elif [ "$setuptype" == "stateful" ]
		then
		# function call for stateful execution
		stateful
	fi		
}

function create_headerfiles(){

	echo "FROM:test2@openwave.com" > header1.txt
	echo "TO:test1@openwave.com" >> header1.txt
	echo "Subject:test2 to test1" >> header1.txt
	echo "This is messages from user test2 to test1.">> header1.txt

	echo "FROM:root@openwave.com" > header2.txt
	echo "TO:test1@openwave.com" >> header2.txt
	echo "Subject:root to test1" >> header2.txt
	echo "This is messages from user root to test1.">> header2.txt


	echo "FROM:abc@openwave.com" > header3.txt
#	echo "TO:test1@openwave.com" >> header3.txt
#	echo "Subject:abc to test1" >> header3.txt
	echo "This is messages from user abc to test1.">> header3.txt

}

function end_time_tc() {
	
	t=$1
	arg1=$1
	interval_time_end_tc=$(date +\%s)
	echo "Test case $arg1 end at $interval_time_end_tc" >> Alltestcasestime.txt
	interval_time_total_tc=$(expr $interval_time_end_tc - $interval_time_start_tc)
	total_min_tc=60		
	interval_time_total_min_tc=$(echo "scale=2; $interval_time_total_tc / $total_min_tc" | bc)
	echo "===================================================================" >> Alltestcasestime.txt
	echo "Time taken by test case $arg1 is $interval_time_total_min_tc Min" >> Alltestcasestime.txt
	echo "===================================================================" >> Alltestcasestime.txt
}
 
function imsanity_version() {
imsanity_version="4.0"
	set_color "6"
	echo "====================================================================" 
	echo "IMSANITY TOOL VERSION $imsanity_version STARTED AT ====> "`date`
	echo "===================================================================="
	set_color "0"
	interval_time_start=$(date +\%s)		
}

# It is used to print the output #
function prints() {

	string_to_print=$1
	test_case_name=$2
	log_level=$3
	colour=$4
		
	if [ "$3" != "debug" ]
	then
	colour=$3
	fi
		
	time=$(date '+%H:%M:%S')
			
	if [ "$colour"  == "1" ] 
	then
	set_color "1"
	elif [ "$colour"  == "2" ] 
	then
	set_color "2"
	elif [ "$colour"  == "3" ] 
	then
	set_color "3"
	elif [ "$colour"  == "4" ] 
	then
	set_color "4"
	elif [ "$colour"  == "5" ] 
	then
	set_color "5"
	elif [ "$colour"  == "6" ] 
	then
	set_color "6"
	else
	set_color "0"
	fi
	
	if [ "$log_level" == "debug" ]	
	then		
	echo "$time [$test_case_name]: $string_to_print" >> debug.log
	else
	echo "$time [$test_case_name]: $string_to_print" 
	echo "$time [$test_case_name]: $string_to_print" >> debug.log
	fi
		
	set_color "0"
}

#!/bin/bash
function set_color(){
	color=$1
	if [ "$color" == "0" ]
	then
	tput sgr0 # Text reset
	fi
	if [ "$color" == "1" ]
	then
	tput setaf 1 # Red
	fi
	if [ "$color" == "2" ]
	then
	tput setaf 2 # green
	fi
	if [ "$color" == "3" ]
	then
	tput setaf 3 # yellow
	fi
	if [ "$color" == "4" ]
	then
	tput setaf 4 # blue
	fi
	if [ "$color" == "5" ]
	then
	tput setaf 5 # pink
	fi
	if [ "$color" == "6" ]
	then
	tput setaf 6 # light blue
	fi
}

# It tells about what time the test case started and finished executing #
function start_time_tc() {	
			
	t=$1
	arg1=$1
	interval_time_start_tc=$(date +\%s) 
	echo " " >> Alltestcasestime.txt
	echo " " >> Alltestcasestime.txt
	echo "Test case $arg1 start at $interval_time_start_tc"  >> Alltestcasestime.txt
}

function summary() {
	func_name=$1
	value=$2
	ITS_no=$3
	interval_time_end_tc=$(date +\%s)
	interval_time_total_tc=$(expr $interval_time_end_tc - $interval_time_start_tc)
	total_min_tc=60		
	interval_time_total_min_tc=$(echo "scale=2; $interval_time_total_tc / $total_min_tc" | bc)
	
	string_count=$(echo -n $func_name | wc -m)
	count=$((90-$string_count))
	
	if [ "$func_name" == "summary_imsanity_tc" ]
	then 
	echo " Total number of Test Cases : "$TotalTestCases >> summary.log
	echo " PASS : "$Pass >> summary.log
	echo " FAIL : "$Fail >> summary.log 
	else
		if [ "$value" == "0" ]
		then
		printf "$func_name :-%"$count"s%s\n" "....................:" "[PASS] (Time Taken : $interval_time_total_tc Sec.)" >> summary.log
		#echo " $func_name ..................................................: [PASS] (Time Taken : $interval_time_total_tc Sec.)" >> summary.log
		Pass=$(($Pass+1))
		TotalTestCases=$(($TotalTestCases+1))
		else
		#echo " "$func_name" ................................................: [FAIL] (Time Taken : $interval_time_total_tc Sec.) $ITS_no" >> summary.log
		printf "$func_name :-%"$count"s%s\n" "....................:" "[FAIL] (Time Taken : $interval_time_total_tc Sec.) $ITS_no" >> summary.log
		Fail=$(($Fail+$value))
		TotalTestCases=$(($TotalTestCases+$value))
		fi
	fi
} 


function summary_imsanity() {
		prints "##############################################" "summary_imsanity" 
		prints "        SUMMARY OF THE IMSANITY TOOL          " "summary_imsanity" 
		prints " Refer sumamry.log file for Test cases result " "summary_imsanity" 
		prints "##############################################" "summary_imsanity" 
		#echo
		prints "FINDING CORES FILES..." "summary_imsanity" 
		prints "===========" "summary_imsanity" 
		#cat cores_found.txt
		#cat mta_cores_found.txt
		#cat pop_cores_found.txt
		#cat imap_cores_found.txt
		find $INTERMAIL/ -name "*core.*"
		#echo
		prints "FINDING ERRORS LOGS..." "summary_imsanity" 
		prints "===============" "summary_imsanity" 
		 
		cat $INTERMAIL/log/mta.log | egrep -i "erro;|urgt;|fatl;" >> debug.log
					err_count=$(cat $INTERMAIL/log/mta.log | egrep -i "erro;|urgt;|fatl;" | wc -l)
					if [ "$err_count" -gt "0" ]	
					then
					prints "MTA ERRORS >>>" "summary_imsanity" "1"
					prints "Error found in mta.log. Please check debug.log" "smtp_operation" "1"
					prints "<<<" "summary_imsanity" "debug" "1"
					else
					prints "No Error found in mta.log." "smtp_operation" "2"
					fi
		
		cat $INTERMAIL/log/popserv.log | egrep -i "erro;|urgt;|fatl;" >> debug.log
					err_count=$(cat $INTERMAIL/log/popserv.log | egrep -i "erro;|urgt;|fatl;" | wc -l)
					if [ "$err_count" -gt "0" ]	
					then
					prints "POP ERRORS >>>" "summary_imsanity" "1"
					prints "Error found in popserv.log. Please check debug.log" "smtp_operation" "1"
					prints "<<<" "summary_imsanity" "debug" "1"
					else
					prints "No Error found in popserv.log." "smtp_operation" "2"
					fi
			
		cat $INTERMAIL/log/imapserv.log | egrep -i "erro;|urgt;|fatl;" >> debug.log
					err_count=$(cat $INTERMAIL/log/imapserv.log | egrep -i "erro;|urgt;|fatl;" | wc -l)
					if [ "$err_count" -gt "0" ]	
					then
					prints "IMAP ERRORS >>>" "summary_imsanity" "1"
					prints "Error found in imapserv.log. Please check debug.log" "smtp_operation" "1"
					prints "<<<" "summary_imsanity" "debug" "1"
					else
					prints "No Error found in imapserv.log." "smtp_operation" "2"
					fi

		cat $INTERMAIL/log/mss.log | egrep -i "erro;|urgt;|fatl;" | egrep -v "ClusterUnableToPingMSS|ProcWriteToStderr" >> debug.log
					err_count=$(cat $INTERMAIL/log/mss.log | egrep -i "erro;|urgt;|fatl;" | egrep -v "ClusterUnableToPingMSS|ProcWriteToStderr" | wc -l)
					if [ "$err_count" -gt "0" ]	
					then
					prints "MSS ERRORS >>>" "summary_imsanity" "1"
					prints "Error found in mss.log. Please check debug.log" "smtp_operation" "1"
					prints "<<<" "summary_imsanity" "debug" "1"
					else
					prints "No Error found in mss.log." "smtp_operation" "2"
					fi

		prints "NOTE:" "summary_imsanity" "6"
		prints "=====" "summary_imsanity" "6"
		prints "1. Please check cores and errors on other Mx hosts in this setup." "summary_imsanity" "6"
		prints "2. This utility works only if atleast 2 MSS are there in the MSS Cluster." "summary_imsanity" "6"
		prints "3. Please refer sumamry.log file for Test cases result" "summary_imsanity" "6"
		prints "4. Please ignore the message \"-ERR no such message\" from the POP results. This occurs in an edge case and is expected." "summary_imsanity" "6"
		prints "##############################################" "summary_imsanity" "6"

		#echo
		prints "===============================================================" "summary_imsanity" "6"
		prints "IMSANITY TOOL COMPLETED AT ====> `date`" "summary_imsanity" "6"
		prints "===============================================================" "summary_imsanity" "6"
		interval_time_end=$(date +\%s)
		interval_time_total=$(expr $interval_time_end - $interval_time_start)
		total_min=60		
		interval_time_total_min=$(echo "scale=2; $interval_time_total / $total_min" | bc)
		prints "================================================================" >> summary.log
		prints "IMSANITY TOOL TOOK "$interval_time_total_min" MINUTE TO COMPLETE" >> summary.log
		prints "================================================================" >> summary.log
		prints "================================================================" "summary_imsanity" "6"
		prints "IMSANITY TOOL TOOK "$interval_time_total_min" MINUTE TO COMPLETE" "summary_imsanity" "6"
		prints "================================================================" "summary_imsanity" "6"
		#calling function for summary logs
		summary summary_imsanity_tc 0
		exit
}
